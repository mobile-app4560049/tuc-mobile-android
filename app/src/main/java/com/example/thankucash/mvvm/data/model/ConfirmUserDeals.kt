package com.example.thankucash.mvvm.data.model

data class ConfirmUserDeals(
    val Amount: Int,
    val Charge: Int,
    val CountryId: Int,
    val CountryKey: String,
    val DealId: Int,
    val DealKey: String,
    val DeliveryCharge: Int,
    val FromAddressId: Int,
    val FromAddressKey: Any,
    val ItemCount: Int,
    val PaymentReference: Any,
    val PaymentSource: String,
    val ReferenceId: String,
    val ReferenceKey: String,
    val Task: String,
    val ToAddressId: Int,
    val ToAddressKey: Any,
    val TotalAmount: Int,
    val TransactionId: Int
)