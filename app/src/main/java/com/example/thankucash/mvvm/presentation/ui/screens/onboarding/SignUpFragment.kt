package com.example.thankucash.mvvm.presentation.ui.screens.onboarding

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentSignUpBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.UserRegistration
import com.example.thankucash.mvvm.presentation.viewmodel.RegistrationViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.VerifyUserViewModel
import com.example.thankucash.mvvm.utils.validation.*
import com.example.thankucash.mvvm.utils.validation.FieldValidationTracker.populateFieldTypeMap
import com.example.thankucash.mvvm.utils.validation.FieldValidations.verifyEmail
import com.example.thankucash.mvvm.utils.validation.FieldValidations.verifyName
import com.example.thankucash.mvvm.utils.validation.FieldValidations.verifyPin
import com.example.thankucash.mvvm.utils.validation.SessionManager.FIRST_NAME
import com.example.thankucash.mvvm.utils.validation.SessionManager.LAST_NAME
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
@AndroidEntryPoint
class SignUpFragment : Fragment() {
    private var _binding: FragmentSignUpBinding? = null
    private val binding get() = _binding!!
    private lateinit var dialog: Dialog
    private val registrationViewModel: RegistrationViewModel by activityViewModels()
    private val shareViewModel: ShareViewModel by activityViewModels()
    private val verifyUserViewModel: VerifyUserViewModel by activityViewModels()

    var numberInput = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            dialog = provideCustomAlertDialog()


            setUpObservers()
        }


        val ccp = binding.countryPicker
        val phone = binding.numberInput
        ccp.registerCarrierNumberEditText(phone)



        binding.signUpBtn.setOnClickListener {
            if (!validateField()){
                return@setOnClickListener
            }

            val firstName = binding.firstName.text.toString()
            val lastName = binding.lastName.text.toString()

            SessionManager.saveToSharedPref(requireContext(), FIRST_NAME, firstName)
            SessionManager.saveToSharedPref(requireContext(), LAST_NAME, lastName)

            validatingCountryInput()
            numberInput = binding.numberInput.text.toString()
            verifyUserViewModel.userPhoneNumber = numberInput
            val user = UserRegistration(
                TaskEnum.REGISTER.taskType,
                binding.lastName.text.toString(),
                binding.firstName.text.toString(),
                0,
                CountryCode.NGN.countries.countryIsd,
                binding.numberInput.text.toString(),
                binding.pinInputLayout.toString(),
                binding.emailInputLayout.toString(),
                binding.referralInputLayout.toString(),
                CountryCode.NGN.countries.countryCode,
                CountryCode.NGN.countries.countryKey
            )
            registerUser(user)
        }

        binding.apply {
            login.setOnClickListener { findNavController().navigate(R.id.loginFragment) }
        }
    }


    private fun registerUser(userRegistration: UserRegistration) {
        lifecycleScope.launch {
            registrationViewModel.registerUser(userRegistration)
        }
    }


    private fun setUpObservers() {
        registrationViewModel.registerUserResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    var action =
                        SignUpFragmentDirections.actionSignUpFragmentToVerifyPhoneNumberFragment(
                            numberInput
                        )
                    findNavController().navigate(action)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }

    }

    override fun onResume() {
        super.onResume()
        binding.firstName.clearFocus()
    }

    private fun validateField() : Boolean {
        val fieldTypesToValidate = listOf(
            FieldValidationTracker.FieldType.EMAIL,
            FieldValidationTracker.FieldType.PIN,
            FieldValidationTracker.FieldType.PHONE_NUMBER,
            FieldValidationTracker.FieldType.REFERRAL_CODE,
            FieldValidationTracker.FieldType.FIRSTNAME,
            FieldValidationTracker.FieldType.LASTNAME,
        )
        populateFieldTypeMap(fieldTypesToValidate)

        var success = false

        binding.apply {
            emailInputLayout.validateField(
                getString(R.string.valideemail)
            ) { input ->
               success = verifyEmail(input)
                success
            }
            pinInputLayout.validateField(
                getString(R.string.pinmustbe4character)
            ) { input ->
               success = success && verifyPin(input)
                success
            }
            firstNameLayout.validateField(
                getString(R.string.enter)
            ) { input ->
                success = success && verifyName(input)
                success
            }
            lastNameLayout.validateField(
                getString(R.string.enteravalidname)
            ){ input ->
                success = success && verifyName(input)
                success
            }
            }
        return success
        }

    private fun validatingCountryInput() {
        val countryCode = binding.countryPicker.toString()
        val phoneNumber = binding.numberInput.toString()
        val errorMessage = FieldValidations.validatePhoneNumberFormat(countryCode, phoneNumber)
        if (errorMessage != null) {
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Phone Number format is valid", Toast.LENGTH_SHORT)
                .show()
        }
    }



    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}

