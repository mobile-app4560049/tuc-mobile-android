package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.app.Dialog
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Query
import com.example.thankucash.MainActivity
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentTopDealsBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.TopDeals
import com.example.thankucash.mvvm.data.model.TopDealsResponse
import com.example.thankucash.mvvm.presentation.adapters.TopDealsAdapter
import com.example.thankucash.mvvm.presentation.viewmodel.TopDealsViewModel
import com.example.thankucash.mvvm.utils.validation.ConnectivityLiveData
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class TopDealsFragment : Fragment() {
    private var _binding : FragmentTopDealsBinding? = null
    private val binding get() = _binding!!
    private val topDealsViewModel: TopDealsViewModel by activityViewModels()
    private lateinit var dialog: Dialog
    private val shareViewModel: ShareViewModel by activityViewModels()

   lateinit var topDealsAdapter: TopDealsAdapter

    lateinit var connectivityLiveData: ConnectivityLiveData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTopDealsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        connectivityLiveData = (activity as MainActivity).connectivityLiveData

        connectivityLiveData.observe(
            viewLifecycleOwner
        ) { hasNetwork ->
            if (hasNetwork)
                topDealsViewModel.topDealsResponse

        }


        topDealsAdapter = TopDealsAdapter { it: TopDealsResponse ->
            shareViewModel.topDeal = it.Result.Data[0]
            findNavController().navigate(R.id.orderDetailsFragment)
        }

        binding.searchNotFound.visibility = View.GONE

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }

        topDealsViewModel.topDealsResponse

        binding.recyclerViewFlashDeals.apply {
            adapter = topDealsAdapter
            layoutManager = LinearLayoutManager(
                requireContext(), LinearLayoutManager.HORIZONTAL, false
            )

        }


        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                   topDealsViewModel.searchQuery.value = newText
                }
                return true
            }
        })


        binding.filterLayout.setOnClickListener { findNavController().navigate(R.id.productDealsFragment) }

        binding.topDealsArrowBack.setOnClickListener { activity?.onBackPressed() }



        var deals = TopDeals(
            TaskEnum.TOPDEALS.taskType,
            0,
            Limit = 15,
            "",
            "Newest",
            arrayOf(),
            arrayOf(),
            "",
            "Top Deals",
            CountryCode.NGN.countries.countryCode,
            CountryCode.NGN.countries.countryKey
        )
        topDeal(deals)

    }

    private fun topDeal(topDeals: TopDeals) {
        lifecycleScope.launch {
            topDealsViewModel.getTopDeals(topDeals)
        }
    }

    private fun setUpObservers() {
        topDealsViewModel.topDealsResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    requireView().showSnackBar(R.string.call_success)
                    it.data?.Result?.Data?.let { it1 ->
                        topDealsViewModel.cachedTopDeals = it1
                        updateViews(topDealsViewModel.searchQuery.value!!)
                    }

                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }

            }
        }
        topDealsViewModel.searchQuery.observe(viewLifecycleOwner){ query ->
         updateViews(query)
        }
    }

    fun updateViews(query: String){
        if (query.isEmpty()){
            topDealsAdapter.differ.submitList(topDealsViewModel.cachedTopDeals)
            binding.searchNotFound.visibility = View.GONE
            binding.recyclerViewFlashDeals.visibility = View.VISIBLE
            return
        }
        if (!topDealsViewModel.cachedTopDeals.any{
                it.Title.lowercase().contains(query)
            }){
            binding.searchNotFound.visibility = View.VISIBLE
            binding.recyclerViewFlashDeals.visibility = View.GONE
        }else{
            binding.searchNotFound.visibility = View.GONE
            binding.recyclerViewFlashDeals.visibility = View.VISIBLE
        }
        topDealsAdapter.differ.submitList(topDealsViewModel.cachedTopDeals.filter{
            it.Title.lowercase().contains(query)
        })
    }

}

