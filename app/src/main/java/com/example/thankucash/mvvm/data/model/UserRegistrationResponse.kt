package com.example.thankucash.mvvm.data.model


data class UserRegistrationResponse(
    val Key: String,

    val IsNewAccount: Boolean,

    val AccountKey: String,

    val AccountID: Long,

    val LoginTime: String,

    val displayName: String,

    val EmailAddress: String,

    val Name: String,

    val IconUrl: String,

    val IsMobileNumberVerified: Boolean,

    val RequestToken: String,

    val ReferralCode: String,

    val CreatedAt: String,

    val Country: Country
)

