package com.example.thankucash.mvvm.presentation.ui.screens.onboarding

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentForgotPinBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.ResetPin
import com.example.thankucash.mvvm.presentation.viewmodel.ResetPinViewModel
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ForgotPinFragment : Fragment() {
private var _binding : FragmentForgotPinBinding? = null
    private val binding get() = _binding!!
    private val resetPinViewModel: ResetPinViewModel by activityViewModels()
    private lateinit var dialog: Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentForgotPinBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
            getPhoneNumber()
        }

        val ccp = binding.countryPicker
        val phone = binding.numberInput
        ccp.registerCarrierNumberEditText(phone)




        binding.apply {
            verifyTextView.setOnClickListener {
                findNavController().navigate(R.id.verifyPhoneNumberFragment)
            }
            forgotPinArrowBack.setOnClickListener { activity?.onBackPressed() }
        }

        binding.forgotPinBtn.setOnClickListener {
            findNavController().navigate(R.id.pinGeneratedFragment)
            var user = ResetPin(
            TaskEnum.RESETPIN.taskType,
                CountryCode.NGN.countries.countryIsd,
                binding.numberInput.text.toString(),
                CountryCode.NGN.countries.countryCode,
                CountryCode.NGN.countries.countryKey
            )

            resetUser(user)
        }

    }

    private fun getPhoneNumber(): String {
        val countryCodePicker = binding.countryPicker
        val phoneNumberEditText = binding.numberInput

        val countryCode = countryCodePicker.selectedCountryCodeWithPlus
        val phoneNumber = phoneNumberEditText.text?.toString()?.trim() ?: ""
        return "$countryCode$phoneNumber"
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
    private fun resetUser(resetPin: ResetPin){
        lifecycleScope.launch{
            resetPinViewModel.resetUserPin(resetPin)
        }
    }

    private fun setUpObservers() {
        resetPinViewModel.resetPinResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_success)
                    findNavController().navigate(R.id.loginFragment)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
    }
}