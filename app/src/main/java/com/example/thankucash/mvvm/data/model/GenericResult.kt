package com.example.thankucash.mvvm.data.model

data class GenericResult<T>(
    val message: String,
    val result: T,
    val status: String,
)
