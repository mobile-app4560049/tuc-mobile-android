package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.AirtimeListBinding
import com.example.thankucash.mvvm.data.model.listModel.UtilityItem
import com.example.thankucash.mvvm.presentation.ui.viewmodels.DataAndAirtimeViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AirtimeAdapter: RecyclerView.Adapter<AirtimeAdapter.AirtimeItemViewHolder>() {
    private var airtimeItemList: ArrayList<UtilityItem> = arrayListOf()
    private var selectedPos = -1

     inner class AirtimeItemViewHolder(val binding: AirtimeListBinding): RecyclerView.ViewHolder(binding.root) {
         var cardView = binding.cardView
         var constraintLayout = binding.constraint

         fun bind(dealsItem: UtilityItem) {
             binding.airtelText.text = dealsItem.text
             binding.imageAirtel.setImageResource(dealsItem.image)
             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 notifyItemChanged(selectedPos)
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirtimeItemViewHolder {
          val inflater = AirtimeListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return AirtimeItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: AirtimeItemViewHolder, @SuppressLint("RecyclerView") position: Int) {
          holder.bind(airtimeItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int = airtimeItemList.size
     fun submitList (list: ArrayList<UtilityItem>){
         airtimeItemList = list
         notifyDataSetChanged()
     }

    companion object {
        private const val TAG = "AirtimeAdapter"
    }
}