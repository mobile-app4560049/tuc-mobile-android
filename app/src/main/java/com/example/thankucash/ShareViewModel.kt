package com.example.thankucash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.thankucash.mvvm.data.model.Datum
import com.example.thankucash.mvvm.data.model.TopDeals
import com.example.thankucash.mvvm.data.model.TopDealsResponse
import com.example.thankucash.mvvm.data.model.listModel.*
import com.example.thankucash.mvvm.presentation.ui.screens.onboarding.VerifyPhoneNumberFragment
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class ShareViewModel @Inject constructor(): ViewModel() {
    val message = MutableLiveData<String>()
    var value:DataPlansItemList? = null
    var method: DstvPlansItem?= null
    var electricityValue: ElectricityPlansItem? = null

    var deals: DealsItem? = null

    var topDeal: Datum? = null

    var currentDealId : Int? = null

    var signUpPhoneNumber: VerifyPhoneNumberFragment? = null

    var airtime: AirtimeItem? = null
    var rewards: RewardsItem? = null
    var dataBundle: DataBundleListItem? = null
    fun sendMessage(text: String){
        message.value = text
    }
}