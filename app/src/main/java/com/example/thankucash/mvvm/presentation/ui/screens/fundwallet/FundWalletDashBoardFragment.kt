package com.example.thankucash.mvvm.presentation.ui.screens.fundwallet

import android.app.Dialog
import android.os.Bundle
import android.os.CountDownTimer
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentFundWalletDashBoardBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.FlashDeals
import com.example.thankucash.mvvm.data.model.TopDeals
import com.example.thankucash.mvvm.presentation.adapters.FeatureDealsAdapter
import com.example.thankucash.mvvm.presentation.adapters.TopDealsAdapter
import com.example.thankucash.mvvm.presentation.viewmodel.FlashDealsViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.TabStateViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.TopDealsViewModel
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.microsoft.appcenter.utils.HandlerUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class FundWalletDashBoardFragment : Fragment() {
    private var _binding: FragmentFundWalletDashBoardBinding? = null
    private val binding get() = _binding!!

    private val topDealsViewModel: TopDealsViewModel by activityViewModels()
    private val flashDealsViewModel: FlashDealsViewModel by activityViewModels()
    private lateinit var dialog: Dialog
    lateinit var topDealsAdapter: TopDealsAdapter
    lateinit var featureDealsAdapter: FeatureDealsAdapter
    private val shareViewModel: ShareViewModel by activityViewModels()


    val tabStateViewModel: TabStateViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFundWalletDashBoardBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topDealsAdapter = TopDealsAdapter {
            shareViewModel.topDeal = it.Result.Data[0]
            findNavController().navigate(R.id.orderDetailsFragment)
        }

        featureDealsAdapter = FeatureDealsAdapter {
            shareViewModel.topDeal = it.Result.Data[0]
            findNavController().navigate(R.id.orderDetailsFragment)
        }

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObserves()
        }

        val timer = binding.timer
        val millisInFuture: Long = 10 * 60 * 1000 // 10 minutes in milliseconds
        val countDownInterval: Long = 1000 // update every second

        val countdownTimer = object : CountDownTimer(millisInFuture, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                val minutes = millisUntilFinished / (60 * 1000)
                val seconds = (millisUntilFinished / 1000) % 60
                timer.text = "$minutes:${String.format("%02d", seconds)}"
            }

            override fun onFinish() {
                timer.text = "0:00"
            }
        }
        countdownTimer.start()

        togglePasswordVisibility(binding.textView19, binding.toggleImage)

        binding.apply {
            fundWalletLayout.setOnClickListener {
                findNavController().navigate(R.id.fundWalletFragment)
            }
            fundWalletLayout.setOnClickListener { findNavController().navigate(R.id.fundWalletFragment) }
            topDeals.constraintLayout4.setOnClickListener {
                tabStateViewModel.setAirtimeProvider(0)
                findNavController().navigate(R.id.airtimeDataViewPagerFragment)
            }
            topDeals.constraintLayout7.setOnClickListener {
                tabStateViewModel.setAirtimeProvider(1)
                findNavController().navigate(R.id.airtimeDataViewPagerFragment)
            }
            topDeals.constraintLayout6.setOnClickListener { findNavController().navigate(R.id.utility_bills_nav) }
            rewardLayout.setOnClickListener { findNavController().navigate(R.id.history_navigation) }
            notificationIcon.setOnClickListener { findNavController().navigate(R.id.notificationFragment) }
            topDealLayout.setOnClickListener { findNavController().navigate(R.id.topDealsFragment) }
            textView98.setOnClickListener { findNavController().navigate(R.id.flashDealsFragment) }
            seeAllFeatureDeals.setOnClickListener { findNavController().navigate(R.id.topDealsFragment) }

        }


        val amount = 100
        val amountText = "$amount USD"
        binding.textView19.text = amountText
        //

        binding.recyclerViewFundWallet.apply {
            adapter = featureDealsAdapter
            layoutManager = LinearLayoutManager(
                requireContext(), LinearLayoutManager.HORIZONTAL, false
            )
        }

        binding.secondRecyclerView.apply {
            adapter = topDealsAdapter
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }

        var deals = TopDeals(
            TaskEnum.TOPDEALS.taskType,
            0,
            Limit = 15,
            "",
            "Newest",
            arrayOf(),
            arrayOf(),
            "",
            "Top Deals",
            CountryCode.NGN.countries.countryCode,
            CountryCode.NGN.countries.countryKey
        )
        topDeal(deals)

        var flash = FlashDeals(
            TaskEnum.TOPDEALS.taskType,
            0,
            Limit = 15,
            "",
            "Flash",
            arrayOf(),
            arrayOf(),
            "",
            0,
            0,
            CountryCode.NGN.countries.countryCode,
            CountryCode.NGN.countries.countryKey
        )
        flashDeal(flash)

    }

    private fun togglePasswordVisibility(textView: TextView, imageView: ImageView) {
        imageView.setOnClickListener {
            if (textView.inputType == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                textView.inputType =
                    InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                imageView.setImageResource(R.drawable.eyes)
            } else {
                textView.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                imageView.setImageResource(R.drawable.ic_eye_toggle)
            }
            binding.textView19.text = textView.text.length.toString()
        }
    }

    private fun flashDeal(flashDeals: FlashDeals) {
        lifecycleScope.launch {
            flashDealsViewModel.getFlashDeals(flashDeals)
        }
    }

    private fun setUpObservers() {
        flashDealsViewModel.flashDealsResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    Log.d("AAAAAA", "${it.message}")
                    binding.cropProgress.visibility = View.INVISIBLE
                    it.data?.Result?.Data?.let { it1 -> featureDealsAdapter.differ.submitList(it1) }

                }
                is Resource.Error -> {
                    dialog.dismiss()
                    binding.cropProgress.visibility = View.INVISIBLE
                }
                is Resource.Loading -> {
                    binding.cropProgress.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun topDeal(topDeals: TopDeals) {
        lifecycleScope.launch {
            topDealsViewModel.getTopDeals(topDeals)
        }
    }

    private fun setUpObserves() {
        topDealsViewModel.topDealsResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    binding.cropProgress.visibility = View.INVISIBLE
                    it.data?.Result?.Data?.let { it1 -> topDealsAdapter.differ.submitList(it1) }

                }
                is Resource.Error -> {
                    dialog.dismiss()
                    binding.cropProgress.visibility = View.INVISIBLE
                }
                is Resource.Loading -> {
                    binding.cropProgress.visibility = View.VISIBLE
                }
            }
        }
    }
}
