package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.GenericResult
import com.example.thankucash.mvvm.data.model.UserRegistration
import com.example.thankucash.mvvm.data.model.UserRegistrationResponse
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegistrationViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel(){

    private var _registerUserResponse = MutableLiveData<Resource<GenericResult<UserRegistrationResponse>>>()
    val registerUserResponse: LiveData<Resource<GenericResult<UserRegistrationResponse>>> get() = _registerUserResponse

    suspend fun registerUser(userRegistration: UserRegistration){
        _registerUserResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch(Dispatchers.Default){
            try {
                _registerUserResponse.value = authRepository.userRegistration(userRegistration)
            }catch (e: Exception){
                Log.d("Loading",e.javaClass.simpleName)
            }
        }
    }


}