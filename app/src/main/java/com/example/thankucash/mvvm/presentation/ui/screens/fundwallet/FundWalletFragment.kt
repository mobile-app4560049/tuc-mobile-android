package com.example.thankucash.mvvm.presentation.ui.screens.fundwallet

import android.os.Bundle
import android.text.Editable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentFundWalletBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FundWalletFragment : Fragment() {
    private var _binding: FragmentFundWalletBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFundWalletBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = ViewModelProvider(requireActivity()).get(ShareViewModel::class.java)


        binding.fundWalletTopUpWalletBtn.setOnClickListener {
            val amount = binding.editTextValue.text.toString().trim()
            if (amount.isEmpty()) {
                Toast.makeText(requireContext(), "Please enter an amount", Toast.LENGTH_SHORT)
                    .show()
            } else {
                findNavController().navigate(R.id.fundWalletPaymentOptionsFragment)
            }
            model.sendMessage(binding.editTextValue.text.toString())
        }

        binding.apply {
            fullStopTextView.setOnClickListener { inputValue(".") }
            numberOneTextView.setOnClickListener { inputValue("1") }
            numberTwoTextView.setOnClickListener { inputValue("2") }
            textView11.setOnClickListener { inputValue("3") }
            numberFourTextView.setOnClickListener { inputValue("4") }
            numberFiveTextView.setOnClickListener { inputValue("5") }
            numberSixTextView.setOnClickListener { inputValue("6") }
            numberSevenTextView.setOnClickListener { inputValue("7") }
            numberEightTextView.setOnClickListener { inputValue("8") }
            numberNineTextView.setOnClickListener { inputValue("9") }
            numberZero.setOnClickListener { inputValue("0") }
            textView14.setOnClickListener {
                val length = editTextValue.text.length
                if (length > 0) {
                    editTextValue.text = editTextValue.text.subSequence(0, length - 1) as Editable?
                }
            }

            fundWalletArrowBack.setOnClickListener { activity?.onBackPressed() }
        }
    }

    private fun inputValue(value: String) {
        Log.d("InputValue", "InputValue called with value: $value")
        val text = binding.editTextValue.text.toString()
        val newText = if (text.isEmpty()) {
            "₦$value"
        } else {
            "$text$value"
        }
        binding.editTextValue.setText(newText)
    }
}
