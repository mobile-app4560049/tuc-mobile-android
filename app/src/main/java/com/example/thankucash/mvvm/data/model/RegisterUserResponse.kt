package com.example.thankucash.mvvm.data.model


data class RegisterUserResponse (
    val Status: String,
    val Message: String,
    val Result: RegisterUserResponse,
    val ResponseCode: String,
    val Mode: String,
    val ReferenceID: String,
    val ReferenceKey: String,
    val Amount: Double,
    val Fee: Long,
    val PaymentSource: String,
    val TotalAmount: Double,
    val Balance: Double,
    val DeliveryCharge: Double
)

