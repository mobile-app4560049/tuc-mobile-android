package com.example.thankucash

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import co.paystack.android.Paystack.TransactionCallback
import co.paystack.android.PaystackSdk
import co.paystack.android.PaystackSdk.applicationContext
import co.paystack.android.Transaction
import co.paystack.android.model.Card
import co.paystack.android.model.Charge
import com.example.thankucash.databinding.FragmentPaystackBinding
import com.example.thankucash.mvvm.utils.validation.PayStackUtils.validateInput
import com.google.android.material.textfield.TextInputLayout
import java.text.NumberFormat
import java.util.*


class PaystackFragment : Fragment() {
    private var _binding: FragmentPaystackBinding? = null
    private val binding get() = _binding!!
    private lateinit var mCardNumber: TextInputLayout
    private lateinit var mCardExpiry: TextInputLayout
    private lateinit var mCardCVV: TextInputLayout
    private val shareViewModel: ShareViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPaystackBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = shareViewModel
        if (model.topDeal != null) {
            val ngnFormat = NumberFormat.getCurrencyInstance(Locale("en", "NG"))
            binding.btnMakePayment.text = ngnFormat.format(model.topDeal?.SellingPrice ?: 0)
        }

        initializePaystack()
        initializeFormVariables()

    }

    private fun initializeFormVariables() {
        mCardNumber = binding.tilCardNumber
        mCardExpiry = binding.tilCardExpiry
        mCardCVV = binding.tilCardCvv

        mCardExpiry.editText!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int, count: Int, after: Int
            ) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 2 && !s.toString().contains("/")) {
                    s!!.append("/")
                }
            }

        })

        binding.btnMakePayment.setOnClickListener { v: View? -> performCharge() }

    }

    private fun initializePaystack() {
        PaystackSdk.initialize(applicationContext)
        PaystackSdk.setPublicKey(BuildConfig.PSTK_PUBLIC_KEY)

    }


    private fun performCharge() {
        val cardNumber = mCardNumber.editText!!.text.toString()
        val cardExpiry = mCardExpiry.editText!!.text.toString()
        val cvv = mCardCVV.editText!!.text.toString()



        val cardExpiryArray = cardExpiry.split("/").toTypedArray()
        val expiryMonth = cardExpiryArray[0].toInt()
        val expiryYear = cardExpiryArray[1].toInt()
        var amount  = (shareViewModel.topDeal?.ActualPrice ?: 0).toInt()
        amount *= 100



        val card = Card(cardNumber, expiryMonth, expiryYear, cvv)

        val charge = Charge()
        charge.amount = amount
        charge.email = "customer@email.com"
        charge.card = card

        PaystackSdk.chargeCard(requireActivity(), charge, object : TransactionCallback {
            override fun onSuccess(transaction: Transaction) {
                parseResponse(transaction.reference)
                findNavController().navigate(R.id.checkOutCongratulationsFragment)
            }

            override fun beforeValidate(transaction: Transaction) {
                Log.d("Main Activity", "beforeValidate: " + transaction.reference)
            }

            override fun onError(error: Throwable, transaction: Transaction) {
                Log.d("Main Activity", "onError: " + error.localizedMessage)
                Log.d("Main Activity", "onError: $error")
            }
        })
    }

    private fun parseResponse(transactionReference: String) {
        val message = "Payment Successful - $transactionReference"
        Toast.makeText(requireActivity(), message, Toast.LENGTH_LONG).show()
    }
}
