package com.example.thankucash.mvvm.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.databinding.DataHistoryItemBinding
import com.example.thankucash.databinding.FlashDealsItemBinding

class TopUpAdapter: RecyclerView.Adapter<TopUpAdapter.DealsItemViewHolder>() {
    private var dealsItemList: ArrayList<String> = arrayListOf()

     class DealsItemViewHolder(val binding: DataHistoryItemBinding): RecyclerView.ViewHolder(binding.root) {
         fun bind(dealsItemList: ArrayList<String>) {
        binding.walletTopText.text = dealsItemList[0]
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealsItemViewHolder {
          val inflater = DataHistoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return DealsItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: DealsItemViewHolder, position: Int) {
          holder.bind(dealsItemList)
     }

     override fun getItemCount(): Int = dealsItemList.size
     fun submitList (list: ArrayList<String>){
          dealsItemList = list
     }
}