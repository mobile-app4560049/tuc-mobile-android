package com.example.thankucash.mvvm.presentation.ui.screens.onboarding

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentViewPagerBinding
import com.example.thankucash.mvvm.presentation.adapters.ViewPagerAdapter
import com.example.thankucash.mvvm.presentation.ui.screens.onboarding.OnBoardingFragment1
import com.example.thankucash.mvvm.presentation.ui.screens.onboarding.OnBoardingFragment2
import com.example.thankucash.mvvm.presentation.ui.screens.onboarding.OnBoardingFragment3
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

//import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ViewPagerFragment : Fragment() {
    private var _binding: FragmentViewPagerBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentViewPagerBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewPagerAdapter = ViewPagerAdapter(this)
        binding.viewPager.adapter = viewPagerAdapter

        TabLayoutMediator(binding.tabLayout, binding.viewPager){tab, position ->
            tab.icon = resources.getDrawable(R.drawable.onboarding_selector_state, resources.newTheme())
        }.attach()

        binding.apply {
            continueAsGuestTxt.setOnClickListener{ findNavController().navigate(R.id.fund_wallet_nav) }
            getStartedBtn.setOnClickListener { findNavController().navigate(R.id.signUpFragment) }
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}