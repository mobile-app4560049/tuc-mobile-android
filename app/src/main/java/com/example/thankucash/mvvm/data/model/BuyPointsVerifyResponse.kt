package com.example.thankucash.mvvm.data.model

data class BuyPointsVerifyResponse(
    val Status: String,
    val Message: String,
    val Result: BuyPointResult,
    val ResponseCode: String,
    val Mode: String
)

data class BuyPointResult (
    val Amount: Double,
    val Charge: Double,
    val TotalAmount: Double,
    val ReferenceID: Long,
    val PaymentReference: String,
    val TransactionDate: String,
    val StatusName: String,
    val StatusCode: String,
    val Balance: Balance
)

data class Balance (
    val SystemDate: String,
    val Credit: Long,
    val Debit: Long,
    val Balance: Long,
    val BalanceValidity: Long,

    val InvoiceAmount: Double,

    val AccountClass: String,

    val AccountClassLimit: Double,

    val PendingReward: Double,

    val TucPlusCredit: Long,
    val TucPlusDebit: Long,
    val TucPlusBalance: Long,
    val TucPlusBalanceValidity: Long
)

