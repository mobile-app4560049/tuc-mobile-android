package com.example.thankucash.mvvm.presentation.ui.screens.referral

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import com.example.thankucash.databinding.FragmentFAQBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FAQFragment : Fragment() {
    private var _binding : FragmentFAQBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFAQBinding.inflate(inflater,container,false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.faqArrowBack.setOnClickListener {
            activity?.onBackPressed()


            binding.thankuImage.setOnClickListener {
                binding.textThanKUcash.isVisible = !binding.textThanKUcash.isVisible
            }
        }

        var currentlyVisibleTextView: TextView? = null

        val imageView = binding.thankuImage
        val textView = binding.textThanKUcash
        imageView.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            textView.isVisible = true
            currentlyVisibleTextView = textView
        }

        val image = binding.howItWorksImage
        val text = binding.howItWorks
        image.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            text.isVisible = true
            currentlyVisibleTextView = text
        }

        val lotteryImage = binding.arrowRightLottery
        val lotteryText = binding.lotteryTextView
        lotteryImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            lotteryText.isVisible = true
            currentlyVisibleTextView = lotteryText
        }

        val nonProfitsTextView = binding.nonProfitsText
        val nonProfitsImage = binding.arrowRightNonProfits
        nonProfitsImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            nonProfitsTextView.isVisible = true
            currentlyVisibleTextView = nonProfitsTextView
        }

        val multipleText = binding.multipleDealsText
        val multipleImage = binding.arrowRightMultipleDeals
        multipleImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            multipleText.isVisible = true
            currentlyVisibleTextView = multipleText
        }

        val supportText = binding.provideSupportText
        val supportImage = binding.arrowRightSupport
        supportImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            supportText.isVisible = true
            currentlyVisibleTextView = supportText
        }

        val reservationImage = binding.reservationArrow
        val reservationText = binding.reservationBookings
        reservationImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            reservationText.isVisible = true
            currentlyVisibleTextView = reservationText
        }

        val purchaseImage = binding.purchaseArrow
        val purchaseText = binding.purchaseDeal
        purchaseImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            purchaseText.isVisible = true
            currentlyVisibleTextView = purchaseText
        }

        val cancelationText = binding.cancelationPolicyTextView
        val cancelationImage = binding.cancelImage
        cancelationImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            cancelationText.isVisible = true
            currentlyVisibleTextView = cancelationText
        }

        val taxText = binding.taxDeductionText
        val taxImage = binding.arrowRightTax
        taxImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            taxText.isVisible = true
            currentlyVisibleTextView = taxText
        }

        val serviceText = binding.serviceProvideText
        val serviceImage = binding.serviceArrow
        serviceImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            serviceText.isVisible = true
            currentlyVisibleTextView = serviceText
        }

        val disabilityText = binding.disabilityText
        val disabilityImage = binding.disabilityArrow
        disabilityImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            disabilityText.isVisible = true
            currentlyVisibleTextView = disabilityText
        }

        val genderText = binding.genderTextView
        val genderArrow = binding.genderArrow
        genderArrow.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            genderText.isVisible = true
            currentlyVisibleTextView = genderText
        }

        val rulesText = binding.rulesText
        val rulesArrow = binding.ruleArrow
        rulesArrow.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            rulesText.isVisible = true
            currentlyVisibleTextView = rulesText
        }

        val teensText = binding.teensTextView
        val arrowImage = binding.teensArrow
        arrowImage.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            teensText.isVisible = true
            currentlyVisibleTextView = teensText
        }

        val deliveryText = binding.delivery
        val deliveryArrow = binding.deliveryArrow
        deliveryArrow.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            deliveryText.isVisible = true
            currentlyVisibleTextView = deliveryText
        }

        val merchantText = binding.merchantTextView
        val merchantArrow = binding.merchantArrow
        merchantArrow.setOnClickListener {
            currentlyVisibleTextView?.isVisible = false
            merchantText.isVisible = true
            currentlyVisibleTextView = merchantText
        }
    }


}