package com.example.thankucash.mvvm.data.model.listModel

import android.widget.ImageView

data class DstvPlansItem(
    var image: Int,
    var dataName: String,
    var earn : String,
    var amount: Int
)
