package com.example.thankucash.mvvm.utils.validation

sealed class Resource<T> (var data: T? = null, var message: String? = null) {
    class Loading<T>(nothing: Nothing?, s: String) : Resource<T>()
    class Success<T>(data: T? = null) : Resource<T>(data)
    class Error<T>(data: T?, message: String?) : Resource<T>(data, message)
}
