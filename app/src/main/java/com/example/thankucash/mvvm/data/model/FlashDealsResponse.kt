package com.example.thankucash.mvvm.data.model

class FlashDealsResponse(
    val Status: String,
    val Message: String,
    val Result: Rest,
    val ResponseCode: String? =null,
    val Mode: String? = null
)

data class Rest (
    val Offset: Long,
    val Limit: Long,
    val TotalRecords: Long,
    val Data: List<Datum?>
)