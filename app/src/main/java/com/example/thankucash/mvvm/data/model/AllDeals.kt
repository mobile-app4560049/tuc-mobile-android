package com.example.thankucash.mvvm.data.model

data class AllDeals(
    val Task: String,
    val Offset: Int,
    val Limit: Int,
    val SearchCondition: String,
    val SortExpression: String,
    val Categories: Array<String>,
    val Merchants: Array<String>,
    val City: String,
    val CityId: Int,
    val CityKey: String,
    val Latitude: Int,
    val Longitude: Int,
    val CountryCode: Int,
    val CountryKey: String
)
