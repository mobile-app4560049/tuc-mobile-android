package com.example.thankucash.mvvm.data.model

data class FlashDeals(
    val Task: String,
    val Offset: Int,
    val Limit: Int,
    val SearchCondition: String,
    val SortExpression: String,
    val Categories: Array<String>,
    val Merchants: Array<String>,
    val City: String,
    val Latitude: Int,
    val Longitude: Int,
    val CountryCode: Int,
    val CountryKey: String
)
