package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.DataPlansItemBinding
import com.example.thankucash.mvvm.data.model.listModel.DataPlansItemList

class DataPlanItemAdapter(
    private val onClick:(item: DataPlansItemList) -> Unit
): RecyclerView.Adapter<DataPlanItemAdapter.DataItemViewHolder>() {
    private var dataItemList: ArrayList<DataPlansItemList> = arrayListOf()
    private var selectedPos = -1

     inner class DataItemViewHolder(val binding: DataPlansItemBinding): RecyclerView.ViewHolder(binding.root) {
         var cardView = binding.cardViewData
         var constraintLayout = binding.constraint

         fun bind(dataItem: DataPlansItemList) {
             binding.walletTopText.text = dataItem.dataAmount
             binding.earnText.text = dataItem.earn
             binding.amount.text = dataItem.amount.toString()
             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }

             binding.cardViewData.setOnClickListener { onClick(dataItem) }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataItemViewHolder {
          val inflater = DataPlansItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return DataItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: DataItemViewHolder, @SuppressLint("RecyclerView") position: Int) {
          holder.bind(dataItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int = dataItemList.size
     fun submitList (list: ArrayList<DataPlansItemList>){
         dataItemList = list
         notifyDataSetChanged()
     }

    companion object {
        private const val TAG = "AirtimeAdapter"
    }
}