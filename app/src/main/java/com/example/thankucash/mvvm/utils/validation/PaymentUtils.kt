package com.example.thankucash.mvvm.utils.validation

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import com.flutterwave.raveandroid.RaveUiManager
import java.time.temporal.TemporalAmount


fun beginFlutterwaveTransaction(
    context: Activity,
    firstName: String,
    lastName: String,
    email: String,
    narration: String,
    transactionReference: String,
    amount: String,

    ){
    amount.toDoubleOrNull()?.let {
        RaveUiManager(context).setAmount(it)
        .setCurrency("NG")
        .setEmail("user@example.com")
        .setfName("Peter")
        .setlName("Eze")
        .setNarration("naration")
        .setPublicKey("FLWPUBK_TEST-a6e984618172d190fd60d61b15673910-X")
        .setTxRef("Transaction Reference")
        .acceptBankTransferPayments(true)
        .acceptUssdPayments(true)
        .acceptCardPayments(true)
        .initialize()
    }
}
