package com.example.thankucash.mvvm.presentation.ui.screens.rewards

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentRewardsBinding
import com.example.thankucash.mvvm.presentation.adapters.DealItemsAdapter
import com.example.thankucash.mvvm.presentation.adapters.RewardsPlanListAdapter
import com.example.thankucash.mvvm.presentation.viewmodel.TabStateViewModel
import com.example.thankucash.mvvm.utils.validation.Constant

class RewardsFragment : Fragment() {
    private var _binding: FragmentRewardsBinding? = null
    private val binding get() = _binding!!
    lateinit var rewardsPlanListAdapter: RewardsPlanListAdapter
    private val tabStateViewModel : TabStateViewModel by activityViewModels()
    private val shareViewModel: ShareViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRewardsBinding.inflate(inflater, container, false)
         return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rewardsPlanListAdapter = RewardsPlanListAdapter{
            shareViewModel.rewards = it
            findNavController().navigate(R.id.historyRewardsFragment2)
        }


        binding.recyclerViewRewards.apply {
            adapter = rewardsPlanListAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
        rewardsPlanListAdapter.submitList(Constant.rewardList)


        binding.apply {
            rewardsHistory.setOnClickListener {
                tabStateViewModel.shouldNavigateRewardHistory = true
                findNavController().navigate(R.id.viewPagerTwoFragment)
            }
                backArrow.setOnClickListener { activity?.onBackPressed() }
        }
    }
}