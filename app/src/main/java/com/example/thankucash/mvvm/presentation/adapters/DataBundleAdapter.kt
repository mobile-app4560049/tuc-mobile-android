package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.DataBundleListItemBinding
import com.example.thankucash.mvvm.data.model.listModel.DataBundleListItem

class DataBundleAdapter(
    private val onClick:(item: DataBundleListItem) -> Unit
): RecyclerView.Adapter<DataBundleAdapter.DataBundleItemViewHolder>() {
    private var dataBundleItemList: ArrayList<DataBundleListItem> = arrayListOf()
    private var selectedPos = -1

     inner class DataBundleItemViewHolder(val binding: DataBundleListItemBinding): RecyclerView.ViewHolder(binding.root) {
         var cardView = binding.cardView
         var constraintLayout = binding.constraint

         fun bind(dataBundleItem: DataBundleListItem) {
             binding.walletTopText.text = dataBundleItem.rewardText
             binding.valueEarn.text =  dataBundleItem.stationName
             binding.bundleDate.text = dataBundleItem.date
             binding.calendarBundle.setImageResource( dataBundleItem.calendarImage)
             binding.bundleTime.text = dataBundleItem.time2
             binding.amount.text =  dataBundleItem.amount


             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 notifyItemChanged(selectedPos)
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }
             binding.cardView.setOnClickListener { onClick(dataBundleItem) }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBundleItemViewHolder {
          val inflater = DataBundleListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return DataBundleItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: DataBundleItemViewHolder, @SuppressLint("RecyclerView") position: Int) {
          holder.bind(dataBundleItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int = dataBundleItemList.size
     fun submitList (list: ArrayList<DataBundleListItem>){
         dataBundleItemList = list
         notifyDataSetChanged()
     }

    companion object {
        private const val TAG = "AirtimeAdapter"
    }
}