package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.Datum
import com.example.thankucash.mvvm.data.model.FlashDeals
import com.example.thankucash.mvvm.data.model.FlashDealsResponse
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FlashDealsViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel(){
    private var _flashDealsResponse = MutableLiveData<Resource<FlashDealsResponse>>()
    val flashDealsResponse: MutableLiveData<Resource<FlashDealsResponse>> get() = _flashDealsResponse
    var cachedTopDeals: List<Datum> = listOf()

    var searchQuery = MutableLiveData<String>("")

    fun getFlashDeals(flashDeals: FlashDeals){
        _flashDealsResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch (Dispatchers.Default){
            try {
                val response = authRepository.getFlashDeals(flashDeals)
                _flashDealsResponse.postValue(Resource.Success(response))
            }catch (e: Exception){
                Log.d("aaaaa", e.javaClass.simpleName)
            }
        }
    }
}