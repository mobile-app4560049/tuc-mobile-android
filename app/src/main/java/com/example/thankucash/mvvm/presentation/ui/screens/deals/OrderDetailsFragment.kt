package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentOrderDetailsBinding
import com.example.thankucash.mvvm.presentation.adapters.DealsTabLayoutAdapter
import com.example.thankucash.mvvm.presentation.adapters.TopDealsAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OrderDetailsFragment : Fragment() {
    private var _binding : FragmentOrderDetailsBinding? = null
    private val binding get() = _binding!!
    private val shareViewModel: ShareViewModel by activityViewModels()
    lateinit var topDealsAdapter: TopDealsAdapter

    private val tabTitles = arrayListOf("About", "Redeem Locations", "Terms & Condition")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentOrderDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.beforAmount.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        val model = shareViewModel
        if (model.topDeal != null) {
            var difference = shareViewModel.topDeal?.ActualPrice!! - shareViewModel.topDeal?.SellingPrice!!
            var percentage = difference / shareViewModel.topDeal?.ActualPrice!! * 100
            binding.percentOff.text = "${percentage}%"
            binding.itemName.text = "NGN ${model.topDeal?.Title}"
            binding.beforAmount.text = "${model.topDeal ?.ActualPrice}"
            binding.addOrderDetailsBtn.text = "Add ${binding.increase.text} for ${model.topDeal?.ActualPrice}"
            Glide.with(this)
                .load(model.topDeal?.ImageUrl)
                .placeholder(R.drawable.deals_title_icon)
                .into(binding.itemImage)
            binding.amountText.text = "${model.topDeal ?.SellingPrice}"
        }



        binding.addOrderDetailsBtn.setOnClickListener { findNavController().navigate(R.id.checkOutProductFragment) }


        binding.viewpager.adapter = DealsTabLayoutAdapter(this)
        setUpTabLayoutWitViewPager()


        binding.apply {
            orderDetailsArrowBack2.setOnClickListener { activity?.onBackPressed() }
            addOrderDetailsBtn.text = "Add ${binding.increase.text} for ${model.topDeal?.SellingPrice}"
        }
        binding.apply {
                cardViewAdd.setOnClickListener {
                    val currentValue = binding.increase.text.toString().toInt()
                    val newValue = currentValue + 1
                    binding.increase.text = newValue.toString()
                    binding.addOrderDetailsBtn.text = "Add ${binding.increase.text} for ${model.topDeal?.SellingPrice}"
                }
                cardViewSubtract.setOnClickListener {
                    val currentValue =  binding.increase.text.toString().toInt()
                    val newValue = currentValue - 1
                    binding.increase.text = newValue.toString()
                    binding.addOrderDetailsBtn.text = "Add ${binding.increase.text} for ${model.topDeal?.SellingPrice}"
                }
            }

    }

    private fun setUpTabLayoutWitViewPager() {
        binding.viewpager.apply {
            val tab = binding.tabLayoutDetails.getTabAt(1)
            tab?.select()
            TabLayoutMediator(
                binding.tabLayoutDetails,
                binding.viewpager
            ) { tab, position ->
                tab.text = tabTitles[position]
            }.attach()
        }
        binding.tabLayoutDetails.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }


}

