package com.example.thankucash.mvvm.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.databinding.AirtimeListBinding
import com.example.thankucash.databinding.DataHistoryItemBinding
import com.example.thankucash.databinding.DealsItemListBinding
import com.example.thankucash.databinding.FlashDealsItemBinding
import com.example.thankucash.databinding.UtilityListItemsBinding
import java.nio.file.Files.size

class RewardsAdapter: RecyclerView.Adapter<RewardsAdapter.AirtimeItemViewHolder>() {
    private var rewardsItemList: ArrayList<String> = arrayListOf()

     class AirtimeItemViewHolder(val binding: DealsItemListBinding): RecyclerView.ViewHolder(binding.root) {
         fun bind(airtimeItemList: ArrayList<String>) {
        binding.dealsText.text = airtimeItemList[0]
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirtimeItemViewHolder {
          val inflater = DealsItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return AirtimeItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: AirtimeItemViewHolder, position: Int) {
          holder.bind(rewardsItemList)
     }

     override fun getItemCount(): Int = rewardsItemList.size
     fun submitList (list: ArrayList<String>){
         rewardsItemList = list
     }
}