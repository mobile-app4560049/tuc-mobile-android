package com.example.thankucash.mvvm.data.repository


import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.data.model.UpdateProfileResponse
import com.example.thankucash.mvvm.data.model.UpdateUserAccount
import com.example.thankucash.mvvm.data.network.ThankUCashApi
import com.example.thankucash.mvvm.utils.validation.ApiCallHandler
import com.example.thankucash.mvvm.utils.validation.Resource
import javax.inject.Inject

class AuthRepository @Inject constructor(private val api: ThankUCashApi) {
    suspend fun loginUser(userLogin: UserLogin): Resource<GenericResult<UserLoginResponse>>{
        return ApiCallHandler.safeApiCall {
            api.loginUser(userLogin)
        }
    }

    suspend fun userRegistration(userRegistration: UserRegistration): Resource<GenericResult<UserRegistrationResponse>>{
        return ApiCallHandler.safeApiCall {
            api.registerUser(userRegistration)
        }
    }

    suspend fun userVerification(userVerification: UserVerification): Resource<GenericResult<UserVerifyPhoneResponse>>{
        return ApiCallHandler.safeApiCall {
            api.verifyUser(userVerification)
        }
    }

    suspend fun resetUserPin(resetPin: ResetPin): Resource<Int>{
        return ApiCallHandler.safeApiCall {
            api.resetPin(resetPin)
        }
    }
    suspend fun updatePin(updatePin: UpdatePin): Resource<Int>{
        return ApiCallHandler.safeApiCall {
            api.updatePin(updatePin)
        }
    }

    suspend fun updateUserProfile(updateUserAccount: UpdateUserAccount): Resource<GenericResult<UpdateProfileResponse>>{
        return ApiCallHandler.safeApiCall {
            api.updateUserProfile(updateUserAccount)
        }
    }
    suspend fun referralProfile(referralProfile: ReferralProfile): Resource<GenericResult<ReferralProfileResponse>>{
        return ApiCallHandler.safeApiCall {
            api.referralProfile(referralProfile)
        }
    }
    suspend fun changePin(changePin: ChangePin): Resource<GenericResult<ChangePinResponse>>{
        return ApiCallHandler.safeApiCall {
            api.changePin(changePin)
        }
    }

    suspend fun requestOtp(requestOtp: RequestOtp): Resource<GenericResult<RequestOtpResponse>>{
        return ApiCallHandler.safeApiCall {
            api.requestOtp(requestOtp)
        }
    }
    suspend fun getUserAccount(getUserAccount: GetUserAccount): Resource<GenericResult<UpdateProfileResponse>>{
        return ApiCallHandler.safeApiCall {
            api.getUserAccount(getUserAccount)
        }
    }

    suspend fun topDeals(topDeals: TopDeals): TopDealsResponse {
        return api.getTopDeals(topDeals)
    }

    suspend fun getFlashDeals(flashDeals: FlashDeals): FlashDealsResponse {
           return api.getFlashDeals(flashDeals)
    }

    suspend fun getAllDeals(allDeals: AllDeals): AllDealsResponse{
        return api.getAllDeals(allDeals)
    }

    suspend fun buyDeals(buyDeals: BuyDeals): BuyDealsResponse{
       return api.buyDeals(buyDeals)

    }

    suspend fun buyDealsConfirms(confirmUserDeals: ConfirmUserDeals): BuyDealsResponse{
        return api.dealsConfirm(confirmUserDeals)
    }

    suspend fun getDealsCode(getDealCode: GetDealCode): GetDealCodeResponse {
        return api.getDealCode(getDealCode)
    }
    suspend fun buyPointsInitialized(buyPointsPayment: BuyPointsPayment): BuyPointsResPonse{
        return api.buyPointsInitialized(buyPointsPayment)
    }

    suspend fun buyPointsVerify(buyPointsVerify: BuyPointsVerify): BuyPointsVerifyResponse{
        return api.buyPointsVerify(buyPointsVerify)
    }

    suspend fun buyDealsRegister(buyDealsRegisterUser: BuyDealsRegisterUser) : RegisterUserResponse{
        return api.buyDealsRegisterUser(buyDealsRegisterUser)
    }
}