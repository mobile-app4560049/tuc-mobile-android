package com.example.thankucash

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import co.paystack.android.PaystackSdk
import com.example.thankucash.databinding.ActivityMainBinding
import com.example.thankucash.mvvm.utils.validation.ConnectivityLiveData
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    val payStackKey = "pk_test_9eb0263ed776c4c892e0281348aee4136cd0dd52"
    lateinit var connectivityLiveData: ConnectivityLiveData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCenter.start(
            application,
            "458920ab-9a96-40c2-982e-059d1c4abb6c",
            Analytics::class.java,
            Crashes::class.java
        )

    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        supportActionBar?.hide()

        connectivityLiveData = ConnectivityLiveData(this)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //set the theme
        setTheme(R.style.Theme_ThankUCash)

        PaystackSdk.initialize(applicationContext)
            PaystackSdk.setPublicKey(payStackKey)


        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController


//

        binding.bottomNavigationView.setupWithNavController(navController)

        disableBottomNavMenu(navController)
    }
        private fun disableBottomNavMenu(navController: NavController) {
            navController.addOnDestinationChangedListener { _, destination, _ ->
                if (
                    destination.id == R.id.fundWalletDashBoardFragment2 || destination.id == R.id.allDealsFragment ||
                    destination.id == R.id.flashDealsFragment || destination.id == R.id.top_deals || destination.id == R.id.topFlashDealsFragment ||
                            destination.id == R.id.dealsTitleHistoryFragment2

                ) {
                    binding.bottomNavigationView.visibility = View.VISIBLE
                } else {
                    binding.bottomNavigationView.visibility = View.GONE
                }
            }
        }
}