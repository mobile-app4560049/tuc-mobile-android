package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.ListItemBinding
import com.example.thankucash.mvvm.data.model.listModel.WalletItem

class WalletPlanItemsAdapter(
    private val onClick:(item: WalletItem) -> Unit
): RecyclerView.Adapter<WalletPlanItemsAdapter.WalletItemViewHolder>() {
    private var walletItemList: ArrayList<WalletItem> = arrayListOf()
    private var selectedPos = -1

     inner class WalletItemViewHolder(val binding: ListItemBinding): RecyclerView.ViewHolder(binding.root) {
         var cardView = binding.cardView
         var constraintLayout = binding.constraint

         fun bind(airtime: WalletItem) {
             binding.walletTopText.text = airtime.rewardText
             binding.paystack.text = airtime.stationName
             binding.date.text = airtime.date
             binding.calendar.setImageResource(airtime.calendarImage)
             binding.time.text = airtime.time2
             binding.amount.text = airtime.amount


             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 notifyItemChanged(selectedPos)
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WalletItemViewHolder {
          val inflater = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return WalletItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder:WalletItemViewHolder, @SuppressLint("RecyclerView") position: Int) {
          holder.bind( walletItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int =  walletItemList.size
     fun submitList (list: ArrayList<WalletItem>){
         walletItemList= list
         notifyDataSetChanged()
     }

    companion object {
        private const val TAG = "AirtimeAdapter"
    }
}