package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.thankucash.R

class GenderAdapter(context: Context): ArrayAdapter<String>(context, android.R.layout.simple_spinner_item) {

    init {
        val genderOptions = context.resources.getStringArray(R.array.gender_options)
        addAll(*genderOptions)
    }

    @SuppressLint("SuspiciousIndentation")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
      val view = super.getView(position, convertView, parent)
        view.findViewById<TextView>(android.R.id.text1)?.setTextColor(ContextCompat.getColor(context,android.R.color.black))
       return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getDropDownView(position, convertView, parent)
        view.findViewById<TextView>(android.R.id.text1)?.setTextColor(ContextCompat.getColor(context,android.R.color.black))
        return view
    }
}