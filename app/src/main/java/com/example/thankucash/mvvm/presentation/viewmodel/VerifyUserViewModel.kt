package com.example.thankucash.mvvm.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.GenericResult
import com.example.thankucash.mvvm.data.model.UserVerification
import com.example.thankucash.mvvm.data.model.UserVerifyPhoneResponse
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class VerifyUserViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _verifyUserResponse = MutableLiveData<Resource<GenericResult<UserVerifyPhoneResponse>>>()
    val verifyUserResponse: LiveData<Resource<GenericResult<UserVerifyPhoneResponse>>> get() = _verifyUserResponse

    var userPhoneNumber:String? = null

    fun verifyUser(userVerification: UserVerification) {
        _verifyUserResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch {
            _verifyUserResponse.value = authRepository.userVerification(userVerification)
        }
    }
}