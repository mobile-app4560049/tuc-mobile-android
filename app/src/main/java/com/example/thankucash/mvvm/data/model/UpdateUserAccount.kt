package com.example.thankucash.mvvm.data.model

data class UpdateUserAccount(
    var authAccountId : Int,
    val firstName: String,
    val middleName : String,
    val lastName: String,
    val emailAddress: String,
    val address : String,
    val gender : String,
    val referralCode : String? = null
)
