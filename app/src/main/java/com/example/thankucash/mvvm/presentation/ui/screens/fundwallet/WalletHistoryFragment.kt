package com.example.thankucash.mvvm.presentation.ui.screens.fundwallet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentWalletHistoryBinding
import com.example.thankucash.mvvm.presentation.adapters.RewardsPlanListAdapter
import com.example.thankucash.mvvm.presentation.adapters.ViewPagerAdapter2
import com.example.thankucash.mvvm.presentation.adapters.WalletPlanItemsAdapter
import com.example.thankucash.mvvm.utils.validation.Constant
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class WalletHistoryFragment : Fragment() {
    private var _binding: FragmentWalletHistoryBinding? = null
    private val binding get() = _binding!!
    lateinit var walletPlanItemsAdapter: WalletPlanItemsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentWalletHistoryBinding.inflate(inflater, container, false)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        walletPlanItemsAdapter = WalletPlanItemsAdapter {
            findNavController().popBackStack()
        }


        binding.walletRecyckerView.apply {
            adapter = walletPlanItemsAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
        walletPlanItemsAdapter.submitList(Constant.walletTop)

    }

}