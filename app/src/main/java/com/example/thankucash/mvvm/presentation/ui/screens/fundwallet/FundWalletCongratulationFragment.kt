package com.example.thankucash.mvvm.presentation.ui.screens.fundwallet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.thankucash.databinding.FragmentFundWalletCongratulationBinding
import dagger.hilt.android.AndroidEntryPoint

//import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FundWalletCongratulationFragment : Fragment() {
    private var _binding: FragmentFundWalletCongratulationBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFundWalletCongratulationBinding.inflate(inflater, container, false)
        return binding.root
    }
}