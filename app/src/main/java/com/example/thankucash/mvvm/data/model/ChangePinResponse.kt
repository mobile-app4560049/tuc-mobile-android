package com.example.thankucash.mvvm.data.model

data class ChangePinResponse(
val Status: String,

val Message: String,

val ResponseCode: String,

val Mode: String

)
