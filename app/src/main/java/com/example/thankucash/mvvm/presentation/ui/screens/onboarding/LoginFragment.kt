package com.example.thankucash.mvvm.presentation.ui.screens.onboarding

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentLoginBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.Country
import com.example.thankucash.mvvm.data.model.UserLogin
import com.example.thankucash.mvvm.presentation.viewmodel.LoginViewModel
import com.example.thankucash.mvvm.utils.validation.*
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class LoginFragment : Fragment() {
 private var _binding : FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var dialog: Dialog
 private val loginViewModel: LoginViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
            validateField()
        }


        val ccp = binding.countryPicker
        val phone = binding.numberInput
        ccp.registerCarrierNumberEditText(phone)


        binding.apply {
            forgottenPinTextview.setOnClickListener { findNavController().navigate(R.id.forgotPinFragment) }
            signUpLoginTextView.setOnClickListener { findNavController().navigate(R.id.signUpFragment) }
            loginArrowBack.setOnClickListener { activity?.onBackPressed() }
        }


        binding.loginScreenButton.setOnClickListener {
          validatingCountryInput()
            var login = UserLogin(
                binding.numberInput.toString(),
                binding.pinInputLayout.toString(),
                CountryCode.NGN.countries.countryIsd
            )
            loginUser(login)
        }
    }

    private fun validateField() {
        val fieldTypesToValidate = listOf(
        FieldValidationTracker.FieldType.PIN,
        FieldValidationTracker.FieldType.PHONE_NUMBER,
        )
        FieldValidationTracker.populateFieldTypeMap(fieldTypesToValidate)

        binding.apply {
            pinInputLayout.validateField(
                "Incorrect Pin"
            ) { input ->
                FieldValidations.verifyPin(input)
            }
        }

        binding.loginScreenButton.observeFieldsValidationToEnableButton(
            requireContext(),
            viewLifecycleOwner
        )

    }


    private fun loginUser(userLogin: UserLogin){
        lifecycleScope.launch{
            loginViewModel.loginUser(userLogin)
        }
    }

    private fun setUpObservers() {
        loginViewModel.loginResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    val token = it.data?.result.toString()
                    SessionManager.saveToSharedPref(requireContext(), SessionManager.TOKEN, token)
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_success)
                    findNavController().navigate(R.id.fund_wallet_nav)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }

    }

    private fun validatingCountryInput() {
        val countryCode = binding.countryPicker.toString()
        val phoneNumber = binding.numberInput.toString()
        val errorMessage = FieldValidations.validatePhoneNumberFormat(countryCode, phoneNumber)
        if (errorMessage != null) {
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "Phone Number format is valid", Toast.LENGTH_SHORT)
                .show()
        }

        val phoneTextView = binding.phoneNumberTextView
        val numberInput = binding.numberInput
        numberInput.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val enteredPin = numberInput.text.toString()
                val correctPin = "1234"
                if (enteredPin == correctPin) {
                    true
                } else {
                    phoneTextView.text = "Phone number not recognized *"
                    phoneTextView.setTextColor(Color.RED)
                    true
                }
            } else {
                false
            }
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}