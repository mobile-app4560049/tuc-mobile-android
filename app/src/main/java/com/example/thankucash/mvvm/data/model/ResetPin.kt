package com.example.thankucash.mvvm.data.model

data class ResetPin(
    var task: String,
    var countryIsd : String,
    var mobileNumber: String,
    var countryId : Int,
    var countryKey : String
)
