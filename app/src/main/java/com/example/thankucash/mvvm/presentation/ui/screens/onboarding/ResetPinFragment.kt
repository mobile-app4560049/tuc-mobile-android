package com.example.thankucash.mvvm.presentation.ui.screens.onboarding

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentResetPinBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.UpdatePin
import com.example.thankucash.mvvm.presentation.viewmodel.ForgotPinViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.VerifyUserViewModel
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class ResetPinFragment : Fragment() {
    private var _binding : FragmentResetPinBinding? = null
    private val binding get() = _binding!!
    private val forgotPinViewModel: ForgotPinViewModel by activityViewModels()

    private val verifyUserViewModel : VerifyUserViewModel by activityViewModels()
    private lateinit var dialog: Dialog


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentResetPinBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }
            binding.iresetPinBackArrow.setOnClickListener { activity?.onBackPressed() }

        binding.verificationCodeText.text = "Enter the temporary 4 digit pin sent to ${verifyUserViewModel.userPhoneNumber} \\n and your new PIN"

        binding.resetPinBtn.setOnClickListener {
            val user = UpdatePin(
                TaskEnum.UPDATEPIN.taskType,
                CountryCode.NGN.countries.countryIsd,
                binding.verificationCodeText.text.toString(),
                binding.tempNumberText.toString(),
                binding.newNumberPin.toString(),
                CountryCode.NGN.countries.countryCode,
                CountryCode.NGN.countries.countryKey
            )
            updatePin(user)
        }
    }


    private fun updatePin(updatePin: UpdatePin){
        lifecycleScope.launch{
            forgotPinViewModel.updateUserPin(updatePin)
        }
    }


    private fun setUpObservers() {
        forgotPinViewModel.updatePinResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    findNavController().popBackStack()
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }

    }

}