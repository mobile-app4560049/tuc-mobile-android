package com.example.thankucash.mvvm.presentation.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class StateAdapter(context: Context) :
    ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item) {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        val holder: ViewHolder
        if (view == null) {
            view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)
            holder = ViewHolder(view!!)
            view.tag = holder
        } else {
            holder = view.tag as ViewHolder
        }

        holder.stateTextView.text = getItem(position)

        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        val holder: ViewHolder
        if (view == null) {
            view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)
            holder = ViewHolder(view!!)
            view.tag = holder
        } else {
            holder = view.tag as ViewHolder
        }

        holder.stateTextView.text = getItem(position)

        return view
    }

    private class ViewHolder(view: View) {
        val stateTextView: TextView = view.findViewById(android.R.id.text1)
    }
}