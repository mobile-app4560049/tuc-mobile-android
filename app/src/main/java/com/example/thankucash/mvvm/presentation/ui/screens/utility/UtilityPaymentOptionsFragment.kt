package com.example.thankucash.mvvm.presentation.ui.screens.utility

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentUtilityPaymentOptionsBinding
import com.example.thankucash.mvvm.presentation.ui.screens.fundwallet.SelectedPaymentOption
import com.example.thankucash.mvvm.utils.validation.beginFlutterwaveTransaction

class UtilityPaymentOptionsFragment : Fragment() {
    private var _binding : FragmentUtilityPaymentOptionsBinding? = null
    private val binding get() = _binding!!
    private var selectedPaymentOption: SelectedPaymentOption = SelectedPaymentOption.NONE
    private var selectedLayout: ConstraintLayout? = null
    private var selectedCheckout: RadioButton? = null
    private val shareViewModel: ShareViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUtilityPaymentOptionsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = shareViewModel
        if (model.method  != null ){
            binding.textView7.text = "${model.method?.amount}"
        }
        binding.dataPlanPaymentOptionsArrowBack2.setOnClickListener { activity?.onBackPressed() }

        binding.buyAirtimeBtn.setOnClickListener { checkout() }

        binding.ussdLayout.setOnClickListener {
            isNotSelected()
            isSelected(binding.radioButtonUssd, binding.ussdLayout)
            selectedLayout = binding.ussdLayout
            selectedCheckout = binding.radioButtonUssd
        }
        binding.layoutCard.setOnClickListener {
            isNotSelected()
            isSelected(binding.radioButtonBank, binding.ussdLayout)
            selectedLayout = binding.transferInputLayout
            selectedCheckout = binding.radioButtonBank
        }
        binding.transferInputLayout.setOnClickListener {
            isNotSelected()
            isSelected(binding.radioButtonTrannsfer, binding.transferInputLayout)
            selectedLayout = binding.transferInputLayout
            selectedCheckout = binding.radioButtonTrannsfer
        }
    }

    fun isSelected(radioButton: RadioButton, view: ConstraintLayout) {
        view.setBackgroundDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.green_stroke_128dp_corners
            )
        )
        radioButton.isChecked = true
        radioButton.background = ContextCompat.getDrawable(
            requireContext(),
            R.drawable.custom_icon
        )

    }


    fun isNotSelected(){
        selectedLayout?.setBackgroundDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.grey_stroke_128dp_corners
            )
        )
        selectedCheckout?.isChecked = false
        selectedCheckout?.background = null
    }

    private fun checkout() {

        when(selectedPaymentOption){
            SelectedPaymentOption.PAYSTACK -> {

            }
            SelectedPaymentOption.FLUTTERWAVE ->{
                val amount = binding.textView7.text
                val re = Regex("[^0-9]")
                val formatAmount = re.replace(amount,"")
                Log.d("XXXX", "formatAmount")
                beginFlutterwaveTransaction(requireActivity(), "Peter", "Eze", "user@example.com","naration","Transaction Reference","500")
            }

            else ->{}

        }


    }


}
