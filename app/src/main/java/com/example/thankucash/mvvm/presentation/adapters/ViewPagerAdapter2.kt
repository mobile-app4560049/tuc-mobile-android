package com.example.thankucash.mvvm.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.thankucash.mvvm.presentation.ui.screens.dataplans.HistoryDataFragment
import com.example.thankucash.mvvm.presentation.ui.screens.fundwallet.FundWalletFragment
import com.example.thankucash.mvvm.presentation.ui.screens.fundwallet.FundWalletHistoryFragment
import com.example.thankucash.mvvm.presentation.ui.screens.fundwallet.WalletHistoryFragment
import com.example.thankucash.mvvm.presentation.ui.screens.rewards.DealsHistoryFragment
import com.example.thankucash.mvvm.presentation.ui.screens.rewards.RewardsHistoryFragment

class ViewPagerAdapter2(fragment: Fragment): FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = 4

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> HistoryDataFragment()
            1 -> RewardsHistoryFragment()
            2 -> WalletHistoryFragment()
            else -> DealsHistoryFragment()
        }
    }
}