package com.example.thankucash.mvvm.presentation.ui.screens.utility

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentDstvPlansBinding
import com.example.thankucash.mvvm.presentation.adapters.DataPlanItemAdapter
import com.example.thankucash.mvvm.presentation.adapters.DstvPlanItemAdapter
import com.example.thankucash.mvvm.utils.validation.Constant
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DstvPlansFragment : Fragment() {
    private var _binding : FragmentDstvPlansBinding? = null
    private val binding get() = _binding!!
    lateinit var dstvPlansItemAdapter: DstvPlanItemAdapter
    private val shareViewModel: ShareViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDstvPlansBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dstvPlansItemAdapter = DstvPlanItemAdapter{
            shareViewModel.method = it
            findNavController().popBackStack()
        }


        binding.recyclerViewDstv.apply {
            adapter = dstvPlansItemAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL,false
            )
        }
        dstvPlansItemAdapter.submitList(Constant.dstvPlansList)

    }

}