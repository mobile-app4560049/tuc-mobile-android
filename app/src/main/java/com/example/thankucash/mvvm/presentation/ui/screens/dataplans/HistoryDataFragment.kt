package com.example.thankucash.mvvm.presentation.ui.screens.dataplans

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentHistoryDataBinding
import com.example.thankucash.mvvm.presentation.adapters.AirtimePlanListAdapter
import com.example.thankucash.mvvm.presentation.adapters.TopUpAdapter
import com.example.thankucash.mvvm.utils.validation.Constant

//@AndroidEntryPoint
class HistoryDataFragment : Fragment() {
    private var _binding : FragmentHistoryDataBinding? = null
    private val binding get() = _binding!!
    lateinit var airtimePlanListAdapter: AirtimePlanListAdapter

    private val shareViewModel: ShareViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHistoryDataBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        airtimePlanListAdapter = AirtimePlanListAdapter{
                shareViewModel.airtime = it
            findNavController().navigate(R.id.dataBundleHistoryFragment)
        }

        binding.recyclerVieweeee.apply {
            adapter = airtimePlanListAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        }
        airtimePlanListAdapter.submitList(Constant.airtimeListUser)
    }
}