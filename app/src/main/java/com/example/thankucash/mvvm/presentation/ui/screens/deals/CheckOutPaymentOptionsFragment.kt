package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentCheckOutPaymentOptionsBinding
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.BuyPointsPayment
import com.example.thankucash.mvvm.data.model.ConfirmUserDeals
import com.example.thankucash.mvvm.presentation.ui.screens.fundwallet.SelectedPaymentOption
import com.example.thankucash.mvvm.presentation.viewmodel.BuyDealsViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.BuyPointsViewModel
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.beginFlutterwaveTransaction
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import com.flutterwave.raveandroid.RavePayActivity
import com.flutterwave.raveandroid.rave_java_commons.RaveConstants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.text.NumberFormat
import java.util.*

@AndroidEntryPoint
class CheckOutPaymentOptionsFragment : Fragment() {
    private var _binding: FragmentCheckOutPaymentOptionsBinding? = null
    private val binding get() = _binding!!
    private var selectedPaymentOption: SelectedPaymentOption = SelectedPaymentOption.NONE
    private lateinit var dialog: Dialog
    private var selectedLayout: ConstraintLayout? = null
    private var selectedCheckout: RadioButton? = null
    private val shareViewModel: ShareViewModel by activityViewModels()
    private val buyPointsViewModel: BuyPointsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCheckOutPaymentOptionsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = shareViewModel
        if (model.topDeal != null) {
            val ngnFormat = NumberFormat.getCurrencyInstance(Locale("en", "NG"))
            binding.amount.text = ngnFormat.format(model.topDeal?.SellingPrice)
        }
        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }


        binding.checkOutBtn.setOnClickListener {
            checkout()
        }


        binding.flutterWaveInputLayout.setOnClickListener {
            isNotSelected()
            isSelected(binding.radioButtonFlutter, binding.flutterWaveInputLayout)
            selectedLayout = binding.flutterWaveInputLayout
            selectedCheckout = binding.radioButtonFlutter
        }
        binding.payStackLayout.setOnClickListener {
            isNotSelected()
            isSelected(binding.radioButtonPaystack, binding.payStackLayout)
            selectedLayout = binding.payStackLayout
            selectedCheckout = binding.radioButtonPaystack
        }

        var paymentInitialize = BuyPointsPayment(
            TaskEnum.BUYDEALCONFIRMS.taskType,
            "3286b67672ff4216864b248ce7e2f489",
            20000,
            ""
        )
        buyPoints(paymentInitialize)

    }

    fun isSelected(radioButton: RadioButton, view: ConstraintLayout) {
        view.setBackgroundDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.green_stroke_128dp_corners
            )
        )
        radioButton.isChecked = true
        radioButton.background = ContextCompat.getDrawable(
            requireContext(),
            R.drawable.custom_icon
        )
        selectedPaymentOption = when (radioButton) {
            binding.radioButtonFlutter -> SelectedPaymentOption.FLUTTERWAVE
            binding.radioButtonPaystack -> SelectedPaymentOption.PAYSTACK
            else -> SelectedPaymentOption.NONE
        }
    }


    fun isNotSelected(){
        selectedLayout?.setBackgroundDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.grey_stroke_128dp_corners
            )
        )
        selectedCheckout?.isChecked = false
        selectedCheckout?.background = null
        selectedPaymentOption = SelectedPaymentOption.NONE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        /*
         *  We advise you to do a further verification of transaction's details on your server to be
         *  sure everything checks out before providing service or goods.
        */
        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            val message = data.getStringExtra("response")
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                Toast.makeText(requireContext(), "SUCCESS $message", Toast.LENGTH_SHORT).show()
            } else if (resultCode == RavePayActivity.RESULT_ERROR) {
                Toast.makeText(requireContext(), "ERROR $message", Toast.LENGTH_SHORT).show()
            } else if (resultCode == RavePayActivity.RESULT_CANCELLED) {
                Toast.makeText(requireContext(), "CANCELLED $message", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    private fun checkout() {
        when(selectedPaymentOption){
            SelectedPaymentOption.PAYSTACK -> {
                Log.d("paystack","${SelectedPaymentOption.PAYSTACK}")
                findNavController().navigate(R.id.paystackFragment2)
            }
            SelectedPaymentOption.FLUTTERWAVE ->{
                val amount = binding.amount.text
                val re = Regex("[^0-9]")
                val formatAmount = re.replace(amount,"")
                beginFlutterwaveTransaction(requireActivity(), "Peter", "Eze", "user@example.com","naration","Transaction Reference", formatAmount)
            }

            else ->{}

        }
    }

    private fun buyPoints(buyPointsPayment: BuyPointsPayment) {
        lifecycleScope.launch {
            buyPointsViewModel.buyPoint(buyPointsPayment)
        }
    }

    private fun setUpObservers() {
        buyPointsViewModel.buyPointsResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {

                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_success)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
    }
}


enum class SelectedPaymentOption{
    FLUTTERWAVE,
    PAYSTACK,
    NONE
}