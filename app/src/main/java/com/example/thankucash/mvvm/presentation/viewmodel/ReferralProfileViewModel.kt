package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.GenericResult
import com.example.thankucash.mvvm.data.model.ReferralProfile
import com.example.thankucash.mvvm.data.model.ReferralProfileResponse
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReferralProfileViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel(){
    private var _referralProfileResponse = MutableLiveData<Resource<GenericResult<ReferralProfileResponse>>>()
    val referralProfileResponse: LiveData<Resource<GenericResult<ReferralProfileResponse>>> get() = _referralProfileResponse


    fun referralProfile(referralProfile: ReferralProfile){
        _referralProfileResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch(Dispatchers.Default) {
            try {
                _referralProfileResponse.value = authRepository.referralProfile(referralProfile)
            }catch (e:Exception){
                Log.d("Is Loading", e.javaClass.simpleName)
            }
        }
    }
}