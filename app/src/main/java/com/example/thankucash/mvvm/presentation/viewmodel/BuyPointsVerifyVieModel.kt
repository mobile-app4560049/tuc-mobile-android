package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BuyPointsVerifyVieModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _buyPointsVerifyResponse = MutableLiveData<Resource<BuyPointsVerifyResponse>>()
    val buyPointsVerifyResponse: LiveData<Resource<BuyPointsVerifyResponse>> get() = _buyPointsVerifyResponse

    fun buyPointsVerify(buyPointsVerify: BuyPointsVerify){
        _buyPointsVerifyResponse.postValue(Resource.Loading(null,"Loading..."))
        viewModelScope.launch(Dispatchers.Main) {
            try {
                val response = authRepository.buyPointsVerify(buyPointsVerify)
                _buyPointsVerifyResponse.postValue(Resource.Success(response))
            } catch (e:Exception){
                Log.d("", e.javaClass.simpleName)
            }
        }
    }

}

