package com.example.thankucash.mvvm.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BuyDealsConfirmViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _dealsConfirmResponse = MutableLiveData<Resource<BuyDealsResponse>>()
    val dealsConfirmResponse: LiveData<Resource<BuyDealsResponse>> get() = _dealsConfirmResponse

    fun confirmDeals(confirmUserDeals: ConfirmUserDeals){
        _dealsConfirmResponse.postValue(Resource.Loading(null,"Loading..."))
        viewModelScope.launch(Dispatchers.Default)  {
            val response = authRepository.buyDealsConfirms(confirmUserDeals)
            _dealsConfirmResponse.postValue(Resource.Success(response))
        }
    }

}