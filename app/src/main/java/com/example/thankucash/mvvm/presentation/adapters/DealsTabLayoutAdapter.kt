package com.example.thankucash.mvvm.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.thankucash.mvvm.presentation.ui.screens.deals.AboutFragment
import com.example.thankucash.mvvm.presentation.ui.screens.deals.DealsTermsAndConditionFragment
import com.example.thankucash.mvvm.presentation.ui.screens.deals.RedeemLocationFragment

class DealsTabLayoutAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return when(position) {
            0 -> AboutFragment()
            1 -> RedeemLocationFragment()
            else -> DealsTermsAndConditionFragment()
        }
    }
}