package com.example.thankucash.mvvm.utils.validation

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatAutoCompleteTextView

class InstantAutoCompleteTextView(context: Context, attrs: AttributeSet?): AppCompatAutoCompleteTextView(context, attrs) {

    constructor(context: Context, attrs: AttributeSet?, defStyle:Int): this (context, attrs)

    override fun enoughToFilter(): Boolean {
        return true
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focused && filter != null){
            performFiltering(text, 0)
        }
    }
}