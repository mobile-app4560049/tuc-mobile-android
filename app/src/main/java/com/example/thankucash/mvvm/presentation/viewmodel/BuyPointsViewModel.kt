package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BuyPointsViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _buyPointsResponse = MutableLiveData<Resource<BuyPointsResPonse>>()
    val buyPointsResponse: LiveData<Resource<BuyPointsResPonse>> get() = _buyPointsResponse

    fun buyPoint(buyPointsPayment: BuyPointsPayment){
        _buyPointsResponse.postValue(Resource.Loading(null,"Loading..."))
        viewModelScope.launch(Dispatchers.Default) {
            try {
                val response = authRepository.buyPointsInitialized(buyPointsPayment)
                _buyPointsResponse.postValue(Resource.Success(response))
            }catch (e:Exception){
                Log.d("aaaaa", e.javaClass.simpleName)
            }
        }
    }
}