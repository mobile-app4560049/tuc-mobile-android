
package com.example.thankucash.mvvm.presentation.ui.screens.tablayout

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentAirtimeBinding
import com.example.thankucash.databinding.FragmentAirtimeDataViewPagerBinding
import com.example.thankucash.mvvm.presentation.adapters.AirtimeDataAdapterViewPager
import com.example.thankucash.mvvm.presentation.adapters.ViewPagerAdapter2
import com.example.thankucash.mvvm.presentation.viewmodel.TabStateViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class AirtimeDataViewPagerFragment : Fragment() {
private var _binding: FragmentAirtimeDataViewPagerBinding? = null
    private val binding get() = _binding!!

    private val tabTitles = arrayListOf("Airtime", "Data")
    private val tabStateViewModel : TabStateViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAirtimeDataViewPagerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpTabLayoutWitViewPager()

        lifecycleScope.launch{
            tabStateViewModel.airtimeDataTabState.collect{
                val tab = binding.tabLayoutAirtimeData.getTabAt(it)
                tab?.select()
                binding.viewpagerAirtimeData.setCurrentItem(it,false)
                binding.dataText.text = if(it == 0) "Airtime" else "Data"
            }
        }


        binding.dataPinArrowBack.setOnClickListener { activity?.onBackPressed() }
    }
    private fun setUpTabLayoutWitViewPager() {
        binding.viewpagerAirtimeData.adapter = AirtimeDataAdapterViewPager(this)
        TabLayoutMediator(
            binding.tabLayoutAirtimeData,
            binding.viewpagerAirtimeData
        ) { tab, position ->
            tab.text = tabTitles[position]
        }.attach()


        binding.tabLayoutAirtimeData.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    tabStateViewModel.setAirtimeProvider(tab.position)
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }
    }

