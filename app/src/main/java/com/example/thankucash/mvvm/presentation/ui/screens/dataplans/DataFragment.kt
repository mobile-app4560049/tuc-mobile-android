package com.example.thankucash.mvvm.presentation.ui.screens.dataplans

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.Instrumentation.ActivityResult
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.registerForActivityResult
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentDataBinding
import com.example.thankucash.mvvm.presentation.adapters.AirtimeAdapter
import com.example.thankucash.mvvm.presentation.adapters.TopUpAdapter
import com.example.thankucash.mvvm.utils.validation.Constant
import com.google.android.material.bottomsheet.BottomSheetDialog


class DataFragment : Fragment() {
    private var _binding: FragmentDataBinding? = null
    private val binding get() = _binding!!
    private val shareViewModel: ShareViewModel by activityViewModels()
    lateinit var airtimeAdapter: AirtimeAdapter



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDataBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        airtimeAdapter = AirtimeAdapter()

        val bottomSheetDialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialogTheme)
        val bottomSheetView = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_data_beneficiary, null)
        bottomSheetDialog.setContentView(bottomSheetView)

        binding.findBeneficiaryTextView.setOnClickListener {
            bottomSheetDialog.show()
        }


        val model = shareViewModel
        if (model.value != null) {
            binding.numberTextViewTwo.text = "NGN ${model.value?.amount} ${model.value?.dataAmount}"
            binding.editTextAmount.text = "${model.value?.amount}"
        }

        binding.buyDataButton.setOnClickListener {

            findNavController().navigate(R.id.dataPlanPaymentOptions)
        }


        binding.recyclerView.apply {
            adapter = airtimeAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL,false
            )
        }
        airtimeAdapter.submitList(Constant.dataList)

        binding.apply {
            dropDown.setOnClickListener { findNavController().navigate(R.id.dataPlansFragment) }

            contactImage.setOnClickListener {
                var i = Intent(Intent.ACTION_PICK)
                i.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(i, 111)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == Activity.RESULT_OK){
            var contactUri:Uri = data?.data ?: return
            var cols = arrayOf(ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
            Log.d("AAAAAA", "${contactUri}")
            var rs  = requireActivity().contentResolver.query(contactUri,cols,null,null,null)
            if (rs?.moveToFirst()!!){
                binding.numberTextView.setText(rs.getString(0))
            }
        }
    }
}

