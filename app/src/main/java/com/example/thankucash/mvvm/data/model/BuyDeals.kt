package com.example.thankucash.mvvm.data.model

data class BuyDeals(
    val AddressComponent: AddressComponent,
    val Customer: Customer,
    val DealId: Int,
    val DealKey: String,
    val Delivery: Delivery,
    val Task: String
)