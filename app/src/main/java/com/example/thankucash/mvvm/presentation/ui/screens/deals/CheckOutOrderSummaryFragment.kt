package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.thankucash.MainActivity
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentCheckOutOrderSummaryBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.presentation.viewmodel.BuyDealsViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.TopDealsViewModel
import com.example.thankucash.mvvm.utils.validation.ConnectivityLiveData
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CheckOutOrderSummaryFragment : Fragment() {
    private var _binding: FragmentCheckOutOrderSummaryBinding? = null
    private val binding get() = _binding!!
    private val shareViewModel : ShareViewModel by activityViewModels()
    lateinit var connectivityLiveData: ConnectivityLiveData
    private lateinit var dialog: Dialog
    private val buyDealsViewModel: BuyDealsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCheckOutOrderSummaryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        connectivityLiveData = (activity as MainActivity).connectivityLiveData

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }

        connectivityLiveData.observe(
            viewLifecycleOwner
        ) { hasNetwork ->
            if (hasNetwork) buyDealsViewModel.buyDealsResponse

        }

        binding.orderDetailsArrowBack2.setOnClickListener {
            findNavController().navigate(R.id.checkOutDealsFragment)

            val model = shareViewModel
            if (model.topDeal != null) {
                binding.itemName.text = "NGN ${model.topDeal?.Title}"
                binding.actualPrice.text = "${model.topDeal?.SellingPrice}"
                Glide.with(this)
                    .load(model.topDeal?.ImageUrl)
                    .placeholder(R.drawable.imagefan)
                    .into(binding.itemImage)
                binding.textView87.text = "${model.topDeal?.SellingPrice}"
                binding.normaPrice.text = "NGN${model.topDeal?.ActualPrice}"
                binding.deliveryFee.text = "${model.topDeal?.DeliveryTypeCode}"
                binding.convenienceFeeTextView.text = "${model.topDeal?.IsBookmarked}"
                binding.textView16.text = "${model.topDeal?.SellingPrice}"
            }
        }

        binding.apply {
            addIncrement.setOnClickListener {
                val currentValue = binding.productItem.text.toString().substringBefore(" ").toInt()
                val newValue = currentValue + 1
                binding.productItem.text = "$newValue Product from ThankUCash Merchant"
            }
            subtractDecrement.setOnClickListener {
                val currentValue = binding.productItem.text.toString().substringBefore(" ").toInt()
                val newValue = currentValue - 1
                binding.productItem.text = "$newValue Product from ThankUCash Merchant"
            }
        }


        binding.buyBtn.setOnClickListener {
            var getDeals = BuyDeals(
                AddressComponent(
                    binding.address.text.toString(),
                    "",
                    890,
                    "",
                    "",
                    binding.contact.text.toString(),
                    CountryCode.NGN.countries.countryCode,
                    CountryCode.NGN.countries.countryKey,
                    "Nigeria",
                    "",
                    "",
                    11968,
                    binding.johnDoeTextView.text.toString(),
                    "",
                    0,
                    0,
                    "",
                    0,
                    binding.johnDoeTextView.toString(),
                    165,
                    "eda97b0da3554f459f617327b8b27e42",
                    25,
                    "",
                    ""
                ),
                Customer(
                    11968,
                    "",
                    "",
                    false,
                    binding.contact.toString(),
                    binding.johnDoeTextView.toString()
                ),
                DealId = 1,
                "",
                Delivery(
                    0, "", binding.deliverAtTextView.text.toString(),
                    DeliveryPartner(
                        643,
                        0,
                        1640,
                        "",
                        "Estimated Days:  Within 24hrs",
                        "https://delivery-staging.apiideraos.com/partners/amstel.png",
                        "",
                        "Delly Logistics Africa Ltd",
                        1640,
                        "",
                        ""
                    ),
                    "", 477, "", 0, "", 0, "", 0, "", 165, "null"
                ),
                TaskEnum.BUYDEALS.taskType
            )
            buyDealsUser(getDeals)
        }

    }

    private fun buyDealsUser(buyDeals: BuyDeals) {
        lifecycleScope.launch {
            buyDealsViewModel.buyDeals(buyDeals)
        }
    }

    private fun setUpObservers() {
        buyDealsViewModel.buyDealsResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    Log.d("AAAAA","${it.message}")
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_success)
                    findNavController().navigate(R.id.checkOutPaymentOptionsFragment)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
    }
}