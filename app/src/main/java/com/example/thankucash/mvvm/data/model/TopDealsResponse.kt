package com.example.thankucash.mvvm.data.model


data class TopDealsResponse(
    val Status: String,
    val Message: String,
    val Result: Res
)

data class Res(
    val Offset: Long,
    val Limit: Long,
    val TotalRecords: Long,
    val Data: List<Datum>
)

data class Datum(
    val ReferenceID: Long,
    val ReferenceKey: String,
    val Title: String,
    val CategoryKey: String,
    val CategoryName: String,
    val MerchantID: Long,
    val MerchantIconURL: String,
    val MerchantKey: String,
    val MerchantName: String,
    val ImageUrl: String,
    val StartDate: String,
    val EndDate: String,
    val CreateDate: String,
    val ActualPrice: Double,
    val SellingPrice: Double,
    val StoreCount: Long,
    val StateName: String,
    val IsFlashDeal: Boolean,
    val Views: Long,
    val Purchase: Long,
    val IsSoldout: Boolean,
    val IsBookmarked: Boolean,
    val DealTypeCode: String,
    val DeliveryTypeCode: String,
    val Slug: String
)
