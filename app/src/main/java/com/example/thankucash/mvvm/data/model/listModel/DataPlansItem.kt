package com.example.thankucash.mvvm.data.model.listModel

data class DataPlansItemList(
    var dataAmount: String,
    var earn : String,
    var amount: Int
)