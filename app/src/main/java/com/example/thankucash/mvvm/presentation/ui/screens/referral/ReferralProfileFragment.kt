package com.example.thankucash.mvvm.presentation.ui.screens.referral

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentReferralProfileBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.GetUserAccount
import com.example.thankucash.mvvm.data.model.ReferralProfile
import com.example.thankucash.mvvm.data.model.UpdateUserAccount
import com.example.thankucash.mvvm.presentation.viewmodel.GetUserAccountViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.ReferralProfileViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.UpdateUserProfileViewModel
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.SessionManager
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ReferralProfileFragment : Fragment() {
    private var _binding: FragmentReferralProfileBinding? = null
    private val binding get() = _binding!!
    private lateinit var dialog: Dialog
    private val getUserAccountViewModel: GetUserAccountViewModel by activityViewModels()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentReferralProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }



        SessionManager.readFromSharedPref(requireContext(), SessionManager.FIRST_NAME)
        SessionManager.readFromSharedPref(requireContext(), SessionManager.LAST_NAME,)


        binding.apply {
            profileNav.setOnClickListener { findNavController().navigate(R.id.profileScreenFragment) }
            scanQrcCodeTextView.setOnClickListener { findNavController().navigate(R.id.scanQRCodeFragment) }
            contactUsTextView.setOnClickListener { findNavController().navigate(R.id.contactUsFragment) }
            termsConditionTextView.setOnClickListener { findNavController().navigate(R.id.termsAndConditionFragment) }
            referalTextView.setOnClickListener { findNavController().navigate(R.id.referAndEarnFragment) }
            changePinText.setOnClickListener { findNavController().navigate(R.id.changePinFragment) }
            switchCountryTextView.setOnClickListener { findNavController().navigate(R.id.switchCountryFragment) }
            historyText.setOnClickListener { findNavController().navigate(R.id.historyRewardsFragment) }
            wishlistTextView.setOnClickListener { findNavController().navigate(R.id.rewardsWishListFragment) }
            faqTextView.setOnClickListener { findNavController().navigate(R.id.action_referralProfileFragment_to_FAQFragment) }
            privacyPolicy.setOnClickListener { findNavController().navigate(R.id.privacyPolicyFragment) }
            refundPolicy.setOnClickListener { findNavController().navigate(R.id.refundPolicyFragment) }

        }

        var getUserAccount = GetUserAccount(
            1,
            binding.userName.text.toString(),
            "",
            "",
            binding.addressText.text.toString(),
            binding.addressText.text.toString(),
            "",
            ""
        )
        getUserAccount(getUserAccount)
    }

    private fun getUserAccount(getUserAccount: GetUserAccount){
        lifecycleScope.launch{
            getUserAccountViewModel.getUserAccount(getUserAccount)
        }
    }

    private fun setUpObservers() {
        getUserAccountViewModel.getUserAccountResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_success)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()

                }
            }
        }
    }

}