package com.example.thankucash.mvvm.data.model

data class TopDeals(
    val Task: String,
    val Offset:Int,
    val Limit: Int,
    val SearchCondition: String,
    val SortExpression: String,
    val Categories: Array<String>,
    val Merchants: Array<String>,
    val City:String,
    val Location: String,
    val CountryCode: Int,
    val CountryKey: String
)
