package com.example.thankucash.mvvm.presentation.ui.screens.tablayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.example.thankucash.databinding.FragmentUtilityBillsAndElectricityViewPagerBinding
import com.example.thankucash.mvvm.presentation.adapters.ElectricityAndUtilityBillsViewPagerAdapter
import com.example.thankucash.mvvm.presentation.viewmodel.TabStateViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.launch

class UtilityBillsAndElectricityViewPagerFragment : Fragment() {
    private var _binding: FragmentUtilityBillsAndElectricityViewPagerBinding? = null
    private val binding get() = _binding!!

    private val tabTitles = arrayListOf("Tv Recharge", "Electricity", "Utility")

    private val tabStateViewModel : TabStateViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUtilityBillsAndElectricityViewPagerBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewpagerUtilityBills.adapter = ElectricityAndUtilityBillsViewPagerAdapter(this)
        binding.utilityViewpagerArrowBack.setOnClickListener { activity?.onBackPressed() }
      setUpTabLayoutWitViewPager()


        lifecycleScope.launch{
            tabStateViewModel.utilityElectricityTabState.collect{
                val tab = binding.tabLayoutUtilityBills.getTabAt(it)
                tab?.select()
                binding.viewpagerUtilityBills.setCurrentItem(it,false)
                binding.utilityBillsText.text = if(it == 0) "Utility Bills" else "Electricity Bills"
            }
        }
    }


    private fun setUpTabLayoutWitViewPager() {
        binding.viewpagerUtilityBills.apply {
            val tab = binding.tabLayoutUtilityBills.getTabAt(1)
            tab?.select()
            TabLayoutMediator(
                binding.tabLayoutUtilityBills,
                binding.viewpagerUtilityBills
            ) { tab, position ->
                tab.text = tabTitles[position]
            binding.utilityBillsText.text = tabTitles[position]
            }.attach()
        }
        binding.tabLayoutUtilityBills.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    tabStateViewModel.setUtilityTabState(tab.position)
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }

    }