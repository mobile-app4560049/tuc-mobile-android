package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.ChangePin
import com.example.thankucash.mvvm.data.model.ChangePinResponse
import com.example.thankucash.mvvm.data.model.GenericResult
import com.example.thankucash.mvvm.data.model.UpdatePin
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChangePinViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _changePinResponse = MutableLiveData<Resource<GenericResult<ChangePinResponse>>>()
    val changePinResponse: LiveData<Resource<GenericResult<ChangePinResponse>>> get() = _changePinResponse

    fun changeUserPin(changePin: ChangePin){
        _changePinResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch(Dispatchers.Default) {
            try {
                _changePinResponse.value = authRepository.changePin(changePin)
            } catch (e: Exception){
                Log.d("", e.javaClass.simpleName)
            }
        }
    }
}