package com.example.thankucash.mvvm.data.model

data class DeliveryX(
    val AccountId: Int,
    val AccountKey: Any,
    val DeliveryFee: Int,
    val DeliveryPartner: DeliveryPartnerXX,
    val DeliveryPartners: List<DeliveryPartnerXXX>,
    val FromAddressId: Int,
    val FromAddressReference: Any,
    val PackageId: Int,
    val PackageKey: Any,
    val ParcelId: Int,
    val ParcelKey: Any,
    val ShipmentId: Int,
    val ShipmentKey: String,
    val ToAddressId: Int,
    val ToAddressReference: Any
)