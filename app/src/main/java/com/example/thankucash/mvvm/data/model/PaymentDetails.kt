package com.example.thankucash.mvvm.data.model

data class PaymentDetails(
    val Amount: Int,
    val DeliveryFee: Int,
    val Fee: Int,
    val ReferenceId: String,
    val ReferenceKey: String,
    val TotalAmount: Int
)