package com.example.thankucash.mvvm.data.model

data class GetDealCodeResponse(
    val Status: String,
    val Message: String,
    val Result: Result,
    val ResponseCode: String,
    val Mode: String
)

data class Restults (
    val ReferenceID: Long,
    val ReferenceKey: String,
    val DealReferenceID: Long,
    val DealReferenceKey: String,
    val AccountID: Long,
    val AccountKey: String,
    val AccountDisplayName: String,
    val AccountMobileNumber: String,
    val AccountIconURL: String,
    val ItemCode: String,
    val StartDate: String,
    val EndDate: String,
    val StatusID: Long,
    val StatusCode: String,
    val StatusName: String,
    val Quantity: Long,
    val Amount: Double,
    val CommissionAmount: Double,
    val Charge: Double,
    val DeliveryCharge: Double,
    val TotalAmount: Double,
    val Title: String,
    val Description: String,
    val ImageURL: String,
    val MerchantReferenceID: Long,
    val MerchantReferenceKey: String,
    val MerchantDisplayName: String,
    val MerchantIconURL: String,
    val MerchantContactNumber: String,
    val MerchantEmail: String,
    val RedeemInstruction: String,
    val CreateDate: String,
    val SharedCustomerIconURL: String,
    val DealTypeID: Long,


    val Locations: List<Location>,

    val DealTypeCode: String,
    val DeliveryTypeCode: String,
    val DeliveryDetails: DeliveryDetails,
    val PaymentDetails: PaymentDetails,
    val TransactionID: Long
)

data class Locations (
    val displayName: String,
    val Address: String,
    val ContactNumber: String,
    val Latitude: Double,
    val Longitude: Double
)

data class PaymentDetailss (
    val PaymentTypeID: Long,
    val PaymentTypeName: String,
    val PaymentTypeCode: String,
    val ItemAmount: Double,
    val DeliveryCharges: Double,
    val TotalAmount: Double,
    val ItemCount: Long
)
