package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.MainActivity
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentSearchNotFoundBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.TopDeals
import com.example.thankucash.mvvm.data.model.TopDealsResponse
import com.example.thankucash.mvvm.presentation.adapters.TopDealsAdapter
import com.example.thankucash.mvvm.presentation.viewmodel.TopDealsViewModel
import com.example.thankucash.mvvm.utils.validation.ConnectivityLiveData
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SearchNotFoundFragment : Fragment() {
    private var _binding: FragmentSearchNotFoundBinding? = null
    private val binding get() = _binding!!
    private val topDealsViewModel: TopDealsViewModel by activityViewModels()
    private lateinit var dialog: Dialog
    private val shareViewModel: ShareViewModel by activityViewModels()
    lateinit var topDealsAdapter: TopDealsAdapter
    lateinit var connectivityLiveData: ConnectivityLiveData
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSearchNotFoundBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        connectivityLiveData = (activity as MainActivity).connectivityLiveData

        connectivityLiveData.observe(
            viewLifecycleOwner
        ) { hasNetwork ->
            if (hasNetwork)
                topDealsViewModel.topDealsResponse

        }
        topDealsAdapter = TopDealsAdapter { it: TopDealsResponse ->
            shareViewModel.topDeal = it.Result.Data[0]
            findNavController().navigate(R.id.orderDetailsFragment)
        }

        binding.seeall.setOnClickListener { findNavController().navigate(R.id.topDealsFragment) }

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }

        topDealsViewModel.topDealsResponse

        binding.recyclerViewTopDeals.apply {
            adapter = topDealsAdapter
            layoutManager = LinearLayoutManager(
                requireContext(), LinearLayoutManager.HORIZONTAL, false
            )

        }

        var deals = TopDeals(
            TaskEnum.TOPDEALS.taskType,
            0,
            Limit = 15,
            "",
            "Newest",
            arrayOf(),
            arrayOf(),
            "",
            "Top Deals",
            CountryCode.NGN.countries.countryCode,
            CountryCode.NGN.countries.countryKey
        )
        topDeal(deals)

    }


    private fun topDeal(topDeals: TopDeals) {
        lifecycleScope.launch {
            topDealsViewModel.getTopDeals(topDeals)
        }
    }

    private fun setUpObservers() {
        topDealsViewModel.topDealsResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    requireView().showSnackBar(R.string.call_success)
                    it.data?.Result?.Data?.let { it1 ->
                    }

                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }

            }
        }
    }
}