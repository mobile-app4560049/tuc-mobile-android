package com.example.thankucash.mvvm.data.model

data class UpdateProfileResponse(
val Status: String,
val Message: String,
val ResponseCode: String,
val Mode: String
)

