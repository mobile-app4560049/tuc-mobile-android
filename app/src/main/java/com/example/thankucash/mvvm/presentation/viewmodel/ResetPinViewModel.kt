package com.example.thankucash.mvvm.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.ResetPin
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ResetPinViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel(){
    private var _resetPinResponse = MutableLiveData<Resource<Int>>()
    val resetPinResponse: LiveData<Resource<Int>> get() = _resetPinResponse

    fun resetUserPin(resetPin: ResetPin){
        _resetPinResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch (Dispatchers.Default){
            try {
                _resetPinResponse.value = authRepository.resetUserPin(resetPin)
            }catch (e: Exception){
            }
        }
    }
}