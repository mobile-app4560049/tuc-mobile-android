package com.example.thankucash.mvvm.data.model

class AllDealsResponse(
    val Status: String,
    val Message: String,
    val Result: Resut,
    val ResponseCode: String? = null,
    val Mode: String? = null
)

data class Resut (
    val Offset: Long,
    val Limit: Long,
    val TotalRecords: Long,
    val Data: List<Datum?>
)