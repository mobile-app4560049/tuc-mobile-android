package com.example.thankucash.mvvm.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.Datum
import com.example.thankucash.mvvm.data.model.TopDeals
import com.example.thankucash.mvvm.data.model.TopDealsResponse
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TopDealsViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel(){
    private var _topDealsResponse = MutableLiveData<Resource<TopDealsResponse>>()
    val topDealsResponse: LiveData<Resource<TopDealsResponse>> get() = _topDealsResponse
    var cachedTopDeals: List<Datum> = listOf()


    var searchQuery = MutableLiveData<String>("")


    fun getTopDeals(topDeals: TopDeals){
        _topDealsResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch  {
            val response = authRepository.topDeals(topDeals)
            _topDealsResponse.postValue(Resource.Success(response))
        }
    }


}