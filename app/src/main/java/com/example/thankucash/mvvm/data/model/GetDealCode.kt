package com.example.thankucash.mvvm.data.model

data class GetDealCode(
    val Task: String,
    val Host: Int,
    val DealCodeKey: String,
    val ReferenceId: String,
    val ReferenceKey: String,
    val CountryId: Int,
    val CountryKey: String
)