package com.example.thankucash.mvvm.presentation.ui.screens.dataplans

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentDataPlansBinding
import com.example.thankucash.mvvm.presentation.adapters.AirtimeAdapter
import com.example.thankucash.mvvm.presentation.adapters.DataPlanItemAdapter
import com.example.thankucash.mvvm.utils.validation.Constant
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DataPlansFragment : Fragment() {
    private var _binding : FragmentDataPlansBinding? = null
    private val binding get() = _binding!!
    lateinit var dataPlansItemAdapter: DataPlanItemAdapter
    private val shareViewModel: ShareViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDataPlansBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataPlansItemAdapter = DataPlanItemAdapter{
            shareViewModel.value = it
            findNavController().popBackStack()
        }


        binding.recyclerView.apply {
            adapter = dataPlansItemAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL,false
            )
        }
        dataPlansItemAdapter.submitList(Constant.dataPlansList)


        binding.dataPlantText.setOnClickListener { findNavController().navigate(R.id.dataPlanPaymentOptions) }
    }

}