package com.example.thankucash.mvvm.presentation.ui.screens.dataplans

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentAirtimeHistoryBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AirtimeHistoryFragment : Fragment() {
    private var _binding: FragmentAirtimeHistoryBinding? = null
    private val binding get() = _binding!!
    private val shareViewModel: ShareViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAirtimeHistoryBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = shareViewModel
        if (model.airtime != null) {
            binding.walletTopText.text = "NGN ${model.airtime ?.rewardText}"
            binding.network.text = "${model.airtime ?.stationName}"
            model.airtime ?.calendarImage?.let { binding.historyCalendar.setImageResource(it).toString() }
            binding.amount.text = "${model.airtime ?.amount}"
            binding.historyDate.text ="${model.airtime ?.date}"
            binding.historyTime.text = "${model.airtime ?.time2}"
        }

        binding.backHomeText.setOnClickListener {
            findNavController().navigate(R.id.airtimeDataViewPagerFragment)
        }
        binding.cancel.setOnClickListener { activity?.onBackPressed() }
    }
}