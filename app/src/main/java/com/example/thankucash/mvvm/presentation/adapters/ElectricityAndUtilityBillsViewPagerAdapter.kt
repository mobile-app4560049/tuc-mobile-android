package com.example.thankucash.mvvm.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.thankucash.mvvm.presentation.ui.screens.utility.ElectricityBillsFragment
import com.example.thankucash.mvvm.presentation.ui.screens.utility.UtilityBillsFragment
import com.example.thankucash.mvvm.presentation.ui.screens.utility.UtilityFragment

class ElectricityAndUtilityBillsViewPagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> UtilityBillsFragment()
            1 -> ElectricityBillsFragment()
            else -> UtilityFragment()
        }
    }
}
