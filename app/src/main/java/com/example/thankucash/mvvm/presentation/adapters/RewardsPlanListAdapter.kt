package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.RewardsHistoryItemBinding
import com.example.thankucash.mvvm.data.model.listModel.DstvPlansItem
import com.example.thankucash.mvvm.data.model.listModel.RewardsItem

class RewardsPlanListAdapter(
    private val onClick:(item: RewardsItem) -> Unit
): RecyclerView.Adapter<RewardsPlanListAdapter.RewardsItemViewHolder>() {
    private var rewardsItemList: ArrayList<RewardsItem> = arrayListOf()
    private var selectedPos = -1

     inner class RewardsItemViewHolder(val binding: RewardsHistoryItemBinding): RecyclerView.ViewHolder(binding.root) {
         var cardView = binding.cardView
         var constraintLayout = binding.constraint

         fun bind(rewardsItem: RewardsItem) {
             binding.walletTopText.text = rewardsItem.rewardText
             binding.statioName.text = rewardsItem.stationName
             binding.dateTxt.text = rewardsItem.date
             binding.calendar.setImageResource(rewardsItem.calendarImage)
             binding.timeTxt.text = rewardsItem.time2
             binding.amount.text = rewardsItem.amount


             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 notifyItemChanged(selectedPos)
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }

             binding.cardView.setOnClickListener { onClick(rewardsItem) }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RewardsItemViewHolder {
          val inflater = RewardsHistoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return RewardsItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: RewardsItemViewHolder, @SuppressLint("RecyclerView") position: Int) {
          holder.bind(rewardsItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int = rewardsItemList.size
     fun submitList (list: ArrayList<RewardsItem>){
         rewardsItemList = list
         notifyDataSetChanged()
     }

    companion object {
        private const val TAG = "AirtimeAdapter"
    }
}