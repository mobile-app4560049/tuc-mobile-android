package com.example.thankucash.mvvm.data.model.listModel

import com.example.thankucash.mvvm.data.enum.CountryCode

data class Countries(
    val countryCode: Int,
    val countryKey: String,
    val countryIsd: String
)
