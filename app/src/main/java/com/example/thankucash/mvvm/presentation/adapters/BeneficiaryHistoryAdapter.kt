package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.BeneficialItemBinding
import com.example.thankucash.mvvm.data.model.listModel.BeneficiaryItems

class BeneficiaryHistoryAdapter(
): RecyclerView.Adapter<BeneficiaryHistoryAdapter.DataItemViewHolder>() {
    private var beneficiaryItemList: ArrayList<BeneficiaryItems> = arrayListOf()
    private var selectedPos = -1

     inner class DataItemViewHolder(val binding: BeneficialItemBinding): RecyclerView.ViewHolder(binding.root) {
         fun bind(beneficial: BeneficiaryItems) {
             binding.victoriaText.text = beneficial.itemName
             binding.textView8.text = beneficial.itemValue
             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 notifyItemChanged(selectedPos)
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }

         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataItemViewHolder {
          val inflater = BeneficialItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return DataItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: DataItemViewHolder, @SuppressLint("RecyclerView") position: Int) {
          holder.bind(beneficiaryItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int = beneficiaryItemList.size
     fun submitList (list: ArrayList<BeneficiaryItems>){
         beneficiaryItemList = list
         notifyDataSetChanged()
     }

    companion object {
        private const val TAG = "AirtimeAdapter"
    }
}