package com.example.thankucash.mvvm.data.model

data class AddressComponent(
    val AddressLine1: String,
    val AddressLine2: String,
    val CityId: Int,
    val CityKey: String,
    val CityName: String,
    val ContactNumber: String,
    val CountryId: Int,
    val CountryKey: String,
    val CountryName: String,
    val CreateDate: String,
    val CreatedByDisplayName: String,
    val CreatedById: Int,
    val DisplayName: String,
    val EmailAddress: String,
    val Latitude: Int,
    val LocationTypeId: Int,
    val LocationTypeName: String,
    val Longitude: Int,
    val Name: String,
    val ReferenceId: Int,
    val ReferenceKey: String,
    val StateId: Int,
    val StateKey: String,
    val StateName: String
)