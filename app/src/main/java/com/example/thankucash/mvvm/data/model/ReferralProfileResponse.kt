package com.example.thankucash.mvvm.data.model

data class ReferralProfileResponse(
    val Status: String,
    val Message: String,
    val ResponseCode: String,
    val Mode: String
)
