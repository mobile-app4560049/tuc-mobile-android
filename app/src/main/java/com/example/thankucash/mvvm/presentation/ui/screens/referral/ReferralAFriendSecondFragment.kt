package com.example.thankucash.mvvm.presentation.ui.screens.referral

import android.content.Context.WINDOW_SERVICE
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Point
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import com.example.thankucash.R
import com.example.thankucash.databinding.ActivityMainBinding.inflate
import com.example.thankucash.databinding.AirtimeListRecyclerBinding.inflate
import com.example.thankucash.databinding.FragmentReferralAFriendSecondBinding
import com.google.zxing.BarcodeFormat
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReferralAFriendSecondFragment : Fragment() {

    private var _binding : FragmentReferralAFriendSecondBinding? = null
    private val binding get() = _binding!!
    lateinit var bitmap: Bitmap
    private lateinit var imageView: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentReferralAFriendSecondBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.referBackArrow.setOnClickListener { activity?.onBackPressed() }

        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.fragment_scan_q_r_code, null, )

        imageView = view.findViewById(R.id.frame_img)
        val text = "thankucashreferrall.com/1234HEe3"
        val bitmap = generateQRCode(text)
        imageView.setImageBitmap(bitmap)

    }

        private fun generateQRCode(text: String): Bitmap? {
            val qrCodeWriter = QRCodeWriter()
            try {
                val bitMatrix: BitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, 512, 512)
                val width = bitMatrix.width
                val height = bitMatrix.height
                val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
                for (x in 0 until width) {
                    for (y in 0 until height) {
                        bmp.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
                    }
                }
                return bmp
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }
    }
