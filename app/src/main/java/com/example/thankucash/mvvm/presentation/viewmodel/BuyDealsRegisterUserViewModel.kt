package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BuyDealsRegisterUserViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _buyDealsRegisterUserResponse = MutableLiveData<Resource<RegisterUserResponse>>()
    val buyDealsRegisterResponse: LiveData<Resource<RegisterUserResponse>> get() = _buyDealsRegisterUserResponse

    fun buyDealsRegister(buyDealsRegisterUser: BuyDealsRegisterUser){
        _buyDealsRegisterUserResponse.postValue(Resource.Loading(null,"Loading..."))
        viewModelScope.launch(Dispatchers.Default)  {
            try {
                val response = authRepository.buyDealsRegister(buyDealsRegisterUser)
                _buyDealsRegisterUserResponse.postValue(Resource.Success(response))
            } catch (e: Exception) {
                Log.e("678ty78", e.javaClass.simpleName)
            }
        }
    }

}