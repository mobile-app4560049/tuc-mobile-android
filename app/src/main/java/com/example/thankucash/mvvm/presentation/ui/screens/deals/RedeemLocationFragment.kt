package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentRedeemLocationBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RedeemLocationFragment : Fragment() {
    private var _binding: FragmentRedeemLocationBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRedeemLocationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

}