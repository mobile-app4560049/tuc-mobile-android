package com.example.thankucash.mvvm.data.model

data class DeliveryPartnerXX(
    val CarrierId: Int,
    val DeductablePrice: Int,
    val DeliveryCharge: Int,
    val DeliveryPartnerKey: String,
    val ExpectedDeliveryTime: String,
    val IconUrl: String,
    val KwikKey: String,
    val Name: String,
    val PayablePrice: Int,
    val RateId: String,
    val RedisKey: String
)