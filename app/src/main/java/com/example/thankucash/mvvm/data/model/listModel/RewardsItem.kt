package com.example.thankucash.mvvm.data.model.listModel

data class RewardsItem (
    val rewardText: String,
    val stationName : String,
    val calendarImage: Int,
    val date: String,
    val time2: String,
    val amount: String
    )