package com.example.thankucash.mvvm.data.model

data class BuyConfirmDeals(
    val AccountId: Int,
    val AccountKey: String,
    val AddressComponent: AddressComponentX,
    val Customer: CustomerX,
    val DealId: Int,
    val DealKey: String,
    val Delivery: DeliveryX,
    val PaymentDetails: PaymentDetails,
    val PaymentReference: Any,
    val Task: String,
    val TransactionId: Int
)