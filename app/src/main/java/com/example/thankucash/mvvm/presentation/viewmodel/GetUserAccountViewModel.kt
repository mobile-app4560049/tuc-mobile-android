package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.GenericResult
import com.example.thankucash.mvvm.data.model.GetUserAccount
import com.example.thankucash.mvvm.data.model.UpdateProfileResponse
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GetUserAccountViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _getUserAccountResponse = MutableLiveData<Resource<GenericResult<UpdateProfileResponse>>>()
    val  getUserAccountResponse: LiveData<Resource<GenericResult<UpdateProfileResponse>>> get() = _getUserAccountResponse
    fun getUserAccount(getUserAccount: GetUserAccount){
        _getUserAccountResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch(Dispatchers.Default){
            try {
                _getUserAccountResponse.value = authRepository.getUserAccount(getUserAccount)
            }catch (e: Exception){
                Log.d("error", e.javaClass.simpleName)
            }
        }
    }
}