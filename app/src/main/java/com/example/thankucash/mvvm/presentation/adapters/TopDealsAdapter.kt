package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Message
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.thankucash.R
import com.example.thankucash.databinding.FlashDealsItemBinding
import com.example.thankucash.mvvm.data.model.Datum
import com.example.thankucash.mvvm.data.model.Res
import com.example.thankucash.mvvm.data.model.TopDeals
import com.example.thankucash.mvvm.data.model.TopDealsResponse
import com.example.thankucash.mvvm.data.model.listModel.AirtimeItem
import javax.net.ssl.SSLEngineResult.Status

class TopDealsAdapter(
    private val onClick:(item: TopDealsResponse) -> Unit)
    :RecyclerView.Adapter<TopDealsAdapter.DealsItemViewHolder>() {

     inner class DealsItemViewHolder(val binding: FlashDealsItemBinding): RecyclerView.ViewHolder(binding.root) {
         var button = binding.buyNow
         @SuppressLint("SuspiciousIndentation")
         fun bind(topDealsResponse: Datum) {
             binding.apply {
                 binding.actualPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                 description.text = topDealsResponse.Title
                 areaState.text = topDealsResponse.StateName
                 location.text = topDealsResponse.StateName
                 sellingPrice.text = topDealsResponse.SellingPrice.toString()
                 actualPrice.text = topDealsResponse.ActualPrice.toString()
                 binding.buyNow
                 Glide.with(binding.root.context)
                     .load(topDealsResponse.ImageUrl)
                     .placeholder(R.drawable.rectangle)
                     .into(imageItem)
             }

             val response = TopDealsResponse(
                 Status = "success",
                 Message = "Success",
                 Result = Res(
                     Offset = 0,
                     Limit = 1,
                     TotalRecords = 1,
                     Data = listOf(topDealsResponse)
                 )
             )
             binding.buyNow.setOnClickListener { onClick(response) }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealsItemViewHolder {
          val inflater = FlashDealsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return DealsItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: DealsItemViewHolder, position: Int) {
          holder.bind(differ.currentList[position])
     }
    private val differCallBack = object:DiffUtil.ItemCallback<Datum>() {
        override fun areItemsTheSame(oldItem:Datum,newItem:Datum):Boolean{
            return oldItem.ReferenceID == newItem.ReferenceID
        }
        override fun areContentsTheSame(oldItem:Datum,newItem:Datum):Boolean{
            return oldItem == newItem
        }
    }

    var differ = AsyncListDiffer(this,differCallBack)

    override fun getItemCount(): Int = differ.currentList.size

}