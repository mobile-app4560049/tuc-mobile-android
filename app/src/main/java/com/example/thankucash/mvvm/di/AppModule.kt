package com.example.thankucash.mvvm.di

import com.example.thankucash.mvvm.data.network.NetworkConstants.Companion.BASE_URL
import com.example.thankucash.mvvm.data.network.ThankUCashApi
import com.example.thankucash.mvvm.data.repository.AuthRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideLogger(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
    }

    @Provides
    @Singleton
    fun provideOKHttpClient(logger: HttpLoggingInterceptor): OkHttpClient {

        val interceptor = Interceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Authorization", "MThkYTY0OGE1OTAyNGYyYmE5M2UyZTA3MzE5YjE3NjQ=")
                .build()
            chain.proceed(request)
        }
        return OkHttpClient.Builder()
            .addInterceptor(logger)
            .addInterceptor(interceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()


    @Provides
    @Singleton
    fun provideClientAPI(retrofit: Retrofit): ThankUCashApi =
        retrofit.create(ThankUCashApi::class.java)

    @Provides
    @Singleton
    fun provideAuthRepository(api: ThankUCashApi): AuthRepository =
        AuthRepository(api)
}