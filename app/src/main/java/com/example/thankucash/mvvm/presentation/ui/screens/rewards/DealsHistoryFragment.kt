package com.example.thankucash.mvvm.presentation.ui.screens.rewards

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentDealsHistoryBinding
import com.example.thankucash.mvvm.presentation.adapters.DealsItemListAdapter
import com.example.thankucash.mvvm.utils.validation.Constant

class DealsHistoryFragment : Fragment() {
    private var _binding : FragmentDealsHistoryBinding? = null
    private val binding get() = _binding!!
    lateinit var dealsItemListAdapter: DealsItemListAdapter
    private val shareViewModel: ShareViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDealsHistoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dealsItemListAdapter = DealsItemListAdapter{
            shareViewModel.deals = it
            findNavController().navigate(R.id.dealsTitleHistoryFragment2)
        }



        binding.dealsRecyclerView.apply {
            adapter = dealsItemListAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
        dealsItemListAdapter.submitList(Constant.deals)



        binding.dealsRecyclerView.setOnClickListener { findNavController().navigate(R.id.dealsTitleHistoryFragment2) }


    }

}