package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.DataHistoryItemBinding
import com.example.thankucash.databinding.RewardsHistoryItemBinding
import com.example.thankucash.mvvm.data.model.listModel.AirtimeItem
import com.example.thankucash.mvvm.data.model.listModel.DstvPlansItem
import com.example.thankucash.mvvm.data.model.listModel.RewardsItem

class AirtimePlanListAdapter(
    private val onClick:(item: AirtimeItem) -> Unit
): RecyclerView.Adapter<AirtimePlanListAdapter.AirtimeItemViewHolder>() {
    private var airtimesItemList: ArrayList<AirtimeItem> = arrayListOf()
    private var selectedPos = -1

     inner class AirtimeItemViewHolder(val binding: DataHistoryItemBinding): RecyclerView.ViewHolder(binding.root) {
         var cardView = binding.cardView
         var constraintLayout = binding.constraint

         fun bind(airtime: AirtimeItem) {
             binding.walletTopText.text = airtime.rewardText
             binding.network.text = airtime.stationName
             binding.date.text = airtime.date
             binding.calendar.setImageResource(airtime.calendarImage)
             binding.time.text = airtime.time2
             binding.amount.text = airtime.amount


             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 notifyItemChanged(selectedPos)
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }
             binding.cardView.setOnClickListener { onClick(airtime) }

         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirtimeItemViewHolder {
          val inflater = DataHistoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return AirtimeItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: AirtimeItemViewHolder, @SuppressLint("RecyclerView") position: Int) {
          holder.bind(airtimesItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int = airtimesItemList.size
     fun submitList (list: ArrayList<AirtimeItem>){
         airtimesItemList = list
         notifyDataSetChanged()
     }

    companion object {
        private const val TAG = "AirtimeAdapter"
    }
}