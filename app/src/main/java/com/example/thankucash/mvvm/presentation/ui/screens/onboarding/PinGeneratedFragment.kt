package com.example.thankucash.mvvm.presentation.ui.screens.onboarding

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentPinGeneratedBinding
import com.example.thankucash.mvvm.presentation.viewmodel.VerifyUserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PinGeneratedFragment : Fragment() {
    private var _binding: FragmentPinGeneratedBinding? = null
    private val binding get() = _binding!!
    private val verifyUserViewModel : VerifyUserViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPinGeneratedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.pinGeneratedText.text = "we will call ${verifyUserViewModel.userPhoneNumber} to give you a verification code"

        val handler = Handler()
        handler.postDelayed({
            requireActivity().supportFragmentManager.popBackStack()
        }, 5000)

    }
}