package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GetDealCodeViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _getDealsCodeResponse = MutableLiveData<Resource<GetDealCodeResponse>>()
    val getDealsResponse: LiveData<Resource<GetDealCodeResponse>> get() = _getDealsCodeResponse

    fun getDealsCode(getDealCode: GetDealCode){
        _getDealsCodeResponse.postValue(Resource.Loading(null,"Loading..."))
        viewModelScope.launch(Dispatchers.Default) {
            try {
                val response = authRepository.getDealsCode(getDealCode)
                _getDealsCodeResponse.postValue(Resource.Success(response))
            }catch (e: Exception){
                Log.d("error", e.javaClass.simpleName)
            }
        }
    }

}