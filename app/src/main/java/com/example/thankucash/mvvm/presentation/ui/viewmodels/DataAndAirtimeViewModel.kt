package com.example.thankucash.mvvm.presentation.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DataAndAirtimeViewModel @Inject constructor(): ViewModel() {

    private var _providerState: MutableStateFlow<String> = MutableStateFlow("")
    val providerState: StateFlow<String> get() = _providerState

    private var _amountState: MutableStateFlow<Int> = MutableStateFlow(0)
    val amountState: StateFlow<Int> get() = _amountState

    fun setAirtimeProvider(state:String){
        viewModelScope.launch(Dispatchers.IO){
            _providerState.emit(state)
        }
    }

    fun setAmount(state: Int){
        viewModelScope.launch(Dispatchers.IO){
            _amountState.emit(state)
        }
    }
}