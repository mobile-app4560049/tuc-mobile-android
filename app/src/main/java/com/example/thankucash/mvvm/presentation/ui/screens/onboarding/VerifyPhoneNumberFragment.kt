package com.example.thankucash.mvvm.presentation.ui.screens.onboarding

import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentVerifyPhoneNumberBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.UserVerification
import com.example.thankucash.mvvm.presentation.viewmodel.VerifyUserViewModel
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.SessionManager
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class VerifyPhoneNumberFragment : Fragment() {
    private var _binding : FragmentVerifyPhoneNumberBinding? = null
    private val binding get() = _binding!!
    private lateinit var dialog: Dialog
    private val verifyUserViewModel : VerifyUserViewModel by activityViewModels()
    private val args: VerifyPhoneNumberFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentVerifyPhoneNumberBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.verifyPhoneArrowBack.setOnClickListener { activity?.onBackPressed() }

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }


        moveFocusOnTextChange(binding.otp1, binding.otp2, binding.otp3, binding.otp4)



        binding.verificationCodeText.text = "we will call ${verifyUserViewModel.userPhoneNumber} to give you a verification code"

        binding.verifyBtn.setOnClickListener {
            var verify = UserVerification(
                TaskEnum.VERIFY.taskType,
                0,
                CountryCode.NGN.countries.countryIsd,
                binding.verificationCodeText.text.toString(),
                SessionManager.TOKEN.length,
                binding.pinLinearLayout.toString(),
                CountryCode.NGN.countries.countryCode,
                CountryCode.NGN.countries.countryKey
            )
            verifyUser(verify)
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun verifyUser(userVerification: UserVerification){
        lifecycleScope.launch{
            verifyUserViewModel.verifyUser(userVerification)
        }
    }

    private fun setUpObservers() {
        verifyUserViewModel.verifyUserResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    findNavController().navigate(R.id.fund_wallet_nav)
                    requireView().showSnackBar(R.string.call_success)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
    }
        fun moveFocusOnTextChange(vararg editTexts: EditText) {
            for (i in editTexts.indices) {
                editTexts[i].addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        if (count == 1 && start == 0 && i < editTexts.lastIndex) {
                            editTexts[i + 1].requestFocus()
                        } else if (count == 0 && start == 0 && i > 0) {
                            editTexts[i - 1].requestFocus()
                        }
                    }
                    override fun afterTextChanged(s: Editable?) {}
                })
            }
    }


}