package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentDealsTermsAndConditionBinding

class DealsTermsAndConditionFragment : Fragment() {
    private var _binding: FragmentDealsTermsAndConditionBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_deals_terms_and_condition, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}