package com.example.thankucash.mvvm.data.model.listModel

data class UtilityItem (
    val image: Int,
    val text : String,
    val name:String,
    )