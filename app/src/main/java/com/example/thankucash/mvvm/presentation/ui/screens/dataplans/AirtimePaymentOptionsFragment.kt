package com.example.thankucash.mvvm.presentation.ui.screens.dataplans

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentAirtimePaymentOptionsBinding
import com.example.thankucash.mvvm.presentation.ui.screens.fundwallet.SelectedPaymentOption
import com.example.thankucash.mvvm.utils.validation.beginFlutterwaveTransaction
import com.flutterwave.raveandroid.RavePayActivity
import com.flutterwave.raveandroid.RaveUiManager
import com.flutterwave.raveandroid.rave_java_commons.RaveConstants
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.*

@AndroidEntryPoint
class AirtimePaymentOptionsFragment : Fragment() {
    private var _binding : FragmentAirtimePaymentOptionsBinding? = null
    private var selectedPaymentOption: SelectedPaymentOption = SelectedPaymentOption.NONE
    private val binding get() = _binding!!
    private var selectedLayout: ConstraintLayout? = null
    private var selectedCheckout: RadioButton? = null
    private val shareViewModel : ShareViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAirtimePaymentOptionsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val model = shareViewModel
        if (model.message != null) {
            try {
                val amount = model.message.toString().toDouble()
                val ngnFormat = NumberFormat.getCurrencyInstance(Locale("en", "NG"))
                binding.textAirtime.text = ngnFormat.format(amount)
            } catch (e: NumberFormatException) {
                // Handle the error here
                e.printStackTrace()
            }
        }


        binding.airtimeBuyBtn.setOnClickListener { findNavController().navigate(R.id.airtimePaymentOptions2Fragment) }
        binding.airtimePlanPaymentOptionsArrowBack2.setOnClickListener { activity?.onBackPressed() }

        binding.flutterWaveLayout.setOnClickListener {
            isNotSelected()
            isSelected(binding.radioButtonFlutterWave, binding.flutterWaveLayout)
            selectedLayout = binding.flutterWaveLayout
            selectedCheckout = binding.radioButtonFlutterWave
        }
        binding.payStackLayout.setOnClickListener {
            isNotSelected()
            isSelected(binding.radioButtonPaystack, binding.payStackLayout)
            selectedLayout = binding.payStackLayout
            selectedCheckout = binding.radioButtonPaystack
        }
        binding.thankULayout.setOnClickListener {
            isNotSelected()
            isSelected(binding.radioButtonThankU, binding.thankULayout)
            selectedLayout = binding.thankULayout
            selectedCheckout = binding.radioButtonThankU
        }

        binding.airtimeBuyBtn.setOnClickListener {
            checkout()
        }
    }


    fun isSelected(radioButton: RadioButton, view: ConstraintLayout) {
        view.setBackgroundDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.green_stroke_128dp_corners
            )
        )
        radioButton.isChecked = true
        radioButton.background = ContextCompat.getDrawable(
            requireContext(),
            R.drawable.custom_icon
        )


    }
    fun isNotSelected(){
        selectedLayout?.setBackgroundDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.grey_stroke_128dp_corners
            )
        )
        selectedCheckout?.isChecked = false
        selectedCheckout?.background = null
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        /*
         *  We advise you to do a further verification of transaction's details on your server to be
         *  sure everything checks out before providing service or goods.
        */
        if (requestCode == RaveConstants.RAVE_REQUEST_CODE && data != null) {
            val message = data.getStringExtra("response")
            if (resultCode == RavePayActivity.RESULT_SUCCESS) {
                Toast.makeText(requireContext(), "SUCCESS $message", Toast.LENGTH_SHORT).show()
            } else if (resultCode == RavePayActivity.RESULT_ERROR) {
                Toast.makeText(requireContext(), "ERROR $message", Toast.LENGTH_SHORT).show()
            } else if (resultCode == RavePayActivity.RESULT_CANCELLED) {
                Toast.makeText(requireContext(), "CANCELLED $message", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    private fun checkout() {
        when(selectedPaymentOption){
            SelectedPaymentOption.PAYSTACK -> {
                findNavController().navigate(R.id.paystackFragment2)
            }
            SelectedPaymentOption.FLUTTERWAVE ->{
                val amount = binding.textAirtime.text
                val re = Regex("[^0-9]")
                val formatAmount = re.replace(amount,"")
                beginFlutterwaveTransaction(requireActivity(), "Peter", "Eze", "user@example.com","naration","Transaction Reference",formatAmount)
            }

            else ->{}

        }
    }
}

enum class SelectedPaymentOption{
    FLUTTERWAVE,
    PAYSTACK,
    NONE
}