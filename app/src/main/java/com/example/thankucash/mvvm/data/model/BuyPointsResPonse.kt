package com.example.thankucash.mvvm.data.model

data class BuyPointsResPonse(
    val Amount: Double,
    val Charge: Double,
    val TotalAmount: Double,
    val PaymentMode: String,
    val PaymentReference: String,
    val PaystackKey: String,
    val EmailAddress: String,
    val Currency: String,
    val FlutterwaveKey: String
)

