package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.DataPlansItemBinding
import com.example.thankucash.databinding.DstvPlansItemBinding
import com.example.thankucash.databinding.ElectricityItemsListBinding
import com.example.thankucash.databinding.ElectricityPlansItemBinding
import com.example.thankucash.mvvm.data.model.listModel.DataPlansItemList
import com.example.thankucash.mvvm.data.model.listModel.DstvPlansItem
import com.example.thankucash.mvvm.data.model.listModel.ElectricityPlansItem

class ElectricityPlanItemAdapter(
    private val onClick:(item: ElectricityPlansItem) -> Unit
): RecyclerView.Adapter<ElectricityPlanItemAdapter.ElectricityItemViewHolder>() {
    private var electricityItemList: ArrayList<ElectricityPlansItem> = arrayListOf()
    private var selectedPos = -1

     inner class ElectricityItemViewHolder(val binding: ElectricityPlansItemBinding): RecyclerView.ViewHolder(binding.root) {
         var cardView = binding.electricityCardView
         var constraintLayout = binding.electricityConstraint2

         fun bind(electricityItem: ElectricityPlansItem) {
             binding.walletTopText.text = electricityItem.electricityName
             binding.earnvalue.text = electricityItem.electricityEarn
             binding.electricityAmount.text = electricityItem.amount.toString()
             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 notifyItemChanged(selectedPos)
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }

             binding.electricityCardView.setOnClickListener { onClick(electricityItem) }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ElectricityItemViewHolder {
          val inflater = ElectricityPlansItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return ElectricityItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: ElectricityItemViewHolder, @SuppressLint("RecyclerView") position: Int) {
          holder.bind(electricityItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int =electricityItemList.size
     fun submitList (list: ArrayList<ElectricityPlansItem>){
         electricityItemList = list
         notifyDataSetChanged()
     }

    companion object {
        private const val TAG = "AirtimeAdapter"
    }
}