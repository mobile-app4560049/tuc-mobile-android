package com.example.thankucash.mvvm.presentation.ui.screens.dataplans

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentAirtimeBinding
import com.example.thankucash.databinding.FragmentDataBeneficiaryBinding
import com.example.thankucash.mvvm.presentation.adapters.AirtimeAdapter
import com.example.thankucash.mvvm.presentation.ui.viewmodels.DataAndAirtimeViewModel
import com.example.thankucash.mvvm.utils.validation.Constant
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class AirtimeFragment : Fragment() {
    private var _binding: FragmentAirtimeBinding? = null
    private val binding get() = _binding!!
    private val shareViewModel: ShareViewModel by activityViewModels()
    lateinit var airtimeAdapter: AirtimeAdapter
    private var currentAmount = 0
    private val airtimeAndDataViewModel: DataAndAirtimeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAirtimeBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        airtimeAdapter = AirtimeAdapter()


        binding.findBeneficiaryTextView.setOnClickListener {
            val bottomSheetDialog = BottomSheetDialog(requireContext())
            val bottomSheetView = LayoutInflater.from(requireContext())
                .inflate(R.layout.beneficiary_history_bottom_sheet, null)
            bottomSheetDialog.setContentView(bottomSheetView)
            bottomSheetDialog.show()

        }

            val model = ViewModelProvider(requireActivity()).get(ShareViewModel::class.java)

            lifecycleScope.launch {
                airtimeAndDataViewModel.amountState.collect {
                    currentAmount = 0
                    binding.textInput.text = it.toString()
                }
            }

            binding.buyAirtimeBtn.setOnClickListener {
                val amount = binding.textInput.text.toString().trim()
                if (amount.isEmpty()) {
                    Toast.makeText(requireContext(), "Please enter an amount", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    findNavController().navigate(R.id.fundWalletPaymentOptionsFragment)
                }
                model.sendMessage(binding.textInput.text.toString())
            }


            binding.recyclerView.apply {
                adapter = airtimeAdapter
                layoutManager = LinearLayoutManager(
                    requireContext(), LinearLayoutManager.HORIZONTAL, false
                )
            }
            airtimeAdapter.submitList(Constant.airtimeList)

            binding.apply {
                buyAirtimeBtn.setOnClickListener { findNavController().navigate(R.id.airtimePaymentOptionsFragment) }
            }


            binding.airtimeContactSelection.setOnClickListener {
                var i = Intent(Intent.ACTION_PICK)
                i.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(i, 111)
            }



            binding.apply {
                val nairaSign = "\u20A6"
                twoBtn.setOnClickListener { inputValue(100) }
                binding.twoBtn.text = "$nairaSign 100"
                threeBtn.setOnClickListener { inputValue(200) }
                binding.threeBtn.text = "$nairaSign 200"
                fourBtn.setOnClickListener { inputValue(500) }
                binding.fourBtn.text = "$nairaSign 500"
                fiveBtn.setOnClickListener { inputValue(1000) }
                binding.fiveBtn.text = "$nairaSign 1000"
                sixBtn.setOnClickListener { inputValue(2000) }
                binding.sixBtn.text = "$nairaSign 2000"
                sevenBtn.setOnClickListener { inputValue(3000) }
                binding.sevenBtn.text = "$nairaSign 3000"
            }
        }


        private fun setButtonStroke(value: Int) {
            val nairaSign = "₦"
            if (value == 100) {
                binding.twoBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.orange)
                binding.twoBtn.text = "$nairaSign$value"
            } else {
                binding.twoBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.stroke_color)
                binding.twoBtn.text = "$nairaSign$value"
            }
            if (value == 200) {
                binding.threeBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.orange)
                binding.threeBtn.text = "$nairaSign$value"
            } else {
                binding.threeBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.stroke_color)
                binding.threeBtn.text = "$nairaSign$value"
            }
            if (value == 500) {
                binding.fourBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.orange)
                binding.fourBtn.text = "$nairaSign$value"
            } else {
                binding.fourBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.stroke_color)
                binding.fourBtn.text = "$nairaSign$value"
            }
            if (value == 1000) {
                binding.fiveBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.orange)
                binding.fiveBtn.text = "$nairaSign$value"
            } else {
                binding.fiveBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.stroke_color)
                binding.fiveBtn.text = "$nairaSign$value"
            }
            if (value == 2000) {
                binding.sixBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.orange)
                binding.sixBtn.text = "$nairaSign$value"
            } else {
                binding.sixBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.stroke_color)
                binding.sixBtn.text = "$nairaSign$value"
            }
            if (value == 3000) {
                binding.sevenBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.orange)
                binding.sevenBtn.text = "$nairaSign$value"
            } else {
                binding.sevenBtn.strokeColor =
                    ContextCompat.getColorStateList(requireContext(), R.color.stroke_color)
                binding.sevenBtn.text = "$nairaSign$value"
            }


        }

        private fun inputValue(value: Int) {
            setButtonStroke(value)
            lifecycleScope.launch {
                airtimeAndDataViewModel.setAmount(currentAmount + value)
            }
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 111 && resultCode == Activity.RESULT_OK) {
                var contactUri: Uri = data?.data ?: return
                var cols = arrayOf(
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                )
                var rs = requireActivity().contentResolver.query(contactUri, cols, null, null, null)
                if (rs?.moveToFirst()!!) {
                    binding.airtimeContact.setText(rs.getString(0))
                }


            }
        }


        override fun onStop() {
            super.onStop()
            airtimeAndDataViewModel.setAmount(0)
            airtimeAndDataViewModel.setAirtimeProvider(" ")
        }

    }

