package com.example.thankucash.mvvm.presentation.ui.screens.tablayout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentWishlistViewPagerBinding
import com.example.thankucash.mvvm.presentation.adapters.ElectricityAndUtilityBillsViewPagerAdapter
import com.example.thankucash.mvvm.presentation.adapters.WishlistTabLayoutAdapter
import com.example.thankucash.mvvm.presentation.viewmodel.TabStateViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class WishlistViewPagerFragment : Fragment() {
    private var _binding: FragmentWishlistViewPagerBinding? = null
    private val binding get() = _binding!!

    private val tabStateViewModel : TabStateViewModel by activityViewModels()
    private val tabTitles = arrayListOf("Saved Items", "Utility")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentWishlistViewPagerBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.wishlistArrowBack.setOnClickListener { activity?.onBackPressed() }

        binding.viewpagerWishlist.adapter = WishlistTabLayoutAdapter(this)

        setUpTabLayoutWitViewPager()

        lifecycleScope.launch{
            tabStateViewModel.wishListTabState.collect{
                val tab = binding.tabLayoutWishlist.getTabAt(it)
                tab?.select()
                binding.viewpagerWishlist.setCurrentItem(it,false)
            }
        }
    }

    private fun setUpTabLayoutWitViewPager() {
        binding.viewpagerWishlist.apply {
            val tab = binding.tabLayoutWishlist.getTabAt(1)
            tab?.select()
            TabLayoutMediator(
                binding.tabLayoutWishlist,
                binding.viewpagerWishlist
            ) { tab, position ->
                tab.text = tabTitles[position]
                binding.wishListText.text = tabTitles[position]
            }.attach()
        }
        binding.tabLayoutWishlist.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    tabStateViewModel.setUtilityTabState(tab.position)
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }
}