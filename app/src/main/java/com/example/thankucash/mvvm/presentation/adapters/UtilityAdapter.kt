package com.example.thankucash.mvvm.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.UtilityListItemsBinding
import com.example.thankucash.mvvm.data.model.listModel.UtilityItem

class UtilityAdapter: RecyclerView.Adapter<UtilityAdapter.UtilityItemViewHolder>() {
    private var utilityItemList: ArrayList<UtilityItem> = arrayListOf()
    private var selectedPos = -1

     inner class UtilityItemViewHolder(val binding: UtilityListItemsBinding): RecyclerView.ViewHolder(binding.root) {
         fun bind(dealsItem: UtilityItem) {
             binding.airtimeText.text  = dealsItem.text
             binding.imageAirtime.setImageResource(dealsItem.image)
             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 selectedPos = absoluteAdapterPosition
                 notifyDataSetChanged()

             }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UtilityItemViewHolder {
          val inflater = UtilityListItemsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return UtilityItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: UtilityItemViewHolder, position: Int) {
          holder.bind(utilityItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int = utilityItemList.size
     fun submitList (list: ArrayList<UtilityItem>){
         utilityItemList = list
     }
}