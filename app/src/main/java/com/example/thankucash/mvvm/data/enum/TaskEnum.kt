package com.example.thankucash.mvvm.data.enum

enum class TaskEnum(val taskType:String) {
    REGISTER("userregistration"),
    LOGIN("userlogin"),
    VERIFY("userverification"),
    RESETPIN("resetpin"),
    UPDATEPIN("updatepin"),
    REFERRAL("updaterefid"),
    TOPDEALS("getdealpromotions"),
    BUYDEALS("buydeal_initizalizev2"),
    BUYDEALCONFIRMS("buydeal_confirm"),
    GETDEALCODE("getdealcode"),
    BUYPOINTSINITIALIZED("buypointinitialize"),
    BUYPOINTSVERIFY("buypointverify"),
    BUYDEALSREGISTER("buydeal_initizalize")


}