package com.example.thankucash.mvvm.data.model

data class Delivery(
    val AccountId: Int,
    val AccountKey: Any,
    val DeliveryFee: String,
    val DeliveryPartner: DeliveryPartner,
    val DeliveryPartners: String,
    val FromAddressId: Int,
    val FromAddressReference: Any,
    val PackageId: Int,
    val PackageKey: Any,
    val ParcelId: Int,
    val ParcelKey: Any,
    val ShipmentId: Int,
    val ShipmentKey: String,
    val ToAddressId: Int,
    val ToAddressReference: Any
)