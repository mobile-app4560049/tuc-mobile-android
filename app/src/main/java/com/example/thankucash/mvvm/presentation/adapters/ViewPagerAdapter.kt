package com.example.thankucash.mvvm.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.thankucash.mvvm.presentation.ui.screens.onboarding.OnBoardingFragment1
import com.example.thankucash.mvvm.presentation.ui.screens.onboarding.OnBoardingFragment2
import com.example.thankucash.mvvm.presentation.ui.screens.onboarding.OnBoardingFragment3

class ViewPagerAdapter(fragment: Fragment
): FragmentStateAdapter(fragment) {

   private val fragmentList = listOf(
       OnBoardingFragment1(),
       OnBoardingFragment2(),
       OnBoardingFragment3()

    )

    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
       return fragmentList[position]
    }
}