package com.example.thankucash.mvvm.data.model

data class PaymentDetailsX(
    val DeliveryCharges: Double,
    val ItemAmount: Double,
    val ItemCount: Int,
    val PaymentTypeCode: String,
    val PaymentTypeId: Int,
    val PaymentTypeName: String,
    val TotalAmount: Double
)