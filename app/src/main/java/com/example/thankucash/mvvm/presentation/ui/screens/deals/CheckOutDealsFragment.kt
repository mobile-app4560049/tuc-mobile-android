
package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentCheckOutDealsBinding
import com.example.thankucash.mvvm.data.network.NetworkConstants.Companion.SPINNER_HEIGHT
import com.ikhiloyaimokhai.nigeriastatesandlgas.Nigeria
import dagger.hilt.android.AndroidEntryPoint
import java.lang.reflect.Field


@AndroidEntryPoint
class CheckOutDealsFragment : Fragment() {
    private var _binding: FragmentCheckOutDealsBinding? = null
    private val binding get() = _binding!!
    private val shareViewModel: ShareViewModel by activityViewModels()

    private lateinit var mStateSpinner: Spinner
    private lateinit var mLgaSpinner: Spinner
    private lateinit var states: Array<String>
    private var mState: String = ""
    private var mLga: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCheckOutDealsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val saveBtn = binding.saveItemsBtn
        val nameEditText = binding.nameEditText
        val mobileNumberEditText = binding.mobileNumberEditText
        val emailEditText = binding.emailEditText
        val addressEditText = binding.addressEditText

        saveBtn.setOnClickListener {
            if (nameEditText.text.isNullOrEmpty() ||
                mobileNumberEditText.text.isNullOrEmpty() ||
                emailEditText.text.isNullOrEmpty()
            ) {
                return@setOnClickListener
            }
        }

        binding.lastPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        val model = shareViewModel
        if (model.topDeal != null) {
            var difference = shareViewModel.topDeal?.ActualPrice!! - shareViewModel.topDeal?.SellingPrice!!
            var percentage = difference / shareViewModel.topDeal?.ActualPrice!! * 100
            binding.percentz.text = "${percentage}%"
            binding.itemName.text = "NGN ${model.topDeal?.Title}"
            binding.lastPrice.text = "${model.topDeal?.SellingPrice}"
            Glide.with(this)
                .load(model.topDeal?.ImageUrl)
                .placeholder(R.drawable.imagefan)
                .into(binding.imageItem)
            binding.currentPrice.text = "${model.topDeal?.ActualPrice}"
        }


        binding.apply {
            checkOutArrowBack2.setOnClickListener { activity?.onBackPressed() }
            storePickUp.setOnClickListener { findNavController().navigate(R.id.checkOutOrderSummaryFragment) }
        }

        binding.apply {
            addIncrement.setOnClickListener {
                val currentValue = binding.productItem.text.toString().substringBefore(" ").toInt()
                val newValue = currentValue + 1
                binding.productItem.text = "$newValue Product from ThankUCash Merchant"
            }
            subtractDecrement.setOnClickListener {
                val currentValue = binding.productItem.text.toString().substringBefore(" ").toInt()
                val newValue = currentValue - 1
                binding.productItem.text = "$newValue Product from ThankUCash Merchant"
            }
        }


        mStateSpinner = binding.stateSpinner
        mLgaSpinner = binding.lgaSpinner

        resizeSpinner(mStateSpinner, SPINNER_HEIGHT)
        resizeSpinner(mLgaSpinner, SPINNER_HEIGHT)

        states = Nigeria.getStates().toTypedArray();

        setUpSpinners()

    }


    private fun setUpSpinners() {
        val statesAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, states)
        statesAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        statesAdapter.notifyDataSetChanged()
        mStateSpinner.adapter = statesAdapter

        mStateSpinner.setSelection(0)

        mStateSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                mState = parent?.getItemAtPosition(position) as String
                setUpStatesSpinner(position)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
    }

    private fun setUpStatesSpinner(position: Int) {
        val list = Nigeria.getLgasByState(states[position]).toMutableList()
        setUpLgaSpinner(list)
    }

    private fun setUpLgaSpinner(lgas: List<String> ) {
        val lgaAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, lgas)
        lgaAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        mLgaSpinner.adapter = lgaAdapter

        mLgaSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                mLga = parent.getItemAtPosition(position) as String
                Toast.makeText(requireContext(), "state: $mState lga: $mLga", Toast.LENGTH_LONG).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }
    private fun resizeSpinner(spinner: Spinner, height: Int) {
        try {
            val popup: Field = Spinner::class.java.getDeclaredField("mPopup")
            popup.setAccessible(true)
            val popupWindow = popup.get(spinner) as ListPopupWindow
            popupWindow.height = height
        } catch (ex: NoClassDefFoundError) {
            ex.printStackTrace()
        } catch (ex: ClassCastException) {
            ex.printStackTrace()
        } catch (ex: NoSuchFieldException) {
            ex.printStackTrace()
        } catch (ex: IllegalAccessException) {
            ex.printStackTrace()
        }
    }



    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}