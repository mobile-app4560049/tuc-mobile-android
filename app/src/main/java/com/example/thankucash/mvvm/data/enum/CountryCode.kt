package com.example.thankucash.mvvm.data.enum

import com.example.thankucash.mvvm.data.model.listModel.Countries
import com.example.thankucash.mvvm.data.model.UpdateUserAccount

enum class CountryCode(val countries: Countries) {
    NGN(Countries(0,"Nigeria","234"))

}
