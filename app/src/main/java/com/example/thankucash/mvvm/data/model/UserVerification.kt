package com.example.thankucash.mvvm.data.model


data class UserVerification(
    var task : String,
    var host : Int,
    var countryIsd: String,
    var mobileNumber : String,
    var requestToken : Int,
    var accessCode : String,
    var countryId : Int,
    var countryKey: String
)
