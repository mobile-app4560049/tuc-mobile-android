package com.example.thankucash.mvvm.data.model

data class UserLogin(
    var mobileNumber: String,
    var accessPin : String,
    var countryIsd : String,
)
