package com.example.thankucash.mvvm.data.model

data class DeliveryPartnerX(
    val CarrierId: Int,
    val DeductablePrice: Double,
    val DeliveryCharge: Double,
    val DeliveryNote: String,
    val DeliveryPartnerKey: String,
    val ExpectedDeliveryTime: String,
    val IconUrl: String,
    val KwikKey: String,
    val Name: String,
    val OriginalPrice: Double,
    val PayablePrice: Double,
    val RateId: String,
    val RedisKey: String,
    val SavedPrice: Int
)