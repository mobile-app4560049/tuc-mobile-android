package com.example.thankucash.mvvm.utils.validation

import android.util.Log
import java.net.InetSocketAddress
import java.net.Socket

object Connectivity {
    fun hasConnectivity(): Boolean{
        return try {
            val socket = Socket()
            socket.connect(InetSocketAddress("8.8.8.8", 53), 1500)
            socket.close()
            Log.d("Connection", "PING Success")
            true
        }catch (e: Exception){
            Log.d("Connection", "PING Error.${e}")
            false
        }
    }
}