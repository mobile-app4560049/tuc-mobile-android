package com.example.thankucash.mvvm.data.model

data class UserVerifyPhoneResponse(
    val Status: String,

    val Message: String,

    val Result: Result,

    val ResponseCode: String,

    val Mode: String
)

data class Result (
    val isNewAccount: Boolean,

    val mobileNumber: String
)

