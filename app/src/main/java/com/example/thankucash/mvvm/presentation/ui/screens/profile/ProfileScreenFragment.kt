package com.example.thankucash.mvvm.presentation.ui.screens.profile

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentProfileScreenBinding
import com.example.thankucash.mvvm.data.model.UpdateUserAccount
import com.example.thankucash.mvvm.presentation.adapters.GenderAdapter
import com.example.thankucash.mvvm.presentation.viewmodel.LoginViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.ReferralProfileViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.UpdateUserProfileViewModel
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class ProfileScreenFragment : Fragment() {
    private var _binding : FragmentProfileScreenBinding? = null
    private val binding get() = _binding!!
    lateinit var genderAdapter: GenderAdapter
    private lateinit var dialog: Dialog
    private val updateUserProfileViewModel: UpdateUserProfileViewModel by activityViewModels()


    companion object{
        private const val PICK_IMAGE_REQUEST = 1
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentProfileScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }

        genderAdapter = GenderAdapter(requireContext())
        binding.genderSpinner.adapter = genderAdapter

        binding.apply {
            changeProfileBtn.setOnClickListener {
                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                intent.type = "image/*"
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST) }
            updatedProfileBackArrow.setOnClickListener { activity?.onBackPressed()}

        }

        val ccp = binding.countryPicker
        val phone = binding.numberInput
        ccp.registerCarrierNumberEditText(phone)


        binding.dateOfBirthTextInput.editText?.setOnClickListener {
            val datePicker = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val calendar = Calendar.getInstance().apply {
                    set(year, monthOfYear, dayOfMonth)
                }
                val dateFormat = SimpleDateFormat("MM-dd-yyyy", Locale.US)
                val selectedDate = dateFormat.format(calendar.time)
                binding.dateOfBirthTextInput.editText?.setText(selectedDate)
            }

            val calendar = Calendar.getInstance()
            val datePickerDialog = context?.let { it1 ->
                DatePickerDialog(
                    it1,
                    datePicker,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
                )
            }
            datePickerDialog!!.show()
        }

        binding.updateProfileBtn.setOnClickListener {
            var profileUpdate = UpdateUserAccount(
                1,
                binding.firstName.text.toString(),
                "",
                binding.lastName.text.toString(),
                binding.emailInputLayout.toString(),
                "",
                "",
                ""
            )
            updateUserProfile(profileUpdate)
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK  && data != null && data.data != null){
            var uri = data.data

            binding.shapeAbleImage.setImageURI(uri)

        }



    }
    private fun updateUserProfile(updateUserAccount: UpdateUserAccount){
        lifecycleScope.launch{
            updateUserProfileViewModel.updateUserProfile(updateUserAccount)
        }
    }

    private fun setUpObservers() {
        updateUserProfileViewModel.updateUserProfileResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_success)
                    findNavController().popBackStack()
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()

                }
            }
        }
    }



}

