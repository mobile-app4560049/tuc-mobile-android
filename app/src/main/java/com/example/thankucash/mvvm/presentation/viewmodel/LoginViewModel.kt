package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.GenericResult
import com.example.thankucash.mvvm.data.model.UserLogin
import com.example.thankucash.mvvm.data.model.UserLoginResponse
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _loginResponse = MutableLiveData<Resource<GenericResult<UserLoginResponse>>>()
    val loginResponse: LiveData<Resource<GenericResult<UserLoginResponse>>> get() = _loginResponse

    fun loginUser(userLogin: UserLogin){
        _loginResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch(Dispatchers.Default){
            try {
                _loginResponse.value = authRepository.loginUser(userLogin)
            }catch (e: Exception){
                Log.d("erro loading", e.javaClass.simpleName)
            }
        }
    }

}