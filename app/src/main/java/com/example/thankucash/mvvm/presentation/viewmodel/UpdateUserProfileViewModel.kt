package com.example.thankucash.mvvm.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.GenericResult
import com.example.thankucash.mvvm.data.model.UpdateProfileResponse
import com.example.thankucash.mvvm.data.model.UpdateUserAccount
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UpdateUserProfileViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {

    var firstName: String? = null
    var emailAddress: String? = null

    private var _updateUserProfileResponse = MutableLiveData<Resource<GenericResult<UpdateProfileResponse>>>()
    val  updateUserProfileResponse: LiveData<Resource<GenericResult<UpdateProfileResponse>>> get() =  _updateUserProfileResponse
    fun updateUserProfile(updateUserAccount: UpdateUserAccount){
        _updateUserProfileResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch {
            _updateUserProfileResponse.value = authRepository.updateUserProfile(updateUserAccount)
        }
    }
}