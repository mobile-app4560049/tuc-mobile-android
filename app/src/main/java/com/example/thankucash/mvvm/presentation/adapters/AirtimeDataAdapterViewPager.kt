package com.example.thankucash.mvvm.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.thankucash.mvvm.presentation.ui.screens.dataplans.AirtimeFragment
import com.example.thankucash.mvvm.presentation.ui.screens.dataplans.DataFragment
import com.example.thankucash.mvvm.presentation.ui.screens.fundwallet.WalletHistoryFragment

class AirtimeDataAdapterViewPager(fragment: Fragment): FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> AirtimeFragment()
            1 -> DataFragment()
            else -> DataFragment()
        }
    }
}