package com.example.thankucash.mvvm.data.model.listModel

data class DealsItem(
    val imageView: Int,
    val buttonColor: String,
    val itemType: String,
    val price: Int,
    val itemNew: String,
    val calendarImage: Int,
    val qtyType: String,
    val orderNumber: Int
)
