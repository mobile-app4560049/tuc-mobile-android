package com.example.thankucash.mvvm.data.model

data class UpdatePin(
    var task : String,
    var countryIsd: String,
    var mobileNumber : String,
    var tempAccessPin : String,
    var newAccessPin: String,
    var countryId : Int,
    var countryKey: String
)
