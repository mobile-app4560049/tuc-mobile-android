package com.example.thankucash.mvvm.utils.validation

import android.widget.Toast
import com.example.thankucash.R

object FieldValidations {

    fun verifyName(name: String): Boolean{
        val regex = Regex("^[a-zA-Z]*\$")
        return name.isNotBlank() && name.matches(regex) && name.length >= 2
    }


    fun verifyEmail(email: String): Boolean{
        val regex = Regex("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$")
        return email.isNotBlank() && email.matches(regex)
    }

    fun verifyPhoneNumber(number: String): Boolean {
            val regex = Regex("^(234|0)(8[01])|(9([01]))|([7])([0])\\d{8}$")
            return number.isNotBlank() && number.matches(regex)
        }


    fun verifyPin(pin: String): Boolean{
        val regex = Regex("^[0-9]{4}$")
        return regex.matches(pin)
    }


        fun validatePhoneNumberFormat(countryCode: String?, phoneNumber: String?): String? {
            val regex = when (countryCode) {
                "234" -> """^0[789][01]\d{8}$"""
                else -> null
            }

            if (phoneNumber != null) {
                return if (regex != null && !phoneNumber.matches(regex.toRegex())) {
                    "Invalid phone number format"
                } else {
                    null
                }
            }
        return null
    }
}