package com.example.thankucash.mvvm.utils.validation

import com.example.thankucash.R
import com.example.thankucash.mvvm.data.model.listModel.*

object Constant {

    val DummyList = arrayListOf(
        "Deal Description less than 100",
        "2",
        "3",
        "4",
        "5",
        "6"
    )
    val electricityProviderList = arrayListOf(
        UtilityItem(R.drawable.dstvlogo,"Earn upto 0.75%","DSTV"),
        UtilityItem(R.drawable.gotv,"Earn upto 0.75%","GOTV"),
        UtilityItem(R.drawable.start_time,"Earn upto 0.75%","STARTIMES"),
        UtilityItem(R.drawable.start_time,"Earn upto 0.75%","STARTIMES")
    )

    val electricityList = arrayListOf(
        UtilityItem(R.drawable.ekdc,"Earn upto 0.75%","EKDC"),
        UtilityItem(R.drawable.eedc_logo,"Earn upto 0.75%","EEDC"),
        UtilityItem(R.drawable.phed,"Earn upto 0.75%","PHED"),
        UtilityItem(R.drawable.ekdc,"Earn upto 0.75%","EKDC"),
        UtilityItem(R.drawable.ikeje_logo,"Earn upto 0.75%","IKEJE"),
        UtilityItem(R.drawable.kaduna_logo,"Earn upto 0.75%","KADUNA"),
        UtilityItem(R.drawable.ekdc,"Earn upto 0.75%","EKDC"),
        UtilityItem(R.drawable.ibada_logo,"Earn upto 0.75%","IBADA"),
        UtilityItem(R.drawable.lumos_logo,"Earn upto 0.75%","LUMOS"),
        UtilityItem(R.drawable.privida_logo,"Earn upto 0.75%","PRIVIDA"),
        UtilityItem(R.drawable.ibada_logo,"Earn upto 0.75%","IBADA"),
        UtilityItem(R.drawable.ikeje_logo,"Earn upto 0.75%","IKEJE")
    )

    val airtimeList = arrayListOf(
        UtilityItem(R.drawable.mtn,"Earn upto 1.25%","MTN"),
        UtilityItem(R.drawable.airtel,"Earn upto 1.25%","AIRTEL"),
        UtilityItem(R.drawable.mobile_etisalat,"Earn upto 1.25%","ETISALAT"),
        UtilityItem(R.drawable.mtn,"Earn upto 1.25%","AIRTEL")
    )

    val dataList = arrayListOf(
        UtilityItem(R.drawable.mtn,"Earn upto 1.25%","MTN"),
        UtilityItem(R.drawable.airtel,"Earn upto 1.25%","AIRTEL"),
        UtilityItem(R.drawable.smile,"Earn upto 1.25%","AIRTEL"),
        UtilityItem(R.drawable.ipn,"Earn upto 1.25%","IPN")
    )

    val dataPlansList = arrayListOf(
        DataPlansItemList(
            "1GB DATA BUNDLE (7 days)",
            "Earn 6.25%",
            500
        ),
        DataPlansItemList(
            "2GB DATA BUNDLE",
            "Earn 15%",
            1200
        ),
        DataPlansItemList(
            "1GB DATA BUNDLE (7 days)",
            "Earn 18.75%",
            1500
        ),
        DataPlansItemList(
            "3GB DATA BUNDLE",
            "Earn 16.75%",
            1500
        ),
        DataPlansItemList(
            "1.5GB DATA BUNDLE (1 days)",
            "Earn 4.38%",
            350
        ),
        DataPlansItemList(
            "750MB DATA BUNDLE (7 days)",
            "Earn 6.25%",
            500
        ),
        DataPlansItemList(
            "250MB DATA BUNDLE (7 days)",
            "Earn 3.25%",
            300
        )
    )

    val dstvPlansList = arrayListOf(
        DstvPlansItem(
            R.drawable.naira,
            "Asia Standalone",
            "Earn N160.12",
            7100

        ),
        DstvPlansItem(
            R.drawable.naira,
            "Compact Plus and Asia",
            "Earn N181.88",
            21350
        ),
        DstvPlansItem(
            R.drawable.naira,
            "Compact Plus, French Plus and  HDPVR/ExtraView",
            "Earn N181.88",
            26450
        ),
        DstvPlansItem(
            R.drawable.naira,
            "Compact Plus, Asia and HDPVR/ExtraView",
            "Earn N109.12",
            24450
        ),
        DstvPlansItem(
            R.drawable.naira,
            "Compact Plus, French Touch and HDPVR/ExtraView",
            "Earn N160.12",
            14550
        ),
        DstvPlansItem(
            R.drawable.naira,
            "Contact and French Plus",
            "Earn N137.25",
            18300
        ),
        DstvPlansItem(
            R.drawable.naira,
            "Compact Plus, French Plus and HDPVR/ExtraView",
            "Earn N181.88",
            14550
        ),
    )
    val electricityPlans = arrayListOf(
        ElectricityPlansItem(
            "EKDC PREPAID",
            "EARN 0.25&",
            1000
        ),
        ElectricityPlansItem(
            "EKDC POSTPAID",
            "EARN 0.25&",
            1500
        ),
        ElectricityPlansItem(
            "EKDC ORDER ID",
            "EARN 0.25&",
            1700
        ),
    )

    val rewardList = arrayListOf(
        RewardsItem(
            "Rewarded at",
            "Ebanao Lekki",
            R.drawable.calendar_2_fill,
            "27-9-2022   |",
            "12:22pm",
            "20.00"
        ),
        RewardsItem(
            "Rewarded at",
            "Ebanao Chevron",
            R.drawable.calendar_2_fill,
            "27-9-2022   |",
            "12:22pm",
            "20.00"
        ),
        RewardsItem(
            "Rewarded at",
            "Enyo filling station, Ajah",
            R.drawable.calendar_2_fill,
            "27-9-2022   |",
            "12:22pm",
            "20.00"
        ),
    )

    val airtimeListUser = arrayListOf(
        AirtimeItem(
            "Compact Plus and Asia",
            "Dstv",
            R.drawable.calendar_2_fill,
            "27-9-2022   |",
            "12:22pm",
            "200.00"
        ),
        AirtimeItem(
            "Data Bundle ",
            "NGN 1500 - 1.5GB MTN Data Bundle",
            R.drawable.calendar_2_fill,
            "27-9-2022   |",
            "12:22pm",
            "200.00"
        ),
        AirtimeItem(
            "Airtime Top Up",
            "Glo",
            R.drawable.calendar_2_fill,
            "27-9-2022   |",
            "12:22pm",
            "20.00"
        ),
    )

    val walletTop = arrayListOf(
        WalletItem(
            "Wallet Top Up",
            "With Paystack",
            R.drawable.calendar_2_fill,
        "27-09-2022   |",
            "12:22 pm",
            "N200,000.00"
        ),

        WalletItem(
            "Wallet Top Up",
            "With Paystack",
            R.drawable.calendar_2_fill,
            "27-09-2022   |",
            "12:22 pm",
            "N200,000.00"
        ),
        WalletItem(
            "Wallet Top Up",
            "With Flutter",
            R.drawable.calendar_2_fill,
            "27-09-2022   |",
            "12:22 pm",
            "N200,000.00"
        ),
    )

    val dataBundle = arrayListOf(
        DataBundleListItem(
                "Data Bundle ",
                "NGN 1500 - 1.5GB MTN Data Bundle",
                R.drawable.calendar_2_fill,
                "27-9-2022   |",
                "12:22pm",
                "200.00"
            ),
        DataBundleListItem(
            "Data Bundle ",
            "NGN 1500 - 1.5GB MTN Data Bundle",
            R.drawable.calendar_2_fill,
            "27-9-2022   |",
            "12:22pm",
            "200.00"
        ),
        DataBundleListItem(
            "Data Bundle ",
            "NGN 1500 - 1.5GB MTN Data Bundle",
            R.drawable.calendar_2_fill,
            "27-9-2022   |",
            "12:22pm",
            "200.00"
        ),
        DataBundleListItem(
            "Data Bundle ",
            "NGN 1500 - 1.5GB MTN Data Bundle",
            R.drawable.calendar_2_fill,
            "27-9-2022   |",
            "12:22pm",
            "200.00"
        ),
    )

    val deals = arrayListOf(
        DealsItem(
            R.drawable.imagefan,
            "green",
            "18 Inches Standing Fan-Black. Authomatic Rechargeable",
            500,
            "Unused",
            R.drawable.calendar_2_fill,
            "Qty 1",
            1234567
        ),
        DealsItem(
            R.drawable.imagefan,
            "red",
            "18 Inches Standing Fan-Black. Authomatic Rechargeable",
            500,
            "Expired",

            R.drawable.calendar_2_fill,
            "Qty 1",
            1234567
        ),
        DealsItem(
            R.drawable.imagefan,
            "purple",
            "18 Inches Standing Fan-Black. Authomatic Rechargeable",
            500,
            "Used",
            R.drawable.calendar_2_fill,
            "Qty 1",
            1234567
        ),
        DealsItem(
            R.drawable.imagefan,
            "purple",
            "18 Inches Standing Fan-Black. Authomatic Rechargeable",
            500,
            "Delivered",
            R.drawable.calendar_2_fill,
            "Qty 1",
            1234567
        ),
    )
    val beneficairy = arrayListOf(
        BeneficiaryItems(
            "Victoria08075333212",
            "MTNQuickCharge(Top-up)"
        ),
        BeneficiaryItems(
            "Odufa08075333212",
            "MTNQuickCharge(Top-up)"
        ),
        BeneficiaryItems(
            "Sherif08075332212",
            "MTNQuickCharge(Top-up)"
        ),

    )
}