package com.example.thankucash.mvvm.data.model

data class BuyPointsVerify(
    val Task: String,
    val AccountKey: String,
    val AccountId: Int,
    val Amount: Int,
    val PaymentReference: String,
    val RefTransactionId: String,
    val RefStatus: String,
    val RefMessage: String,
    val PaymentMode: String,
)
