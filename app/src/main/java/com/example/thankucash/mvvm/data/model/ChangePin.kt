package com.example.thankucash.mvvm.data.model


data class ChangePin(
    val authAccountKey: String,
    val referenceNumber: String,
    val oldPin: String,
    val newPin: String
)
