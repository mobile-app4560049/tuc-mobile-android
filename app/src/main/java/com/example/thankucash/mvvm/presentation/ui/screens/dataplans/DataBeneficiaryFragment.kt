package com.example.thankucash.mvvm.presentation.ui.screens.dataplans

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.databinding.BeneficalCongratulationsBottomSheetBinding
import com.example.thankucash.databinding.BeneficiaryHistoryBottomSheetBinding
import com.example.thankucash.databinding.FragmentDataBeneficiaryBinding
import com.example.thankucash.mvvm.presentation.adapters.BeneficiaryHistoryAdapter
import com.example.thankucash.mvvm.utils.validation.Constant
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DataBeneficiaryFragment : Fragment() {
    private var _binding: FragmentDataBeneficiaryBinding? = null
    private val binding get() = _binding!!
    lateinit var beneficiaryHistoryAdapter: BeneficiaryHistoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDataBeneficiaryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        beneficiaryHistoryAdapter = BeneficiaryHistoryAdapter()

        binding.recyclerView.apply {
            adapter = beneficiaryHistoryAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL,false
            )
        }
        beneficiaryHistoryAdapter.submitList(Constant.beneficairy)

    }

}