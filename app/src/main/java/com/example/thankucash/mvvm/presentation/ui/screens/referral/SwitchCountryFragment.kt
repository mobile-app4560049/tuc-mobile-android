package com.example.thankucash.mvvm.presentation.ui.screens.referral

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.thankucash.databinding.FragmentSwitchCountryBinding
import com.example.thankucash.mvvm.utils.validation.Constant
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SwitchCountryFragment : Fragment() {
    private var _binding: FragmentSwitchCountryBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSwitchCountryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.switchCountryBackArrow.setOnClickListener { activity?.onBackPressed() }

        binding.nigeria.setOnClickListener {

        }

    }


    var status = 0

}
