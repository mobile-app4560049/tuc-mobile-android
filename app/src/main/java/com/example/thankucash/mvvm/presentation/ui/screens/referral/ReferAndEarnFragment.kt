package com.example.thankucash.mvvm.presentation.ui.screens.referral

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentReferAndEarnBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.ReferralProfile
import com.example.thankucash.mvvm.presentation.viewmodel.ReferralProfileViewModel
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ReferAndEarnFragment : Fragment() {
    private var _binding : FragmentReferAndEarnBinding? = null
    private val binding get() = _binding!!
    private val referralProfileViewModel: ReferralProfileViewModel by activityViewModels()
    private lateinit var dialog: Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentReferAndEarnBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply { dialog = provideCustomAlertDialog()
            setUpObservers() }


        val model = ViewModelProvider(requireActivity()).get(ShareViewModel::class.java)

        binding.referBackArrow.setOnClickListener { activity?.onBackPressed() }

        binding.customizeBtn.setOnClickListener{
            val amount = binding.customize.text.toString().trim()
            if (amount.isEmpty()){
                Toast.makeText(requireContext(), "Please enter an amount", Toast.LENGTH_SHORT).show()
            }else{
                findNavController().navigate(R.id.referAFriendFragment)
            }
            model.sendMessage(binding.customize.text.toString())

        }


        var referral = ReferralProfile(
            TaskEnum.REFERRAL.taskType,
            binding.referAndEarnText.text.toString(),
            "",
            CountryCode.NGN.countries.countryCode,
            CountryCode.NGN.countries.countryKey
        )
        referralProfile(referral)
    }
    private fun referralProfile(referralProfile: ReferralProfile) {
        lifecycleScope.launch {
            referralProfileViewModel.referralProfile(referralProfile)
        }
    }


    private fun setUpObservers() {
        referralProfileViewModel.referralProfileResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_success)
                    findNavController().popBackStack()
                }
                is Resource.Error -> {

                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }

    }

}