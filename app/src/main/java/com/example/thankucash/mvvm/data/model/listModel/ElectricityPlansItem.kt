package com.example.thankucash.mvvm.data.model.listModel

data class ElectricityPlansItem (
    var electricityName: String,
    var electricityEarn: String,
    var amount: Int
    )