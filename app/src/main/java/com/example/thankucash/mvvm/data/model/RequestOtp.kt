package com.example.thankucash.mvvm.data.model

data class RequestOtp(
    val requestToken : String,
    val countryIsd: String,
    val mobileNumber: String
)
