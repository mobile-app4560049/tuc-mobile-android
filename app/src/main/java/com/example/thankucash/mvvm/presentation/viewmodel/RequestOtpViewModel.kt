package com.example.thankucash.mvvm.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.GenericResult
import com.example.thankucash.mvvm.data.model.RequestOtp
import com.example.thankucash.mvvm.data.model.RequestOtpResponse
import com.example.thankucash.mvvm.data.model.UpdatePin
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RequestOtpViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _otpRequestResponse = MutableLiveData<Resource<GenericResult<RequestOtpResponse>>>()
    val otpRequestResponse: LiveData<Resource<GenericResult<RequestOtpResponse>>> get() = _otpRequestResponse

    fun requestOtp(requestOtp: RequestOtp){
        _otpRequestResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch (Dispatchers.Default){
            _otpRequestResponse.value = authRepository.requestOtp(requestOtp)
        }
    }
}