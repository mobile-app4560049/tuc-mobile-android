package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.toColorInt
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.AirtimeListBinding
import com.example.thankucash.databinding.DataHistoryItemBinding
import com.example.thankucash.databinding.DealsItemListBinding
import com.example.thankucash.databinding.FlashDealsItemBinding
import com.example.thankucash.databinding.UtilityListItemsBinding
import com.example.thankucash.mvvm.data.model.listModel.DealsItem
import java.nio.file.Files.size

class DealsItemListAdapter(
    private val onClick :(item : DealsItem) -> Unit
): RecyclerView.Adapter<DealsItemListAdapter.AirtimeItemViewHolder>() {
    private var dealsItemList: ArrayList<DealsItem> = arrayListOf()
    private var selectedPos = -1

     inner class AirtimeItemViewHolder(val binding: DealsItemListBinding): RecyclerView.ViewHolder(binding.root) {
         var cardView = binding.cardView
         var constraintLayout = binding.constraint

         fun bind(dealsItem: DealsItem) {
             binding.imageView3.setImageResource(dealsItem.imageView)
             binding.usedButton.text = dealsItem.itemNew
             binding.dealsText.text = dealsItem.itemType
             binding.fiveHundredTextView.text = dealsItem.price.toString()
             binding.imageView4.setImageResource(dealsItem.calendarImage)
             binding.quantity.text = dealsItem.qtyType
             binding.orderId.text = dealsItem.orderNumber.toString()


             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 notifyItemChanged(selectedPos)
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }
             binding.cardView.setOnClickListener { onClick(dealsItem) }
         }

     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirtimeItemViewHolder {
          val inflater = DealsItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return AirtimeItemViewHolder(inflater)
     }

     @SuppressLint("ResourceType")
     override fun onBindViewHolder(holder: AirtimeItemViewHolder, position: Int) {
          holder.bind(dealsItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)
         holder.binding.usedButton.backgroundTintList = ContextCompat.getColorStateList(
             holder.itemView.context,
             when(dealsItemList[position].buttonColor){
                 "green" -> R.color.green
                 "red" -> R.color.red
                 "purple" -> R.color.brand_primary
                 else -> R.color.brand_primary
             }
         )
     }

     override fun getItemCount(): Int = dealsItemList.size
     fun submitList (list: ArrayList<DealsItem>){
         dealsItemList = list
     }
}