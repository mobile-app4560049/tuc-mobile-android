package com.example.thankucash.mvvm.data.model

data class BuyDealsRegisterUser(
    val Task: String,
    val ReferenceId: Int,
    val ReferenceKey: String,
    val PaymentSource: String,
    val DeliveryCharge: Int,
    val DeliveryPartnerKey: String? = null,
    val FromAddressId: Int? = null,
    val FromAddressKey: String? = null,
    val ToAddressId: Int,
    val ToAddressKey: String? = null,
    val CarrierId: String? = null,
    val DeliveryEta: Int,
    val ItemCount: Int,
    val CountryId: Int,
    val CountryKey: String
)
