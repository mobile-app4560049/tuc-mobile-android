package com.example.thankucash.mvvm.presentation.ui.screens.fundwallet

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentViewPagerTwoBinding
import com.example.thankucash.mvvm.presentation.adapters.ViewPagerAdapter
import com.example.thankucash.mvvm.presentation.adapters.ViewPagerAdapter2
import com.example.thankucash.mvvm.presentation.viewmodel.TabStateViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.launch

class ViewPagerTwoFragment : Fragment() {
    private var _binding: FragmentViewPagerTwoBinding? = null
    private val binding get() = _binding!!

    private val tabTitles = arrayListOf("Bills Payment (0)", "Rewards (1)", "Top Up", "Deals (4)")

    private val tabStateViewModel : TabStateViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentViewPagerTwoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bottomSheetDialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialogTheme)
        val bottomSheetView = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_deals_history_filter, null)
        bottomSheetDialog.setContentView(bottomSheetView)

        binding.icon.setOnClickListener {
            bottomSheetDialog.show()
        }

        setUpTabLayoutWitViewPager()

        if (tabStateViewModel.shouldNavigateRewardHistory){
            binding.viewpager.setCurrentItem(1)
        }
        binding.apply {
            historyArrowBack.setOnClickListener { findNavController().navigate(R.id.fundWalletDashBoardFragment2) }
        }
    }

    private fun setUpTabLayoutWitViewPager() {
        binding.viewpager.adapter = ViewPagerAdapter2(this)
        TabLayoutMediator(
            binding.tabLayoutFund,
            binding.viewpager
        ) { tab, position ->
            tab.text = tabTitles[position]

        }.attach()


        binding.tabLayoutFund.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {

                if (tab != null) {
                    tabStateViewModel.setRewardsViewPager(tab.position)
                }
                if (tab?.position == 2){binding.icon.visibility= View.GONE} else {
                        binding.icon.visibility = View.VISIBLE
                    }
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }
}

