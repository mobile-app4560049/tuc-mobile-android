package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.MainActivity
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentAllDealsBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.AllDeals
import com.example.thankucash.mvvm.data.model.Datum
import com.example.thankucash.mvvm.data.model.TopDealsResponse
import com.example.thankucash.mvvm.presentation.adapters.AllDealsAdapter
import com.example.thankucash.mvvm.presentation.adapters.TopDealsAdapter
import com.example.thankucash.mvvm.presentation.viewmodel.GetAllDealsViewModel
import com.example.thankucash.mvvm.utils.validation.ConnectivityLiveData
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class AllDealsFragment : Fragment() {
    private var _binding : FragmentAllDealsBinding? = null
    private val binding get() = _binding!!

    private val getAllDealsViewModel: GetAllDealsViewModel by activityViewModels()

    private lateinit var dialog: Dialog
    private val shareViewModel: ShareViewModel by activityViewModels()
    lateinit var allDealsAdapter: AllDealsAdapter
    lateinit var connectivityLiveData: ConnectivityLiveData


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAllDealsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        connectivityLiveData = (activity as MainActivity).connectivityLiveData

        allDealsAdapter = AllDealsAdapter {
            shareViewModel.topDeal = it.Result.Data[0]
            findNavController().navigate(R.id.orderDetailsFragment)
        }


        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }

        connectivityLiveData.observe(
            viewLifecycleOwner
        ) { hasNetwork ->
            if (hasNetwork) getAllDealsViewModel.allDealsResponse

        }


        binding.allDealsArrowBack.setOnClickListener { activity?.onBackPressed() }

        binding.recyclerViewAllDeals.apply {
            adapter = allDealsAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)

        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    getAllDealsViewModel.searchQuery.value = newText
                }
                return true
            }
        })

        binding.searchNotFound.visibility = View.GONE



        binding.apply {
            categories.setOnClickListener {
                findNavController().navigate(R.id.categoriesDealFragment)
            }
            notificationIcon.setOnClickListener { findNavController().navigate(R.id.notificationFragment) }

        }
        binding.filterLayout.setOnClickListener { findNavController().navigate(R.id.productDealsFragment) }

        var dealsAll = AllDeals(
            TaskEnum.TOPDEALS.taskType,
            0,
            Limit = 20,
            "",
            "Newest",
            arrayOf(),
            arrayOf(),
            "",
            0,
            "",
            0,
            0,
            CountryCode.NGN.countries.countryCode,
            CountryCode.NGN.countries.countryKey
        )
        allDeals(dealsAll)

    }


    private fun allDeals(allDeals: AllDeals) {
        lifecycleScope.launch {
            getAllDealsViewModel.getAllDeals(allDeals)
        }
    }

    private fun setUpObservers() {
        getAllDealsViewModel.allDealsResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    requireView().showSnackBar(R.string.call_success)
                    it.data?.Result?.Data?.let { it1 ->
                        getAllDealsViewModel.cachedTopDeals = it1 as List<Datum>
                        updateViews(getAllDealsViewModel.searchQuery.value!!)
                    }

                }
                is Resource.Error -> {
                    dialog.dismiss()

                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
            getAllDealsViewModel.searchQuery.observe(viewLifecycleOwner){ query ->
            updateViews(query)
        }
    }
    fun updateViews(query: String){
        if (query.isEmpty()){
            allDealsAdapter.differ.submitList(getAllDealsViewModel.cachedTopDeals)
            binding.searchNotFound.visibility = View.GONE
            binding.recyclerViewAllDeals.visibility = View.VISIBLE
            return
        }
        if (!getAllDealsViewModel.cachedTopDeals.any{
                it.Title.lowercase().contains(query)
            }){
            binding.searchNotFound.visibility = View.VISIBLE
            binding.recyclerViewAllDeals.visibility = View.GONE
        }else{
            binding.searchNotFound.visibility = View.GONE
            binding.recyclerViewAllDeals.visibility = View.VISIBLE
        }
        allDealsAdapter.differ.submitList(getAllDealsViewModel.cachedTopDeals.filter{
            it.Title.lowercase().contains(query)
        })
    }

}