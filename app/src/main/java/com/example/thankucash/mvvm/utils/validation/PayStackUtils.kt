package com.example.thankucash.mvvm.utils.validation

object PayStackUtils {
    fun validateInput(input: String): Boolean {
        val cardNumberPattern = Regex("\\d{16}")
        val expirationDatePattern = Regex("(0[1-9]|1[0-2])/\\d{2}")
        val ccvPattern = Regex("\\d{3}")
        val values = input.split(",")

        if (values.size != 3) {
            return false
        }
        if (!cardNumberPattern.matches(values[0])) {
            return false
        }
        if (!expirationDatePattern.matches(values[1])) {
            return false
        }
        if (!ccvPattern.matches(values[2])) {
            return false
        }
        return true
    }
}