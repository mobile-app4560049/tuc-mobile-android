package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Message
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.thankucash.R
import com.example.thankucash.databinding.FlashDealsItemBinding
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.data.model.listModel.AirtimeItem
import javax.net.ssl.SSLEngineResult.Status

class FeatureDealsAdapter(
    private val onClick:(item: FlashDealsResponse) -> Unit)
    :RecyclerView.Adapter<FeatureDealsAdapter.DealsItemViewHolder>() {

     inner class DealsItemViewHolder(val binding: FlashDealsItemBinding): RecyclerView.ViewHolder(binding.root) {
         var button = binding.buyNow
         @SuppressLint("SuspiciousIndentation")
         fun bind(flashDealsResponse: Datum) {
             binding.apply {
                 binding.actualPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                 description.text = flashDealsResponse.Title
                 location.text = flashDealsResponse.StateName
                 sellingPrice.text = flashDealsResponse.SellingPrice.toString()
                 actualPrice.text = flashDealsResponse.ActualPrice.toString()
                 binding.buyNow
                 Glide.with(binding.root.context)
                     .load(flashDealsResponse.ImageUrl)
                     .placeholder(R.drawable.rectangle)
                     .into(imageItem)
             }

             val response = FlashDealsResponse(
                 Status = "success",
                 Message = "Success",
                 Result = Rest(
                     Offset = 0,
                     Limit = 1,
                     TotalRecords = 1,
                     Data = listOf(flashDealsResponse)
                 )
             )
             binding.buyNow.setOnClickListener { onClick(response) }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealsItemViewHolder {
          val inflater = FlashDealsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return DealsItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: DealsItemViewHolder, position: Int) {
          holder.bind(differ.currentList[position])
     }
    private val differCallBack = object:DiffUtil.ItemCallback<Datum>() {
        override fun areItemsTheSame(oldItem:Datum,newItem:Datum):Boolean{
            return oldItem.ReferenceID == newItem.ReferenceID
        }
        override fun areContentsTheSame(oldItem:Datum,newItem:Datum):Boolean{
            return oldItem == newItem
        }
    }

    var differ = AsyncListDiffer(this,differCallBack)

    override fun getItemCount(): Int = differ.currentList.size

}