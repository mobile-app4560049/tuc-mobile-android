package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentProductDealsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDealsFragment : Fragment() {
    private var _binding : FragmentProductDealsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentProductDealsBinding.inflate(inflater,container,false)
        return binding.root
    }



    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val radioGroup = binding.radioGroup
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            // Find the checked radio button using the checkedId parameter
            val checkedRadioButton = binding.root.findViewById<RadioButton>(checkedId)
            val radioButtonValue = checkedRadioButton.text
            // Display a toast message with the text of the checked radio button
            Toast.makeText(requireContext(), "Checked radio button value: $radioButtonValue", Toast.LENGTH_SHORT).show()
        }

        val addEditText = binding.editText
        val subtractEditText = binding.editText2

        var addValue = 10000
        var subtractValue = 10000
        addEditText.setText(addValue.toString())
        subtractEditText.setText(subtractValue.toString())

        addEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val newValue = s.toString().toIntOrNull() ?: 0
                if (newValue != addValue) {
                    val diff = newValue - addValue
                    addValue += diff
                    addEditText.setText(addValue.toString())
                }
            }
            override fun afterTextChanged(s: Editable?) {}
        })
        subtractEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val newValue = s.toString().toIntOrNull() ?: 0
                if (newValue != subtractValue) {
                    val diff = newValue - subtractValue
                    subtractValue += diff
                    subtractEditText.setText(subtractValue.toString())
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })
    }




}