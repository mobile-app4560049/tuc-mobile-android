package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.DataPlansItemBinding
import com.example.thankucash.databinding.DstvPlansItemBinding
import com.example.thankucash.mvvm.data.model.listModel.DataPlansItemList
import com.example.thankucash.mvvm.data.model.listModel.DstvPlansItem

class DstvPlanItemAdapter(
    private val onClick:(item: DstvPlansItem) -> Unit
): RecyclerView.Adapter<DstvPlanItemAdapter.DstvItemViewHolder>() {
    private var dstvItemList: ArrayList<DstvPlansItem> = arrayListOf()
    private var selectedPos = -1

     inner class DstvItemViewHolder(val binding: DstvPlansItemBinding): RecyclerView.ViewHolder(binding.root) {
         var cardView = binding.cardViewDstv
         var constraintLayout = binding.constraintDstv

         fun bind(dstvItem: DstvPlansItem) {
             binding.imageView6.setImageResource(R.drawable.naira).toString()
             binding.walletTopText.text = dstvItem.dataName
             binding.earnValue.text = dstvItem.earn
             binding.tAmount.text = dstvItem.amount.toString()
             binding.root.setOnClickListener {
                 if (absoluteAdapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                 notifyItemChanged(selectedPos)
                 selectedPos = absoluteAdapterPosition
                 notifyItemChanged(selectedPos)
             }

             binding.cardViewDstv.setOnClickListener { onClick(dstvItem) }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DstvItemViewHolder {
          val inflater = DstvPlansItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return DstvItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: DstvItemViewHolder, @SuppressLint("RecyclerView") position: Int) {
         holder.bind(dstvItemList[position])
         holder.itemView.isSelected = selectedPos == position
         holder.itemView.setBackgroundResource(if (selectedPos ==position) R.drawable.green_stroke_128dp_corners else R.drawable.grey_stroke_128dp_corners)

     }

     override fun getItemCount(): Int = dstvItemList.size
     fun submitList (list: ArrayList<DstvPlansItem>){
         dstvItemList = list
         notifyDataSetChanged()
     }

    companion object {
        private const val TAG = "AirtimeAdapter"
    }
}