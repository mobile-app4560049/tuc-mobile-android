package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.MainActivity
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentFlashDealsBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.Datum
import com.example.thankucash.mvvm.data.model.FlashDeals
import com.example.thankucash.mvvm.presentation.adapters.DealItemsAdapter
import com.example.thankucash.mvvm.presentation.adapters.FeatureDealsAdapter
import com.example.thankucash.mvvm.presentation.adapters.TopDealsAdapter
import com.example.thankucash.mvvm.presentation.viewmodel.FlashDealsViewModel
import com.example.thankucash.mvvm.utils.validation.ConnectivityLiveData
import com.example.thankucash.mvvm.utils.validation.Constant
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class FlashDealsFragment : Fragment() {
    private var _binding: FragmentFlashDealsBinding? = null
    private val binding get() = _binding!!

    private val flashDealsViewModel: FlashDealsViewModel by activityViewModels()
    private lateinit var dialog: Dialog
    lateinit var featureDealsAdapter: FeatureDealsAdapter
    private val shareViewModel: ShareViewModel by activityViewModels()

    lateinit var connectivityLiveData: ConnectivityLiveData


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFlashDealsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        connectivityLiveData = (activity as MainActivity).connectivityLiveData

        featureDealsAdapter = FeatureDealsAdapter {
            shareViewModel.topDeal = it.Result.Data[0]
            findNavController().navigate(R.id.orderDetailsFragment)
        }
        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }


        connectivityLiveData.observe(
            viewLifecycleOwner
        ) { hasNetwork ->
            if (hasNetwork)
                flashDealsViewModel.flashDealsResponse

        }

        binding.recyclerViewFlashDeals.apply {
            adapter = featureDealsAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)

        }



        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    flashDealsViewModel.searchQuery.value = newText
                }
                return true
            }
        })

        binding.searchNotFound.visibility = View.GONE

        binding.filterLayout.setOnClickListener { findNavController().navigate(R.id.productDealsFragment) }


        binding.dealsArrowBack.setOnClickListener { activity?.onBackPressed() }

        val textView = binding.timingTextView
        val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        val currentTime = dateFormat.parse(textView.text.toString())

        val calendar = Calendar.getInstance()
        calendar.time = currentTime
        calendar.add(Calendar.MINUTE, 1)

        val timer = Timer()
        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    textView.text = dateFormat.format(calendar.time)
                    calendar.add(Calendar.SECOND, 1)
                }
            }
        }, 0, 1000)


    var flash = FlashDeals(
        TaskEnum.TOPDEALS.taskType,
        0,
        Limit = 15,
        "",
        "Flash",
        arrayOf(),
        arrayOf(),
        "",
        0,
        0,
        CountryCode.NGN.countries.countryCode,
        CountryCode.NGN.countries.countryKey
    )
    flashDeal(flash)

}
    private fun flashDeal(flashDeals: FlashDeals) {
        lifecycleScope.launch {
            flashDealsViewModel.getFlashDeals(flashDeals)
        }
    }

    private fun setUpObservers() {
        flashDealsViewModel.flashDealsResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    Log.d("AAAAAA", "${it.message}")
                    requireView().showSnackBar(R.string.call_success)
                    it.data?.Result?.Data?.let { it1 ->
                        flashDealsViewModel.cachedTopDeals = it1 as List<Datum>
                        updateViews(flashDealsViewModel.searchQuery.value!!)
                    }
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }

        flashDealsViewModel.searchQuery.observe(viewLifecycleOwner){ query ->
            updateViews(query)
        }
    }

    private fun updateViews(query: String) {
        if (query.isEmpty()){
            featureDealsAdapter.differ.submitList(flashDealsViewModel.cachedTopDeals)
            binding.searchNotFound.visibility = View.GONE
            binding.recyclerViewFlashDeals.visibility = View.VISIBLE
            return
        }
        if (!flashDealsViewModel.cachedTopDeals.any{
                it.Title.lowercase().contains(query)
            }){
            binding.searchNotFound.visibility = View.VISIBLE
            binding.recyclerViewFlashDeals.visibility = View.GONE
        }else{
            binding.searchNotFound.visibility = View.GONE
            binding.recyclerViewFlashDeals.visibility = View.VISIBLE
        }
            featureDealsAdapter.differ.submitList(flashDealsViewModel.cachedTopDeals.filter{
            it.Title.lowercase().contains(query)
        })

    }

}
