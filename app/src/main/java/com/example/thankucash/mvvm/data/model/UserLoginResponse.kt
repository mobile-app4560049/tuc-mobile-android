package com.example.thankucash.mvvm.data.model

data class UserLoginResponse(
    val Key: String,

    val IsNewAccount: Boolean,
    val AccountKey: String,
    val AccountID: Long,
    val LoginTime: String,
    val DisplayName: String,
    val EmailAddress: String,
    val Name: String,
    val IconURL: String,
    val IsMobileNumberVerified: Boolean,
    val RequestToken: String,
    val ReferralCode: String,
    val CreatedAt: String,
    val Country: Country
)

data class Country(
    val Name: String,
    val Isd: String,
    val Iso: String,
    val Symbol: String
)
