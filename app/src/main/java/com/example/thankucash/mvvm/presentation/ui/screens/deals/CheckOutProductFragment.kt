package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.app.Dialog
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.thankucash.MainActivity
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentCheckOutProductBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.presentation.viewmodel.BuyDealsRegisterUserViewModel
import com.example.thankucash.mvvm.utils.validation.ConnectivityLiveData
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CheckOutProductFragment : Fragment() {
    private var _binding: FragmentCheckOutProductBinding? = null
    private val binding get() = _binding!!
    lateinit var connectivityLiveData: ConnectivityLiveData
    private lateinit var dialog: Dialog
    private val shareViewModel: ShareViewModel by activityViewModels()
    private val buyDealsRegisterUserViewModel: BuyDealsRegisterUserViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCheckOutProductBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        connectivityLiveData = (activity as MainActivity).connectivityLiveData

        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }

        connectivityLiveData.observe(
            viewLifecycleOwner
        ) { hasNetwork ->
            if (hasNetwork) buyDealsRegisterUserViewModel.buyDealsRegisterResponse

        }

        binding.apply {
            addIncrement.setOnClickListener {
                val currentValue = binding.productItem.text.toString().substringBefore(" ").toInt()
                val newValue = currentValue + 1
                binding.productItem.text = "$newValue Product from ThankUCash Merchant"
            }
            subtractDecrement.setOnClickListener {
                val currentValue = binding.productItem.text.toString().substringBefore(" ").toInt()
                val newValue = currentValue - 1
                binding.productItem.text = "$newValue Product from ThankUCash Merchant"
            }
        }

        binding.changeAddress.setOnClickListener {
            findNavController().navigate(R.id.billingAddressFragment)
        }


        binding.buyNowDealsBtn.setOnClickListener { findNavController().navigate(R.id.checkOutPaymentOptionsFragment) }


        binding.orderDetailsArrowBack2.setOnClickListener { activity?.onBackPressed() }

        binding.lessAmount.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        val model = shareViewModel
        if (model.topDeal != null) {
            var difference = shareViewModel.topDeal?.ActualPrice!! - shareViewModel.topDeal?.SellingPrice!!
            var percentage = difference / shareViewModel.topDeal?.ActualPrice!! * 100
            binding.percentTextview.text = "${percentage}%"
            binding.itemName.text = "NGN ${model.topDeal?.Title}"
            binding.amountPrice.text = "${model.topDeal?.SellingPrice}"
            binding.buyNowDealsBtn.text = "Buy Now @ ${model.topDeal?.SellingPrice}"
            Glide.with(this)
                .load(model.topDeal?.ImageUrl)
                .placeholder(R.drawable.imagefan)
                .into(binding.itemImage)
            binding.textView87.text = "${model.topDeal?.SellingPrice}"
            binding.lessAmount.text = "NGN${model.topDeal?.ActualPrice}"
            binding.convenienceFeeTextView.text = "${model.topDeal?.IsBookmarked}"
            binding.priceAmount.text = "${model.topDeal?.SellingPrice}"
        }


            var registerUser = BuyDealsRegisterUser(
                TaskEnum.BUYDEALSREGISTER.taskType,
                587,
                "eda97b0da3554f459f617327b8b27e42",
                "online",
                0,
                "",
                0,
                null,
                0,
                null,
                null,
                0,
                1,
                CountryCode.NGN.countries.countryCode,
                CountryCode.NGN.countries.countryKey
            )

            buyDealsUser(registerUser)

        }

    private fun buyDealsUser(buyDealsRegisterUser: BuyDealsRegisterUser) {
        lifecycleScope.launch {
            buyDealsRegisterUserViewModel.buyDealsRegister(buyDealsRegisterUser)
        }
    }


    private fun setUpObservers() {
      buyDealsRegisterUserViewModel.buyDealsRegisterResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_success)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
    }



    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}