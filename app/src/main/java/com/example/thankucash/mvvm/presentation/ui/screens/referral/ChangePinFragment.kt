package com.example.thankucash.mvvm.presentation.ui.screens.referral

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentChangePinBinding
import com.example.thankucash.mvvm.data.model.ChangePin
import com.example.thankucash.mvvm.presentation.viewmodel.ChangePinViewModel
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import kotlinx.coroutines.launch

class ChangePinFragment : Fragment() {
    private var _binding : FragmentChangePinBinding? = null
    private val binding get() = _binding!!
    private val changePinViewModel: ChangePinViewModel by activityViewModels()
    private lateinit var dialog: Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentChangePinBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            dialog = provideCustomAlertDialog()
            setUpObservers()
        }

        binding.changePinBackArrow.setOnClickListener { activity?.onBackPressed() }

        binding.changePinBtn.setOnClickListener {
            val user = ChangePin(
                "",
                "",
                binding.tempPin.text.toString(),
                binding.newPin.text.toString()
            )
            changePin(user)
        }
    }

    private fun changePin(changePin: ChangePin){
        lifecycleScope.launch{
            changePinViewModel.changeUserPin(changePin)
        }
    }


    private fun setUpObservers() {
        changePinViewModel.changePinResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    findNavController().popBackStack()
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}