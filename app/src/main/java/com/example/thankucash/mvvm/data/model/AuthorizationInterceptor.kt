package com.example.thankucash.mvvm.data.model

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response


class MainAuthorizationInterceptor(val context: Context) : Interceptor {

        override fun intercept(chain: Interceptor.Chain): Response {



            val request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("User-Token", "token!!")




            val builtRequest =     request.build()

            return chain.proceed(builtRequest)
        }
    }

