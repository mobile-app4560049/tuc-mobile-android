package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GetAllDealsViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel(){
    private var _allDealsResponse = MutableLiveData<Resource<AllDealsResponse>>()
    val allDealsResponse: MutableLiveData<Resource<AllDealsResponse>> get() = _allDealsResponse
    var cachedTopDeals: List<Datum> = listOf()

    var searchQuery = MutableLiveData<String>("")


    fun getAllDeals(allDeals: AllDeals){
        _allDealsResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch(Dispatchers.Default) {
            try {
                val response = authRepository.getAllDeals(allDeals)
                _allDealsResponse.postValue(Resource.Success(response))
            }catch (e: Exception){
                Log.d("Error connection", e.javaClass.simpleName)
            }
        }
    }
}