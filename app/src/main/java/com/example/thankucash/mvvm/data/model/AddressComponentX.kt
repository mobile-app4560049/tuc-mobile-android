package com.example.thankucash.mvvm.data.model

data class AddressComponentX(
    val AddressLine1: String,
    val AddressLine2: String,
    val CityId: Int,
    val CityName: String,
    val ContactNumber: String,
    val CountryId: Int,
    val CountryName: String,
    val EmailAddress: String,
    val Latitude: Int,
    val Longitude: Int,
    val Name: String,
    val ReferenceId: Int,
    val ReferenceKey: String,
    val StateId: Int,
    val StateName: String
)