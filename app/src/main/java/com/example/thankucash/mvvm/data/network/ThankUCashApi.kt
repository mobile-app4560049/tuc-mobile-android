package com.example.thankucash.mvvm.data.network

import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.data.model.UpdateProfileResponse
import com.example.thankucash.mvvm.data.model.UpdateUserAccount
import com.example.thankucash.mvvm.utils.validation.Resource
import retrofit2.http.Body
import retrofit2.http.POST

interface ThankUCashApi {

    @POST("maddeals/register/userlogin")
    suspend fun loginUser(@Body userLogin: UserLogin): GenericResult<UserLoginResponse>

    @POST("maddeals/register/userregistration")
    suspend fun registerUser(@Body userRegistration: UserRegistration): GenericResult<UserRegistrationResponse>

    @POST("maddeals/register/userverification")
    suspend fun verifyUser(@Body userVerification: UserVerification): GenericResult<UserVerifyPhoneResponse>

    @POST("maddeals/register/resetpin")
    suspend fun resetPin(@Body resetPin: ResetPin): Int

    @POST("maddeals/register/updatepin")
    suspend fun updatePin(@Body updatePin: UpdatePin): Int

    @POST("maddeals/account/updateaccount")
    suspend fun updateUserProfile(@Body updateUserAccount: UpdateUserAccount): GenericResult<UpdateProfileResponse>

    @POST("maddeals/account/updaterefid")
    suspend fun referralProfile(@Body referralProfile: ReferralProfile) : GenericResult<ReferralProfileResponse>

    @POST("maddeals/register/changepin")
    suspend fun changePin(@Body changePin: ChangePin): GenericResult<ChangePinResponse>

    @POST("maddeals/account/requestotp")
    suspend fun requestOtp(@Body requestOtp: RequestOtp): GenericResult<RequestOtpResponse>

    @POST("maddeals/account/getaccount")
    suspend fun getUserAccount(@Body getUserAccount: GetUserAccount): GenericResult<UpdateProfileResponse>

    @POST("v3/cust/deals/getdealpromotions")
    suspend fun getTopDeals(@Body topDeals: TopDeals): TopDealsResponse

    @POST("v3/cust/deals/getdeals")
    suspend fun getFlashDeals(@Body flashDeals: FlashDeals): FlashDealsResponse

    @POST("v3/cust/deals/getdeals")
    suspend fun getAllDeals(@Body allDeals: AllDeals): AllDealsResponse

    @POST("v3/deals/op/buydeal_initizalize")
    suspend fun buyDeals(@Body buyDeals: BuyDeals): BuyDealsResponse

    @POST("v3/deals/op/buydeal_confirm")
    suspend fun dealsConfirm(@Body confirmUserDeals: ConfirmUserDeals): BuyDealsResponse

    @POST("v3/cust/deals/getdealcode")
    suspend fun getDealCode(@Body getDealCode: GetDealCode): GetDealCodeResponse

    @POST("v3/cust/trans/buypointinitialize")
    suspend fun buyPointsInitialized(@Body buyPointsPayment: BuyPointsPayment): BuyPointsResPonse

    @POST("v3/cust/trans/buypointverify")
    suspend fun buyPointsVerify(@Body buyPointsVerify: BuyPointsVerify): BuyPointsVerifyResponse

    @POST("v3/deals/op/buydeal_initizalize")
    suspend fun buyDealsRegisterUser(@Body buyDealsRegisterUser: BuyDealsRegisterUser): RegisterUserResponse
}