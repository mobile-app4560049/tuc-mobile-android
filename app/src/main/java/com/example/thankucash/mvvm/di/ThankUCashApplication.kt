package com.example.thankucash.mvvm.di

import android.app.Application
import com.freshchat.consumer.sdk.Freshchat
import com.freshchat.consumer.sdk.FreshchatConfig
import dagger.hilt.android.HiltAndroidApp

//
@HiltAndroidApp
class ThankUCashApplication: Application()
