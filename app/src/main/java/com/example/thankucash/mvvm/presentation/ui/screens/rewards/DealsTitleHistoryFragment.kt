package com.example.thankucash.mvvm.presentation.ui.screens.rewards

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentDealsTitleHistoryBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DealsTitleHistoryFragment : Fragment() {
    private var _binding: FragmentDealsTitleHistoryBinding? = null
    private val binding get() = _binding!!

    private val shareViewModel: ShareViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDealsTitleHistoryBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.viewSave.setOnClickListener { findNavController().navigate(R.id.wishlistViewPagerFragment) }

        //create object of viewmodel
        val model = shareViewModel
        if (model.deals != null) {
            model.deals ?.imageView?.let { binding.dealsFan.setImageResource(R.drawable.deals_title_icon).toString() }
            binding.usedItemButton.text = "${model.deals ?.itemNew}"
        }

        binding.dealsHistoryTitleArrowBack2.setOnClickListener { activity?.onBackPressed() }
    }
}