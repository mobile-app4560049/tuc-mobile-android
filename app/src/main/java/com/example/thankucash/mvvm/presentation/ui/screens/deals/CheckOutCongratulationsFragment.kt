package com.example.thankucash.mvvm.presentation.ui.screens.deals

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.thankucash.MainActivity
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentCheckOutCongratulationsBinding
import com.example.thankucash.mvvm.data.enum.CountryCode
import com.example.thankucash.mvvm.data.enum.TaskEnum
import com.example.thankucash.mvvm.data.model.*
import com.example.thankucash.mvvm.presentation.viewmodel.BuyDealsConfirmViewModel
import com.example.thankucash.mvvm.presentation.viewmodel.BuyPointsVerifyVieModel
import com.example.thankucash.mvvm.presentation.viewmodel.GetDealCodeViewModel
import com.example.thankucash.mvvm.utils.validation.ConnectivityLiveData
import com.example.thankucash.mvvm.utils.validation.Resource
import com.example.thankucash.mvvm.utils.validation.viewextensions.provideCustomAlertDialog
import com.example.thankucash.mvvm.utils.validation.viewextensions.showSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.lang.Float.parseFloat
import java.text.NumberFormat
import java.util.*

@AndroidEntryPoint
class CheckOutCongratulationsFragment : Fragment() {
    private val buyDealsConfirmViewModel: BuyDealsConfirmViewModel by activityViewModels()
    private var _binding: FragmentCheckOutCongratulationsBinding? = null
    private lateinit var dialog: Dialog
    private val shareViewModel: ShareViewModel by activityViewModels()
    private val getDealCodeViewModel : GetDealCodeViewModel by activityViewModels()
    lateinit var connectivityLiveData: ConnectivityLiveData
    private val buyPointsVerifyVieModel: BuyPointsVerifyVieModel by activityViewModels()


    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCheckOutCongratulationsBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = shareViewModel
        if (model.topDeal != null) {
            val ngnCurrency = Currency.getInstance("NGN")
            val ngnFormat = NumberFormat.getCurrencyInstance().apply {
                currency = ngnCurrency
            }
            var sellingPrice = model.topDeal?.SellingPrice?.toFloat()
            if (sellingPrice != null) {
                binding.confirmPayment.text = ngnFormat.format(sellingPrice)
            }
        }
        connectivityLiveData = (activity as MainActivity).connectivityLiveData



        binding.apply {
            dialog = provideCustomAlertDialog()
            setObserves()
        }





        var confirmationOfDeals = ConfirmUserDeals(
            0,
            1230,
            CountryCode.NGN.countries.countryCode,
            CountryCode.NGN.countries.countryKey,
            587,
            "",
            0,
            0,
            "null",
            1,
            "null",
            "wallet",
            "1265",
            "",
            TaskEnum.BUYDEALCONFIRMS.taskType,
            0,
            "",
            1230,
            0
        )
        confirmDeals(confirmationOfDeals)


        var getDealsCode = GetDealCode(
            TaskEnum.GETDEALCODE.taskType,
            0,
            "a6b0626de76040298bbd854450d9be73",
            "455",
            "a6b0626de76040298bbd854450d9be73",
            CountryCode.NGN.countries.countryCode,
            CountryCode.NGN.countries.countryKey
        )
        getDealsCode(getDealsCode)


        var buyPoints = BuyPointsVerify(
        TaskEnum.BUYPOINTSVERIFY.taskType,
            "3286b67672ff4216864b248ce7e2f489",
            11968,
            20000,
            "TPP11968O3031313011080323",
            "2605230496",
            "success",
            "Approved",
            "paystack"
        )
        buyPoints(buyPoints)
    }





    private fun confirmDeals(confirmUserDeals: ConfirmUserDeals) {
        lifecycleScope.launch {
            buyDealsConfirmViewModel.confirmDeals(confirmUserDeals)
        }
    }

    private fun buyDealsConfirm() {
       buyDealsConfirmViewModel.dealsConfirmResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                   getDealCode()
                    requireView().showSnackBar(R.string.call_success)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
    }


    private fun getDealsCode(getDealCode: GetDealCode) {
        lifecycleScope.launch {
            getDealCodeViewModel.getDealsCode(getDealCode)
        }
    }

    private fun getDealCode() {
        getDealCodeViewModel.getDealsResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_success)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
    }


    private fun buyPoints(buyPointsVerify: BuyPointsVerify) {
        lifecycleScope.launch {
            buyPointsVerifyVieModel.buyPointsVerify(buyPointsVerify)
        }
    }

    private fun setObserves() {
        buyPointsVerifyVieModel.buyPointsVerifyResponse.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    buyDealsConfirm()
                    requireView().showSnackBar(R.string.call_success)
                }
                is Resource.Error -> {
                    dialog.dismiss()
                    requireView().showSnackBar(R.string.call_failed)
                }
                is Resource.Loading -> {
                    dialog.show()
                }
            }
        }
    }
}