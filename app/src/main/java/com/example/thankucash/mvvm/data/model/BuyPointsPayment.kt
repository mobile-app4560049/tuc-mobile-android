package com.example.thankucash.mvvm.data.model

data class BuyPointsPayment(
    val Task: String,
    val AccountKey : String,
    val Amount: Int,
    val PaymentMode: String
)
