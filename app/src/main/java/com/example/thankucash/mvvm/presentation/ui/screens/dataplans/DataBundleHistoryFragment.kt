package com.example.thankucash.mvvm.presentation.ui.screens.dataplans

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentDataBundleHistoryBinding
import com.example.thankucash.mvvm.presentation.adapters.DataBundleAdapter
import com.example.thankucash.mvvm.utils.validation.Constant
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DataBundleHistoryFragment : Fragment() {
    private var _binding: FragmentDataBundleHistoryBinding? = null
    private val binding get() = _binding!!
    private val shareViewModel: ShareViewModel by activityViewModels()
    lateinit var dataBundleAdapter: DataBundleAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDataBundleHistoryBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBundleAdapter = DataBundleAdapter{
            shareViewModel.dataBundle = it
            findNavController().navigate(R.id.airtimeHistoryFragment2)
        }

        binding.dataBundleRecyclerView.apply {
            adapter = dataBundleAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL,false
            )
        }
        dataBundleAdapter.submitList(Constant.dataBundle)

            binding.cancel.setOnClickListener { activity?.onBackPressed() }
    }
}