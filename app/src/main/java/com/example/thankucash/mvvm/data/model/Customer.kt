package com.example.thankucash.mvvm.data.model

data class Customer(
    val AccountId: Int,
    val AccountKey: String,
    val EmailAddress: String,
    val IsGuestUser: Boolean,
    val MobileNumber: String,
    val Name: String
)