package com.example.thankucash.mvvm.presentation.ui.screens.dataplans

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.thankucash.R
import com.example.thankucash.databinding.BeneficalCongratulationsBottomSheetBinding
import com.example.thankucash.databinding.FragmentBeneficiaryBinding
import com.example.thankucash.databinding.SaveBeneficaryBottomSheetBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior

//@AndroidEntryPoint
class SaveBeneficiaryFragment : Fragment() {
    private var _binding : FragmentBeneficiaryBinding? = null
    private val binding get() = _binding!!

    private var bottomSheetBinding: SaveBeneficaryBottomSheetBinding? = null
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentBeneficiaryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bottomSheetBinding = SaveBeneficaryBottomSheetBinding.inflate(layoutInflater, binding.root, false)


    }

}