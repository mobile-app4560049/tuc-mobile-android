package com.example.thankucash.mvvm.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thankucash.mvvm.data.model.UpdatePin
import com.example.thankucash.mvvm.data.repository.AuthRepository
import com.example.thankucash.mvvm.utils.validation.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ForgotPinViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private var _updatePinResponse = MutableLiveData<Resource<Int>>()
    val updatePinResponse: LiveData<Resource<Int>> get() = _updatePinResponse

    fun updateUserPin(updatePin: UpdatePin){
        _updatePinResponse.value = Resource.Loading(null, "Loading...")
        viewModelScope.launch(Dispatchers.Default) {
            try {
                _updatePinResponse.value = authRepository.updatePin(updatePin)
            }catch (e:Exception){
                Log.d("e.exception", e.javaClass.simpleName)
            }
        }
    }
}