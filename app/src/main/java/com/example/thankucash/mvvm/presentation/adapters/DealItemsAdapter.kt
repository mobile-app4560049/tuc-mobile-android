package com.example.thankucash.mvvm.presentation.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.thankucash.R
import com.example.thankucash.databinding.FlashDealsItemBinding

class DealItemsAdapter: RecyclerView.Adapter<DealItemsAdapter.DealsItemViewHolder>() {
    private var dealsItemList: ArrayList<String> = arrayListOf()

     class DealsItemViewHolder(val binding: FlashDealsItemBinding): RecyclerView.ViewHolder(binding.root) {
         @SuppressLint("SuspiciousIndentation")
         fun bind(dealsItemList: ArrayList<String>) {
        binding.description.text = dealsItemList[0]
             binding.menu.setOnClickListener{
                 it.findNavController().navigate(R.id.checkOutDealsFragment)
             }
             binding.buyNow.setOnClickListener { it.findNavController().navigate(R.id.orderDetailsFragment) }
         }
     }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealsItemViewHolder {
          val inflater = FlashDealsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
          return DealsItemViewHolder(inflater)
     }

     override fun onBindViewHolder(holder: DealsItemViewHolder, position: Int) {
          holder.bind(dealsItemList)
     }

     override fun getItemCount(): Int = dealsItemList.size
     fun submitList (list: ArrayList<String>){
          dealsItemList = list
     }
}