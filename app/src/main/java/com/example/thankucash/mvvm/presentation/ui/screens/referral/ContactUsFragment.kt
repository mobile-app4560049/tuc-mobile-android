package com.example.thankucash.mvvm.presentation.ui.screens.referral

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.paystack.android.PaystackSdk.applicationContext
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentContactUsBinding
import com.freshchat.consumer.sdk.Freshchat
import com.freshchat.consumer.sdk.FreshchatConfig
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContactUsFragment : Fragment() {
    private var _binding : FragmentContactUsBinding? = null
    private val binding get() = _binding!!
    private val appId = ""
    private val appKey = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentContactUsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val freshchatConfig = FreshchatConfig(appId, appKey)

        binding.contactBackArrow.setOnClickListener { activity?.onBackPressed() }
        //set user properties
        binding.chatImage.setOnClickListener {
            // Get the user object for the current installation
            val freshchatUser = Freshchat.getInstance(applicationContext).user
            freshchatUser.firstName = "John"
            freshchatUser.lastName = "Doe"
            freshchatUser.email = "john.doe.1982@mail.com"
            freshchatUser.setPhone("+91", "9790987495")


            Freshchat.getInstance(applicationContext).user = freshchatUser
        }
    }

}