package com.example.thankucash.mvvm.presentation.ui.screens.utility

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentElectricityBillsBinding
import com.example.thankucash.mvvm.presentation.adapters.UtilityAdapter
import com.example.thankucash.mvvm.utils.validation.Constant

class ElectricityBillsFragment : Fragment() {
    private var _binding : FragmentElectricityBillsBinding? = null
    private val binding get() = _binding!!
    lateinit var utilityAdapter: UtilityAdapter
    private val shareViewModel: ShareViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentElectricityBillsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        utilityAdapter = UtilityAdapter()

        val model = shareViewModel
        if (model.electricityValue  != null ){
            binding.electricitySelectPackage.text = "NGN ${model.electricityValue?.amount} ${model.electricityValue?.electricityName}"
            binding.editTextAmount.text = "${model.electricityValue?.amount}"
        }

        binding.arrowElectricity.setOnClickListener {
            findNavController().navigate(R.id.electricityPlansFragment)
        }

        binding.buyElectricityBtn.setOnClickListener {
            findNavController().navigate(R.id.electricityPaymentOptionsFragment)
        }
        binding.recyclerViewElectricity.apply {
            adapter = utilityAdapter
            layoutManager = StaggeredGridLayoutManager(3,LinearLayoutManager.HORIZONTAL)

        }
        utilityAdapter.submitList(Constant.electricityList)
    }

}