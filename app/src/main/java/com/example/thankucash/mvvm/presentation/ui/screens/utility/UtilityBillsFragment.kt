package com.example.thankucash.mvvm.presentation.ui.screens.utility

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentUtilityBillsBinding
import com.example.thankucash.mvvm.presentation.adapters.UtilityAdapter
import com.example.thankucash.mvvm.utils.validation.Constant

class UtilityBillsFragment : Fragment() {
    private var _binding: FragmentUtilityBillsBinding? = null
    private val binding get() = _binding!!
    lateinit var utilityAdapter: UtilityAdapter
    private val shareViewModel: ShareViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUtilityBillsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        utilityAdapter = UtilityAdapter()

        val model = shareViewModel
        if (model.method  != null ){
            binding.selectPackageText.text = "NGN ${model.method?.amount} ${model.method?.dataName} ${model?.method?.image}"
            binding.editTextAmount.text = "${model.method?.amount}"
            binding
        }

        binding.tvRechargeBtn.setOnClickListener {
            findNavController().navigate(R.id.utilityPaymentOptionsFragment)
        }

        binding.arrowDown.setOnClickListener { findNavController().navigate(R.id.dstvPlansFragment) }


        binding.recyclerView.apply {
            adapter = utilityAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL,false
            )

        }
        utilityAdapter.submitList(Constant.electricityProviderList)

    }

}