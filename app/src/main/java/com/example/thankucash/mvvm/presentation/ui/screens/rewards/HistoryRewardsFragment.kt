package com.example.thankucash.mvvm.presentation.ui.screens.rewards

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.thankucash.R
import com.example.thankucash.ShareViewModel
import com.example.thankucash.databinding.FragmentHistoryRewardsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HistoryRewardsFragment : Fragment() {
    private val shareViewModel: ShareViewModel by activityViewModels()
    private var _binding : FragmentHistoryRewardsBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHistoryRewardsBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = shareViewModel
        if (model.rewards != null) {
            binding.walletTopText.text = "NGN ${model.rewards?.rewardText}"
            binding.fillingStationText.text = "${model.rewards ?.stationName}"
            model.rewards ?.calendarImage?.let { binding.rewardCalendar.setImageResource(it).toString() }
            binding.amount.text = "${model.rewards ?.amount}"
            binding.rewardDate.text ="${model.rewards ?.date}"
            binding.rewardTime.text = "${model.rewards ?.time2}"
        }

        binding.cancel.setOnClickListener { activity?.onBackPressed() }
    }




}