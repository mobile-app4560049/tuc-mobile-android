package com.example.thankucash.mvvm.presentation.ui.screens.rewards

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thankucash.R
import com.example.thankucash.databinding.FragmentRewardsHistoryBinding
import com.example.thankucash.mvvm.presentation.adapters.DealItemsAdapter
import com.example.thankucash.mvvm.presentation.adapters.DealsItemListAdapter
import com.example.thankucash.mvvm.presentation.adapters.RewardsPlanListAdapter
import com.example.thankucash.mvvm.utils.validation.Constant

class RewardsHistoryFragment : Fragment() {
    private var _binding : FragmentRewardsHistoryBinding? = null
    private val binding get() = _binding!!
    lateinit var rewardsPlanListAdapter: RewardsPlanListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRewardsHistoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rewardsPlanListAdapter = RewardsPlanListAdapter{
            findNavController().navigate(R.id.historyRewardsFragment2)
        }




        binding.recyclerHistoryRewards.apply {
            adapter = rewardsPlanListAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
        rewardsPlanListAdapter.submitList(Constant.rewardList)


    }

}