package com.example.thankucash.mvvm.data.model

data class GuestCheckOutResponse (
    val Status: String,
    val Message: String,
    val Result: GuestResult,
    val ResponseCode: String,
    val Mode: String
)

data class GuestResult (
    val AccountID: Long,
    val Key: String,
    val AddressID: Long
)
