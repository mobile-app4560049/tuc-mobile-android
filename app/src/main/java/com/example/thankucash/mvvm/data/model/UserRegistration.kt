package com.example.thankucash.mvvm.data.model

data class UserRegistration(
    var firstName: String,
    var lastName: String,
    var task : String,
    var host : Int,
    var countryIsd: String,
    var mobileNumber : String,
    var accountPin : String,
    var emailAddress: String,
    var referredBy: String?,
    var countryId : Int,
    var countryKey: String

)
