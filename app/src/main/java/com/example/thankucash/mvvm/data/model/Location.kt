package com.example.thankucash.mvvm.data.model

data class Location(
    val Address: String,
    val ContactNumber: String,
    val DisplayName: String,
    val Latitude: Double,
    val Longitude: Double
)