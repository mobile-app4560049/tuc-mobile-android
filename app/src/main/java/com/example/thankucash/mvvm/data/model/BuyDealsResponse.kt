package com.example.thankucash.mvvm.data.model

data class BuyDealsResponse(
    val Status: String,
    val Message : String,
    val ResponseCode: String,
    val Mode: String
)
