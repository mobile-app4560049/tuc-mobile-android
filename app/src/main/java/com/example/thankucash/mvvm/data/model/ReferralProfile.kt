package com.example.thankucash.mvvm.data.model

data class ReferralProfile(
    val task: String,
    val email: String,
    val referralCode : String? = null,
    val countryId: Int,
    val countryKey: String

)